"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Map tools for drawing Rectangles on the canvas
"""

from math import cos, sin, radians, sqrt, degrees, atan2
from qgis.gui import QgsMapTool, QgsRubberBand
from qgis.core import (QgsCoordinateTransform,
                       QgsGeometry,
                       QgsPointXY,
                       QgsMapSettings,
                       QgsDistanceArea,
                       QgsPoint,
                       QgsProject,
                       QgsCoordinateReferenceSystem)

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QCursor, QPixmap, QColor

from iquaview.src.utils.calcutils import calc_is_collinear


def rotate_point(point, angle):
    """
    Auxliary function to rotate point by a given angle (in radians)
    :param point: Point to rotate
    :type point: QgsPointXY
    :param angle: Angle to rotate
    :type angle: float
    :return: Position of the rotated point
    :rtype: QgsPointXY
    """
    x = cos(angle) * point.x() - sin(angle) * point.y()
    y = sin(angle) * point.x() + cos(angle) * point.y()
    return QgsPointXY(x, y)


class Rectangle:
    def __init__(self, crs):
        """
        Init of the object Rectangle
        :param crs: coordinate reference system
        :type crs: QgsCoordinateReferenceSystem
        """
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(crs.ellipsoidAcronym())

    def get_rect_from_center(self, pc, p2, angle=0.0):
        """
        Creates a rectangle from a center point, a corner point and an angle

        :param pc: center point of the geometry
        :type pc: QgsPointXY
        :param p2: a corner of the geometry
        :type p2: QgsPointXY
        :param angle: angle of the geometry, 0 by default. In radians
        :type angle: float
        :return: 4 points geometry
        :rtype: QgsGeometry
        """

        if angle == 0:
            x_offset = abs(p2.x() - pc.x())
            y_offset = abs(p2.y() - pc.y())

            pt1 = QgsPointXY(pc.x() - x_offset, pc.y() - y_offset)
            pt2 = QgsPointXY(pc.x() - x_offset, pc.y() + y_offset)
            pt3 = QgsPointXY(pc.x() + x_offset, pc.y() + y_offset)
            pt4 = QgsPointXY(pc.x() + x_offset, pc.y() - y_offset)

            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

        else:
            x_offset = (cos(angle) * (p2.x() - pc.x()) + sin(angle) * (p2.y() - pc.y()))
            y_offset = -(-sin(angle) * (p2.x() - pc.x()) + cos(angle) * (p2.y() - pc.y()))

            pt1 = QgsPointXY(pc.x() - x_offset, pc.y() - y_offset)
            pt2 = QgsPointXY(pc.x() - x_offset, pc.y() + y_offset)
            pt3 = QgsPointXY(pc.x() + x_offset, pc.y() + y_offset)
            pt4 = QgsPointXY(pc.x() + x_offset, pc.y() - y_offset)

            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

        return geom

    def get_rect_rotated(self, geom, cp, ep = QgsPointXY(0, 0), ip = QgsPointXY(0, 0), delta = 0):
        """
        Rotates a geometry by some delta + the angle between 2 points and the center point of the geometry

        :param geom: geometry to be rotated
        :type geom: QgsGeometry
        :param cp: center point of the geometry
        :type cp: QgsPointXY
        :param ep: point marking the end of the rotation
        :type ep: QgsPointXY
        :param ip: point marking the beginning of the rotation
        :type ip: QgsPointXY
        :param delta: extra angle of rotation in radians
        :type delta: float
        :return: geometry rotated and total angle of rotation
        :rtype: QgsGeometry, float
        """

        angle_1 = self.distance.bearing(cp, ep)
        angle_2 = self.distance.bearing(cp, ip)
        angle_rotation = delta + (angle_2 - angle_1)

        coords = []
        ring = []
        for i in geom.asPolygon():
            for k in i:
                ini_point = QgsPointXY(k.x() - cp.x(), k.y() - cp.y())
                end_point = rotate_point(ini_point, angle_rotation)
                p3 = QgsPointXY(cp.x() + end_point.x(), cp.y() + end_point.y())
                ring.append(p3)
            coords.append(ring)
            ring = []

        geom = QgsGeometry().fromPolygonXY(coords)

        return geom, angle_rotation

    def get_rect_by3_points(self, p1, p2, p3, length=0):
        """
        Creates a projected rectangle over an spheroid from 3 different points
        :param p1: A rectangle point
        :type p1: QgsPointXY
        :param p2: A rectangle point
        :type p2: QgsPointXY
        :param p3: A rectangle point
        :type p3: QgsPointXY
        :param length: distance from p2 to p3
        :type length: float
        :return: Geometry with the 4 points of the rectangle
        :rtype: QgsGeometry
        """
        angle_exist = self.distance.bearing(p1, p2)
        side = calc_is_collinear(p1, p2, p3)  # check if x_p2 > x_p1 and inverse side
        if side == 0:
            return None
        if length == 0:
            length = self.distance.measureLine(p2, p3)
        p3 = self.distance.measureLineProjected(p2, length, angle_exist + radians(90) * side)[1]
        p4 = self.distance.measureLineProjected(p1, length, angle_exist + radians(90) * side)[1]
        geom = QgsGeometry.fromPolygonXY([[p1, p2, p3, p4]])
        return geom

    def get_rect_projection(self, rect_geom, cp, x_length = 0, y_length = 0):
        """
        Transforms the rectangle geometry to its projection on the real world, making all its angles 90º

        :param rect_geom: 4 point geometry
        :type rect_geom: QgsGeometry
        :param cp: central point of the geometry
        :type cp: QgsPointXY
        :param x_length: distance between first and second points of the rect_geom
        :type x_length: float
        :param y_length: distance between second and third points of the rect_geom
        :type y_length: float
        :return: geometry projected to map
        :rtype: QgsGeometry
        """

        if x_length == 0 and y_length == 0:
            proj_geom = self.get_rect_by3_points(rect_geom.asPolygon()[0][0],
                                                 rect_geom.asPolygon()[0][1],
                                                 rect_geom.asPolygon()[0][2])

        else:
            point_two = self.distance.measureLineProjected(rect_geom.asPolygon()[0][0],
                                 x_length,
                                 self.distance.bearing(rect_geom.asPolygon()[0][0],
                                                               rect_geom.asPolygon()[0][1]))[1]
            point_three = self.distance.measureLineProjected(rect_geom.asPolygon()[0][0],
                                   y_length,
                                   self.distance.bearing(rect_geom.asPolygon()[0][1],
                                                                 rect_geom.asPolygon()[0][3]))[1]

            proj_geom = self.get_rect_by3_points(rect_geom.asPolygon()[0][0],
                                                 point_two,
                                                 point_three,
                                                 y_length)

        if proj_geom is not None:

            p1 = proj_geom.asPolygon()[0][0]
            p2 = proj_geom.asPolygon()[0][1]
            p3 = proj_geom.asPolygon()[0][2]
            p4 = proj_geom.asPolygon()[0][3]

            px = (p1.x() + p3.x()) / 2.0
            py = (p1.y() + p3.y()) / 2.0

            p1 = QgsPointXY(p1.x() - px + cp.x(), p1.y() - py + cp.y())
            p2 = QgsPointXY(p2.x() - px + cp.x(), p2.y() - py + cp.y())
            p3 = QgsPointXY(p3.x() - px + cp.x(), p3.y() - py + cp.y())
            p4 = QgsPointXY(p4.x() - px + cp.x(), p4.y() - py + cp.y())

            proj_geom = QgsGeometry.fromPolygonXY([[p1, p2, p3, p4]])

            return proj_geom

        else:
            return rect_geom


class RectBy3PointsTool(QgsMapTool):
    msgbar = pyqtSignal(str)
    rbFinished = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas):
        """
        Init of the object RectBy3PointsTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.n_points = 0
        self.rb = None
        self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3, self.x_p4, self.y_p4 = None, None, None, None, None, None, None, None
        self.length = 0
        crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(crs.ellipsoidAcronym())
        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #1210f3",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.     .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.     .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))
        self.rectangle = Rectangle(crs)

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
            if self.rb:
                self.rb.reset(True)
            self.rb = None

            self.canvas.refresh()
            self.rb_reset_signal.emit()
            return

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already drawn.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.msgbar.emit("Define bearing and extent along track")
            self.rb_reset_signal.emit()

            self.x_p1 = point.x()
            self.y_p1 = point.y()
            self.n_points += 1

        elif self.n_points == 1 and (self.x_p1 != point.x() or self.y_p1 != point.y()):
            # First and second points must be different to generate the geometry correctly
            self.x_p2 = point.x()
            self.y_p2 = point.y()
            self.msgbar.emit("Define extent across track")
            self.n_points += 1

        elif self.n_points == 2 and (self.x_p2 != point.x() or self.y_p2 != point.y()):
            # Second and third points also must be different
            self.canvas.refresh()

            self.x_p3 = point.x()
            self.y_p3 = point.y()

            geom = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1),
                                                      QgsPointXY(self.x_p2, self.y_p2),
                                                      QgsPointXY(self.x_p3, self.y_p3))
            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None

            self.rbFinished.emit(geom)

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """

        if not self.rb: return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.n_points == 1:

            self.rb.setToGeometry(QgsGeometry.fromPolyline([QgsPoint(trans_wgs_to_map.transform(QgsPointXY(self.x_p1,
                                                                                                           self.y_p1))),
                                                            QgsPoint(map_pos)]), None)
            curr_dist = self.distance.measureLine(QgsPointXY(self.x_p1, self.y_p1), currpoint)
            curr_bearing = degrees(self.distance.bearing(QgsPointXY(self.x_p1, self.y_p1), currpoint))
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(curr_dist, curr_bearing))
        if self.n_points >= 2:
            geom = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1),
                                                      QgsPointXY(self.x_p2, self.y_p2),
                                                      currpoint)
            curr_dist = self.distance.measureLine(QgsPointXY(self.x_p2, self.y_p2), currpoint)
            curr_bearing = degrees(
                self.distance.bearing(QgsPointXY(self.x_p2, self.y_p2), QgsPointXY(geom.vertexAt(2))))
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(curr_dist, curr_bearing))

            pt1 = trans_wgs_to_map.transform(geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])
            self.rb.setToGeometry(geom, None)

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
        if self.rb:
            self.rb.reset(True)
            self.rb.hide()
        self.rb = None
        self.canvas.refresh()

    def is_zoom_tool(self):
        """
        Tells if this tool is a zoom tool
        :return: False
        :rtype: bool
        """
        return False

    def is_transient(self):
        """
        Tells if this tool is transient
        :return: False
        :rtype: bool
        """
        return False

    def is_edit_tool(self):
        """
        Tells if this tool is an edit tool
        :return: True
        :rtype: bool
        """
        return True


# RectByFixedExtentTool with fixed length values for the rectangle sides
# Tool class
class RectByFixedExtentTool(QgsMapTool):
    msgbar = pyqtSignal(str)
    rbFinished = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas, x_length, y_length):
        """
        Init of the object RectByFixedExtentTool
        :param canvas: canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param x_length: horizontal length of the rectangle
        :type x_length: float
        :param y_length: vertical length of the rectangle
        :type y_length: float
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.n_points = 0
        self.rb = None
        self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3, self.x_p4, self.y_p4 = None, None, None, None, None, None, None, None
        crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(crs.ellipsoidAcronym())
        self.fixed_p2, self.fixed_p3 = QgsPointXY(0, 0), QgsPointXY(0, 0)
        self.length = 0
        self.x_length = x_length
        self.y_length = y_length
        self.bearing = 0.0
        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #1210f3",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.     .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.     .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))
        self.rectangle = Rectangle(crs)

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
            self.fixed_p2, self.fixed_p3 = None, None
            if self.rb:
                self.rb.reset(True)
            self.rb = None

            self.canvas.refresh()
            self.rb_reset_signal.emit()
            return

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        point_map = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(point_map.x(), point_map.y())

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.msgbar.emit("Define bearing along track")
            self.rb_reset_signal.emit()

            self.x_p1 = point.x()
            self.y_p1 = point.y()
            self.n_points += 1

        elif self.n_points == 1 and (self.x_p1 != point.x() or self.y_p1 != point.y()):
            self.x_p2 = point.x()
            self.y_p2 = point.y()
            self.bearing = self.distance.bearing(QgsPointXY(self.x_p1, self.y_p1), QgsPointXY(self.x_p2, self.y_p2))
            self.msgbar.emit("Define across track direction")
            self.n_points += 1

        elif self.n_points == 2 and (self.x_p2 != point.x() or self.y_p2 != point.y()):
            self.canvas.refresh()

            self.x_p3 = point.x()
            self.y_p3 = point.y()

            geom_wgs = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1), self.fixed_p2, self.fixed_p3,
                                                      self.y_length)

            pt1 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(geom_wgs.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

            self.rb.setToGeometry(geom, None)

            self.n_points = 0
            self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
            self.fixed_p2, self.fixed_p3 = None, None
            self.msgbar.emit("")
            self.rbFinished.emit(geom_wgs)

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """

        if not self.rb: return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())

        point_map = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(point_map.x(), point_map.y())

        if self.n_points == 1:
            self.bearing = self.distance.bearing(QgsPointXY(self.x_p1, self.y_p1), currpoint)
            self.fixed_p2 = self.distance.measureLineProjected(QgsPointXY(self.x_p1, self.y_p1), self.x_length,
                                                                 self.bearing)[1]

            self.rb.setToGeometry(QgsGeometry.fromPolyline([QgsPoint(trans_wgs_to_map.transform(QgsPointXY(self.x_p1, self.y_p1))),
                                                            QgsPoint(trans_wgs_to_map.transform(QgsPointXY(self.fixed_p2)))]),
                                  None)
            curr_bearing = degrees(self.bearing)
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(self.x_length, curr_bearing))
        if self.n_points >= 2:
            # test if currpoint is left or right of the line defined by p1 and p2
            side = calc_is_collinear(QgsPointXY(self.x_p1, self.y_p1), self.fixed_p2, currpoint)

            if side == 0:
                return None
            self.fixed_p3 = self.distance.measureLineProjected(QgsPointXY(self.x_p2, self.y_p2), self.y_length,
                                                                 self.bearing + radians(90) * side)[1]

            geom = self.rectangle.get_rect_by3_points(QgsPointXY(self.x_p1, self.y_p1), self.fixed_p2, self.fixed_p3,
                                                      self.y_length)
            pt1 = trans_wgs_to_map.transform(geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])
            self.rb.setToGeometry(geom, None)

            curr_bearing = degrees(self.bearing + radians(90) * side)
            if curr_bearing < 0.0:
                curr_bearing = 360 + curr_bearing
            self.msgbar.emit(
                "Current distance: {:.3F} m, Current bearing: {:.3F} degrees".format(self.y_length, curr_bearing))

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.x_p1, self.y_p1, self.x_p2, self.y_p2, self.x_p3, self.y_p3 = None, None, None, None, None, None
        self.fixed_p2, self.fixed_p3 = None, None
        if self.rb:
            self.rb.reset(True)
            self.rb.hide()
        self.rb = None

        self.canvas.refresh()

    def is_zoom_tool(self):
        """
        Tells if this tool is a zoom tool
        :return: False
        :rtype: bool
        """
        return False

    def is_transient(self):
        """
        Tells if this tool is transient
        :return: False
        :rtype: bool
        """
        return False

    def is_edit_tool(self):
        """
        Tells if this tool is an edit tool
        :return: True
        :rtype: bool
        """
        return True


# Tool class
class RectFromCenterTool(QgsMapTool):
    msgbar = pyqtSignal(str)
    rbFinished = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas):
        """
        Init of the object RectFromCenterTool
        :param canvas: Canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.n_points = 0
        self.rb = None
        self.mCtrl = None
        self.xc, self.yc = None, None
        crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(crs.ellipsoidAcronym())
        self.x_length = 0
        self.y_length = 0
        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #17a51a",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.  .  .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.  .  .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))
        self.curr_geom = None
        self.last_currpoint = None
        self.curr_angle = 0.0
        self.total_angle = 0.0
        self.rectangle = Rectangle(crs)
        self.angle_ini_rot = 0.0
        self.ini_geom = None

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is pressed, sets
        mCtrl to True and creates apoint and angle of rotation.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.mCtrl = True
            trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                      QgsCoordinateReferenceSystem(4326,
                                                                                   QgsCoordinateReferenceSystem.EpsgCrsId),
                                                      QgsProject.instance().transformContext())
            map_pos = self.toMapCoordinates(self.canvas.mouseLastXY())
            point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())
            self.point_ini_rot = point
            self.angle_ini_rot = self.curr_angle

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is released,
        set mCtrl to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.mCtrl = False

        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.xc, self.yc = None, None
            if self.rb:
                self.rb.reset(True)
            self.rb = None
            self.canvas.refresh()
            self.rb_reset_signal.emit()
            return

    def changegeomSRID(self, geom):
        """
        Transforms the geometry of the layer if it has a different SRID than the project
        :param geom: geometry to transform
        :type geom: QgsGeometry
        :return: geometry transformed, or the same if it's not necessary
        :rtype: QgsGeometry
        """
        layer = self.canvas.currentLayer()
        layerCRSSrsid = layer.crs().srsid()
        projectCRSSrsid = QgsMapSettings().destinationCrs().srsid()
        if layerCRSSrsid != projectCRSSrsid:
            g = QgsGeometry.fromPoint(geom)
            g.transform(QgsCoordinateTransform(projectCRSSrsid, layerCRSSrsid))
            ret_point = g.asPoint()
        else:
            ret_point = geom

        return ret_point

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.canvas.refresh()
            self.rb_reset_signal.emit()

            self.xc = point.x()
            self.yc = point.y()
            self.n_points += 1

        elif self.n_points == 1:
            self.n_points = 0
            self.xc, self.yc = None, None
            self.last_currpoint = None
            self.curr_angle = 0.0
            self.total_angle = 0.0
            self.mCtrl = False

            self.rbFinished.emit(self.curr_geom)
            self.curr_geom = None

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        if not self.rb or not self.xc or not self.yc: return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())
        map_pos = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        self.msgbar.emit(
            "Adjust lengths along track and across track. Hold Ctrl to adjust track orientation. Click when finished.")

        if not self.mCtrl:
            self.curr_geom = self.rectangle.get_rect_from_center(QgsPointXY(self.xc, self.yc), currpoint,
                                                                 self.curr_angle)
            self.ini_geom = self.curr_geom

            if self.curr_angle != 0:
                self.curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.curr_geom,
                                                                                  QgsPointXY(self.xc, self.yc),
                                                                                  delta=self.curr_angle)

            self.x_length = self.distance.measureLine(self.curr_geom.asPolygon()[0][0],
                                                      self.curr_geom.asPolygon()[0][1])
            self.y_length = self.distance.measureLine(self.curr_geom.asPolygon()[0][1],
                                                      self.curr_geom.asPolygon()[0][2])

            self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.xc, self.yc),
                                                                self.x_length, self.y_length)

        elif self.ini_geom is not None:
            if self.last_currpoint is None:
                self.last_currpoint = currpoint

            self.curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.ini_geom,
                                                                              QgsPointXY(self.xc, self.yc),
                                                                              currpoint, self.point_ini_rot,
                                                                              self.angle_ini_rot)

            self.last_currpoint = currpoint
            self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.xc, self.yc),
                                                                self.x_length, self.y_length)


        if self.curr_geom is not None:
            pt1 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])
            self.rb.setToGeometry(geom, None)

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.xc, self.yc = None, None
        if self.rb:
            self.rb.reset(True)
            self.rb.hide()
        self.rb = None

        self.canvas.refresh()

    def is_zoom_tool(self):
        """
        Tells if this tool is a zoom tool
        :return: False
        :rtype: bool
        """
        return False

    def is_transient(self):
        """
        Tells if this tool is transient
        :return: False
        :rtype: bool
        """
        return False

    def is_edit_tool(self):
        """
        Tells if this tool is an edit tool
        :return: True
        :rtype: bool
        """
        return True


# Tool class
class RectFromCenterFixedTool(QgsMapTool):
    msgbar = pyqtSignal(str)
    rbFinished = pyqtSignal(object)
    rb_reset_signal = pyqtSignal()

    def __init__(self, canvas, x_length=0.0, y_length=0.0):
        """
        Init of the object RectFromCenterFixedTool
        :param canvas: Canvas to draw on and get signals from
        :type canvas: QgsMapCanvas
        :param x_length: horizontal length of the rectangle
        :type x_length: float
        :param y_length: vertical length of the rectangle
        :type y_length: float
        """
        QgsMapTool.__init__(self, canvas)
        self.canvas = canvas
        self.n_points = 0
        self.rb = None
        self.mCtrl = None
        self.xc, self.yc, self.p2, self.x_p2, self.y_p2 = None, None, None, None, None
        crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
        self.distance = QgsDistanceArea()
        self.distance.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance.setEllipsoid(crs.ellipsoidAcronym())
        self.x_length = x_length
        self.y_length = y_length
        self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)
        # our own fancy cursor
        self.cursor = QCursor(QPixmap(["16 16 3 1",
                                       "      c None",
                                       ".     c #FF0000",
                                       "+     c #17a51a",
                                       "                ",
                                       "       +.+      ",
                                       "      ++.++     ",
                                       "     +.....+    ",
                                       "    +.  .  .+   ",
                                       "   +.   .   .+  ",
                                       "  +.    .    .+ ",
                                       " ++.    .    .++",
                                       " ... ...+... ...",
                                       " ++.    .    .++",
                                       "  +.    .    .+ ",
                                       "   +.   .   .+  ",
                                       "   ++.  .  .+   ",
                                       "    ++.....+    ",
                                       "      ++.++     ",
                                       "       +.+      "]))

        self.curr_geom = None
        self.last_currpoint = None
        self.curr_angle = 0.0
        self.total_angle = 0.0
        self.rectangle = Rectangle(crs)
        self.angle_ini_rot = 0.0
        self.ini_geom = None

    def keyPressEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is pressed, sets
        mCtrl to True and creates apoint and angle of rotation.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.mCtrl = True

            trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                      QgsCoordinateReferenceSystem(4326,
                                                                                   QgsCoordinateReferenceSystem.EpsgCrsId),
                                                      QgsProject.instance().transformContext())
            map_pos = self.toMapCoordinates(self.canvas.mouseLastXY())
            point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())
            self.point_ini_rot = point
            self.angle_ini_rot = self.curr_angle

    def keyReleaseEvent(self, event):
        """
        Overrides method keyPressEvent from QgsMapTool. If the control key is released,
        set mCtrl to false. If the scape key is released, cancel current drawings on canvas.
        :param event: key event
        :type event: QKeyEvent
        """
        if event.key() == Qt.Key_Control:
            self.mCtrl = False

        if event.key() == Qt.Key_Escape:
            self.n_points = 0
            self.xc, self.yc, self.p2 = None, None, None
            if self.rb:
                self.rb.reset(True)
            self.rb = None

            self.canvas.refresh()
            self.rb_reset_signal.emit()
            return

    def changegeomSRID(self, geom):
        """
        Transforms the geometry of the layer if it has a different SRID than the project
        :param geom: geometry to transform
        :type geom: QgsGeometry
        :return: geometry transformed, or the same if it's not necessary
        :rtype: QgsGeometry
        """
        layer = self.canvas.currentLayer()

        layerCRSSrsid = layer.crs().srsid()
        projectCRSSrsid = QgsMapSettings().destinationCrs().srsid()
        if layerCRSSrsid != projectCRSSrsid:
            g = QgsGeometry.fromPoint(geom)
            g.transform(QgsCoordinateTransform(projectCRSSrsid, layerCRSSrsid))
            retPoint = g.asPoint()
        else:
            retPoint = geom

        return retPoint

    def canvasPressEvent(self, event):
        """
        Overrides method canvasPressEvent from QgsMapTool. When a press event is registered from the canvas,
        this method starts the drawing of a rectangle if it is not already and finishes drawing if it was
        already started.
        :param event: click event
        :type event: QgsMapMouseEvent
        """
        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        map_pos = self.toMapCoordinates(event.pos())
        point = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        if self.n_points == 0:
            color = QColor(255, 0, 0, 128)
            if self.rb:
                self.rb.reset()
                self.rb = None
            self.rb = QgsRubberBand(self.canvas, True)
            self.rb.setColor(color)
            self.rb.setWidth(1)
            self.canvas.refresh()
            self.rb_reset_signal.emit()

            self.xc = point.x()
            self.yc = point.y()
            if self.x_length != 0:
                self.diagonal = sqrt(self.x_length / 2 * self.x_length / 2 + self.y_length / 2 * self.y_length / 2)
                self.p2 = self.distance.measureLineProjected(QgsPointXY(self.xc, self.yc), self.diagonal,
                                                             atan2(self.y_length / 2, self.x_length / 2))[1]
            self.n_points += 1

        elif self.n_points == 1:
            self.n_points = 0
            self.xc, self.yc, self.p2 = None, None, None
            self.last_currpoint = None
            self.rbFinished.emit(self.curr_geom)
            self.curr_geom = None
            self.curr_angle = 0.0
            self.total_angle = 0.0

    def canvasMoveEvent(self, event):
        """
        Overrides method canvasMoveEvent from QgsMapTool. This method draws a rectangle from a set point
        to the event point if that set point exists, else does nothing.
        :param event: move event
        :type event: QgsMapMouseEvent
        """
        if not self.rb or not self.xc or not self.yc: return

        trans_map_to_wgs = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                                  QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  QgsProject.instance().transformContext())

        trans_wgs_to_map = QgsCoordinateTransform(QgsCoordinateReferenceSystem(4326,
                                                                               QgsCoordinateReferenceSystem.EpsgCrsId),
                                                  self.canvas.mapSettings().destinationCrs(),
                                                  QgsProject.instance().transformContext())
        map_pos = self.toMapCoordinates(event.pos())
        currpoint = trans_map_to_wgs.transform(map_pos.x(), map_pos.y())

        self.msgbar.emit("Hold Ctrl to adjust track orientation. Click when finished.")

        if not self.mCtrl:
            if self.last_currpoint is None:
                self.last_currpoint = self.p2
                self.curr_geom = self.rectangle.get_rect_from_center(QgsPointXY(self.xc, self.yc),
                                                                     self.last_currpoint,)
                self.ini_geom = self.curr_geom

                self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.xc, self.yc),
                                                                    self.x_length, self.y_length)

        elif self.ini_geom is not None:
            if self.last_currpoint is None:
                self.last_currpoint = currpoint

            self.curr_geom, self.curr_angle = self.rectangle.get_rect_rotated(self.ini_geom,
                                                                              QgsPointXY(self.xc, self.yc),
                                                                              currpoint, self.point_ini_rot,
                                                                              self.angle_ini_rot)

            self.last_currpoint = currpoint
            self.curr_geom = self.rectangle.get_rect_projection(self.curr_geom, QgsPointXY(self.xc, self.yc),
                                                                self.x_length, self.y_length)

        if self.curr_geom is not None:
            pt1 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][0])
            pt2 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][1])
            pt3 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][2])
            pt4 = trans_wgs_to_map.transform(self.curr_geom.asPolygon()[0][3])
            geom = QgsGeometry.fromPolygonXY([[pt1, pt2, pt3, pt4]])

            self.rb.setToGeometry(geom, None)

    def activate(self):
        """
        Sets the custom cursor on canvas
        """
        self.canvas.setCursor(self.cursor)

    def deactivate(self):
        """
        Deletes started rectangles and closes all bands
        """
        self.n_points = 0
        self.xc, self.yc, self.x_p2, self.y_p2 = None, None, None, None
        if self.rb:
            self.rb.reset(True)
        self.rb = None

        self.canvas.refresh()

    def is_zoom_tool(self):
        """
        Tells if this tool is a zoom tool
        :return: False
        :rtype: bool
        """
        return False

    def is_transient(self):
        """
        Tells if this tool is transient
        :return: False
        :rtype: bool
        """
        return False

    def is_edit_tool(self):
        """
        Tells if this tool is an edit tool
        :return: True
        :rtype: bool
        """
        return True

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to periodically check for mission activation state.
"""
import logging
import subprocess

from PyQt5.QtCore import QObject, QTimer, pyqtSignal
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction, QMenu, QToolButton, QMessageBox, QDialog

from iquaview.src.cola2api import cola2_interface
from iquaview.src.mission.sftp_change_current_mission_dialog import SFTPChangeCurrentMissionDialog

logger = logging.getLogger(__name__)


class MissionActive(QObject):
    update_paused_list_signal = pyqtSignal()
    terminate_teleoperation_signal = pyqtSignal()

    def __init__(self, config, vehicledata, vehicleinfo, thrusters_status, keep_position_status, joystick_teleop_action,
                 parent=None):
        """
        Init of the object MissionActive
        :param config: iquaview configuration
        :type config: Config
        :param vehicledata: data from the vehicle
        :type vehicledata: VehicleData
        :param vehicleinfo: vehicle info
        :type vehicleinfo: VehicleInfo
        :param thrusters_status: class that check thrusters status
        :type thrusters_status: ThrustersStatus
        :param keep_position_status: class that check keep position status
        :type keep_position_status: KeepPositionStatus
        :param joystick_teleop_action: joystick teleoperation action
        :type joystick_teleop_action: QAction
        """
        super(MissionActive, self).__init__()
        self.config = config
        self.vehicle_data = vehicledata
        self.vehicle_info = vehicleinfo
        self.thrusters_status = thrusters_status
        self.keep_position_status = keep_position_status
        self.joystick_teleop_action = joystick_teleop_action
        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh_mission_status)
        self.status = None
        self.captain_status = None
        self.subscribed = False
        self.wifi_state = False
        self.parent = parent

        self.select_mission_dialog = None

        self.execute_mission_action = QAction(QIcon(":/resources/mActionExecuteMission.svg"), "Execute Mission", self)
        self.select_mission_action = QAction("Select Mission...", self)
        self.pause_mission_action = QAction(QIcon(":/resources/mActionPauseMission.svg"), "Pause Mission", self)
        self.disable_mission_action = QAction(QIcon(":/resources/mActionStopMission.svg"), "Disable Active Mission", self)

        self.execute_mission_action.triggered.connect(self.execute_last_mission)
        self.select_mission_action.triggered.connect(self.select_mission)
        self.pause_mission_action.triggered.connect(self.pause_mission)
        self.disable_mission_action.triggered.connect(self.disable_last_mission)

        self.execute_mission_menu = QMenu("Execute Mission")
        self.execute_mission_menu.setIcon(QIcon(":/resources/mActionExecuteMission.svg"))
        self.execute_mission_menu.addAction(self.execute_mission_action)
        self.execute_mission_menu.addSeparator()
        self.execute_mission_menu.addAction(self.select_mission_action)
        self.execute_mission_menu.addSeparator()
        self.exec_paused_list = self.execute_mission_menu.addMenu("Resume Mission...")

        self.stop_mission_menu = QMenu("Stop Mission")
        self.stop_mission_menu.setIcon(QIcon(":/resources/mActionStopMission.svg"))
        self.stop_mission_menu.addAction(self.disable_mission_action)
        self.stop_mission_menu.addSeparator()
        self.stop_paused_list = self.stop_mission_menu.addMenu("Disable Mission...")

        self.execute_mission_bt = QToolButton()
        self.execute_mission_bt.setIcon(QIcon(":/resources/mActionExecuteMission.svg"))
        self.execute_mission_bt.setMenu(self.execute_mission_menu)
        self.execute_mission_bt.setPopupMode(QToolButton.InstantPopup)

        self.stop_mission_bt = QToolButton()
        self.stop_mission_bt.setIcon(QIcon(":/resources/mActionStopMission.svg"))
        self.stop_mission_bt.setMenu(self.stop_mission_menu)
        self.stop_mission_bt.setPopupMode(QToolButton.InstantPopup)

        self.update_paused_list_signal.connect(self.update_list)

    def get_execute_mission_menu(self):
        """
        Return execute mission menu
        :return: execute mission  menu
        :rtype: QMenu
        """
        return self.execute_mission_menu

    def get_stop_mission_menu(self):
        """
        Return stop mission menu
        :return: stop mission  menu
        :rtype: QMenu
        """
        return self.stop_mission_menu

    def get_execute_mission_toolbutton(self):
        """
        Return execute mission toolbutton
        :return: execute mission toolbutton
        :rtype: QToolButton
        """
        return self.execute_mission_bt

    def get_stop_mission_toolbutton(self):
        """
        Return stop mission toolbutton
        :return: stop mission toolbutton
        :rtype: QToolButton
        """
        return self.stop_mission_bt

    def get_pause_mission_action(self):
        """
        Return pause mission action
        :return: pause mission action
        :rtype: QToolButton
        """
        return self.pause_mission_action

    def update_mission_status(self):
        """ set subscribed status to true and start timer"""
        self.subscribed = True
        self.timer.start(1000)

    def refresh_mission_status(self):
        """Refresh mission status. Timer to get every second mission status. """

        if self.subscribed:
            mission_status = self.vehicle_data.get_captain_state()
            #back compatibility
            if mission_status is None:
                mission_status = self.vehicle_data.get_mission_active()

            if mission_status is not None:
                # if new status is different that old status
                if mission_status != self.status:
                    self.status = mission_status
                    # send signal to notify that mission status
                    self.change_icon_mission()

            captain_status = self.vehicle_data.get_captain_status()
            if captain_status is not None:

                if self.captain_status is not None:
                    if len(captain_status['loaded_missions']) != len(self.captain_status['loaded_missions']):
                        if self.captain_status != captain_status:
                            self.captain_status = captain_status
                        self.update_paused_list_signal.emit()
                    else:
                        for item in captain_status['loaded_missions']:
                            ok = False
                            for item_two in self.captain_status['loaded_missions']:
                                if item['name'] == item_two['name']:
                                    ok = True
                                    break

                            if not ok:
                                if self.captain_status != captain_status:
                                    self.captain_status = captain_status
                                self.update_paused_list_signal.emit()
                                break

                else:
                    self.captain_status = captain_status
                    self.update_paused_list_signal.emit()


    def get_mission_active(self):
        """
        :return: mission active state
        :rtype: bool
        """
        is_active = False

        mission_status = self.vehicle_data.get_captain_state()
        #back compatibility
        if mission_status is None:
            is_active = self.status
        else:
            if mission_status == 2:
                is_active = True

        return is_active

    def check_disk_capacity(self):
        """
        check the auv disk capacity
        :return: return True if (used disk)<75 or the user wants to continue, otherwise False
        :type: bool
        """
        try:
            user = self.vehicle_info.get_vehicle_user()
            server = self.vehicle_info.get_vehicle_ip()

            disk_usage_command = "ssh " + "{}@{}".format(user,
                                                         server) + " df --total  | grep total | grep -"
            ps = subprocess.Popen(disk_usage_command, shell=True, stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)
            output = ps.communicate()[0].decode()
            for line in output.split('\n'):
                if "total" in line:
                    values = line.split()
                    free_space = values[3]
                    disk_usage, __ = values[4].split('%')

                    free_space_gb = int(free_space) / (1024*1024)

                    if int(disk_usage) > 75:
                        reply = QMessageBox.warning(None,
                                                    "Enable mission warning",
                                                    "{:.2f} GB of disk space left. \n".format(free_space_gb) +
                                                    "{:s} % of the disk has been used. \n".format(disk_usage) +
                                                    "Be aware that you can run out of disk space within the mission. "
                                                    "Do you want to continue?",
                                                    QMessageBox.Yes, QMessageBox.No)
                        if reply == QMessageBox.Yes:
                            return True
                        else:
                            return False
                    else:
                        return True
            raise Exception("could not parse disk check command")
        except Exception as e:
            logger.error("Disk capacity could not be checked")
            reply = QMessageBox.warning(None,
                                        "Enable mission warning",
                                        "Disk capacity could not be checked: {} \n".format(e.args[0]) +
                                        "Be aware that you can run out of disk space within the mission. Do you want to continue?",
                                        QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                return True
            else:
                return False

    def execute_last_mission(self):
        """Call execute mission method without parameters, to execute last mission xml"""

        self.execute_mission()

    def execute_mission(self, mission_name=""):
        """
        Execute a mission sent to the AUV.
        :param mission_name: name of the mission, empty by default
        :type mission_name: str
        """

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()

        if (self.thrusters_status.get_thrusters_enabled()) \
                and (not self.keep_position_status.get_keep_position_enabled()):
            if self.joystick_teleop_action.isChecked():
                logger.info("Stopping joystick connection")
                self.terminate_teleoperation_signal.emit()

            enable_mission = self.vehicle_data.get_enable_mission_service()

            if enable_mission is not None and self.check_disk_capacity():

                    response = cola2_interface.send_mission_service(ip, port,
                                                        vehicle_namespace + enable_mission, mission_name)
                    try:
                        if not response['values']['success']:
                            QMessageBox.critical(None,
                                                 "Execute mission failed",
                                                 response['values']['message'],
                                                 QMessageBox.Close)
                            self.change_icon_mission()

                    # back compatibility
                    except Exception as e:
                        logger.warning("The execute mission service response can not be read")
            else:
                QMessageBox.critical(None,
                                     "Enable mission error",
                                     "The service 'Enable mission' could not be sent.",
                                     QMessageBox.Close)

            # self.mission_sts.start_check_mission_start()
        else:
            # thrusters disabled
            if not self.thrusters_status.get_thrusters_enabled():
                QMessageBox.warning(None,
                                    "Mission start",
                                    "Mission cannot start. Thrusters are disabled.",
                                    QMessageBox.Close)
            # keep position enabled
            else:
                QMessageBox.warning(None,
                                    "Mission start",
                                    "Mission cannot start. Keep position are enabled.",
                                    QMessageBox.Close)

    def select_mission(self):
        """Select mission from AUV"""
        user = self.vehicle_info.get_vehicle_user()
        ip = self.vehicle_info.get_vehicle_ip()
        remote_ini_path = self.vehicle_info.get_remote_vehicle_package() + "/missions"
        local_ini_path = self.config.settings['last_open_project']

        if self.select_mission_dialog is None or self.select_mission_dialog.isHidden():
            if local_ini_path is None or local_ini_path == "":
                QMessageBox.warning(None, "No open project", "Open a project to start the sftp connection")
            else:
                self.select_mission_dialog = SFTPChangeCurrentMissionDialog(user, ip, local_ini_path, remote_ini_path, parent=self.parent)
                result = self.select_mission_dialog.exec_()
                if result == QDialog.Accepted:
                    self.execute_mission(self.select_mission_dialog.get_filename())

    def disable_last_mission(self):
        """ Disable last mission sent to the AUV. """

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()

        disable_mission = self.vehicle_data.get_disable_mission_service()
        if disable_mission is not None:
            response = cola2_interface.send_mission_service(ip, port,
                                                            vehicle_namespace + disable_mission, "")
            try:
                if not response['values']['success']:
                    QMessageBox.critical(None,
                                         "Disable mission failed",
                                         response['values']['message'],
                                         QMessageBox.Close)
                    self.change_icon_mission()

            # back compatibility
            except Exception as e:
                logger.warning("The disable mission response can not be read")
            #/girona500/captain/disable_mission
        else:
            QMessageBox.critical(None,
                                 "Disable mission error",
                                 "The service 'Disable mission' could not be sent.",
                                 QMessageBox.Close)
        # self.mission_sts.start_check_mission_stop()

    def pause_mission(self):
        """ Pause current running mission. """

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()

        if self.get_mission_active():
            pause_mission = self.vehicle_data.get_pause_mission_service()

            response = cola2_interface.send_trigger_service(ip, port, vehicle_namespace + pause_mission)
            try:

                if not response['values']['success']:
                    QMessageBox.critical(None,
                                         "Pause mission failed",
                                         response['values']['message'],
                                         QMessageBox.Close)
                    self.change_icon_mission()

            # back compatibility
            except Exception as e:
                logger.warning("The pause mission service response can not be read")

    def resume_mission(self):
        """ Resume the selected mission. """
        sender = self.sender()

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()

        if not self.get_mission_active():
            resume_mission = self.vehicle_data.get_resume_mission_service()
            response = cola2_interface.send_mission_service(ip, port, vehicle_namespace + resume_mission, sender.text())
            try:

                if not response['values']['success']:
                    QMessageBox.critical(None,
                                         "Resume mission failed",
                                         response['values']['message'],
                                         QMessageBox.Close)
                    self.change_icon_mission()

            # back compatibility
            except Exception as e:
                logger.warning("The resume mission service response can not be read")

    def disable_mission(self):
        """ Disables selected mission"""
        sender = self.sender()

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()

        disable_mission = self.vehicle_data.get_disable_mission_service()
        response = cola2_interface.send_mission_service(ip, port, vehicle_namespace + disable_mission, sender.text())
        try:

            if not response['values']['success']:
                QMessageBox.critical(None,
                                     "Disable mission failed",
                                     response['values']['message'],
                                     QMessageBox.Close)
                self.change_icon_mission()

            # back compatibility
        except Exception as e:
            logger.warning("The disable mission service response can not be read")

    def update_list(self):
        self.exec_paused_list.clear()
        self.stop_paused_list.clear()

        for item in self.captain_status['loaded_missions']:
            name = item['name']
            exec_act = self.exec_paused_list.addAction(name)
            exec_act.triggered.connect(self.resume_mission)
            disable_act = self.stop_paused_list.addAction(name)
            disable_act.triggered.connect(self.disable_mission)

        if not self.stop_paused_list.isEmpty():
            self.stop_mission_bt.setEnabled(self.wifi_state)
            self.stop_mission_menu.setEnabled(self.wifi_state)
        else:
            self.stop_mission_bt.setEnabled(False)
            self.stop_mission_menu.setEnabled(False)


    def change_icon_mission(self):
        """ Change icon of the self.execute_mission_action"""
        if self.get_mission_active():
            self.execute_mission_bt.setEnabled(False)
            self.execute_mission_menu.setEnabled(False)

            self.pause_mission_action.setEnabled(self.wifi_state)
            self.stop_mission_bt.setEnabled(self.wifi_state)
            self.stop_mission_menu.setEnabled(self.wifi_state)

        else:

            self.execute_mission_bt.setEnabled(self.wifi_state)
            self.execute_mission_menu.setEnabled(self.wifi_state)

            self.pause_mission_action.setEnabled(False)
            if not self.stop_paused_list.isEmpty():
                self.stop_mission_bt.setEnabled(self.wifi_state)
                self.stop_mission_menu.setEnabled(self.wifi_state)

    def change_enable(self, state):
        self.wifi_state = state
        self.change_icon_mission()

    def disable(self):
        self.execute_mission_menu.setEnabled(False)
        self.stop_mission_menu.setEnabled(False)
        self.execute_mission_bt.setEnabled(False)
        self.stop_mission_bt.setEnabled(False)

    def disconnect(self):
        """ disconnect and stop timer"""
        self.subscribed = False
        self.status = None
        self.change_icon_mission()
        self.timer.stop()

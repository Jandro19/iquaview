"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QDialog

from iquaview.src.ui.ui_sftp_transfer_progress import Ui_transfer_progress


class SFTPTransferProgressDialog(QDialog, Ui_transfer_progress):
    cancel_transfer_signal = pyqtSignal()

    def __init__(self, n_files, parent=None):
        super(SFTPTransferProgressDialog, self).__init__(parent)
        self.setupUi(self)
        self.progress_bar.setValue(0)
        self.curr_file_label.setText("1")
        self.n_files_label.setText(str(n_files))
        self.n_files = n_files
        self.cancel_button.clicked.connect(self.cancel_transfer_signal.emit)

    def update_values(self, v1, v2):
        total = v2 / 1000
        transferred = v1 / 1000
        progress = 100 * transferred / total
        self.curr_kbytes_label.setText("{:.0F}".format(transferred))
        self.total_kbytes_label.setText("{:.0F}".format(total))
        self.progress_bar.setValue(progress)
        if progress == 100:
            curr_file = int(self.curr_file_label.text())
            if curr_file < self.n_files:
                curr_file += 1
                self.curr_file_label.setText(str(curr_file))

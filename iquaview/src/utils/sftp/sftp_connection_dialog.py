"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
Creates an SFTP connection with the AUV and a dialog in order to transfer files from and to the UAV.
"""

import logging
import subprocess
import pysftp
from datetime import datetime
from os import path, stat, listdir
from stat import S_ISDIR, S_ISREG
from paramiko import SSHException

from PyQt5.QtCore import QItemSelectionModel, Qt, QPoint, QTimer
from PyQt5.QtGui import QStandardItem, QIcon, QPixmap, QColor
from PyQt5.QtWidgets import QDialog, QStyle, QHeaderView, QMessageBox, QMenu, QInputDialog, QLineEdit
from qgis.core import QgsApplication

from iquaview.src import resources_qgis

from iquaview.src.ui.ui_sftp_connection_dialog import Ui_SFTP_Connection_Dialog
from iquaview.src.utils import busywidget
from iquaview.src.utils.sftp.sftp_folder import SFTPFolder
from iquaview.src.utils.sftp.sftp_help_dialog import SFTPHelpDialog
from iquaview.src.utils.sftp.sftp_table_view import SFTPTableView
from iquaview.src.utils.sftp.sftp_transfer_progress_dialog import SFTPTransferProgressDialog
from iquaview.src.utils.sftp.sftp_tree_view import SFTPTreeView

logger = logging.getLogger(__name__)


class SFTPConnectionDialog(QDialog, Ui_SFTP_Connection_Dialog):

    def __init__(self, user, ip, local_ini_path, remote_ini_path,
                 file_extension=None, filter_by_extension=False, compare_with_md5=False, init_models=True, parent=None):
        """
        Init of the object SFTPConnectionDialog
        :param user: User that will establish connection
        :type user: str
        :param ip: IP address of the remote machine
        :type ip: str
        :param local_ini_path: Initial path of the local machine
        :type local_ini_path: str
        :param remote_ini_path: Initial path of the remote machine
        :type remote_ini_path: str
        :param file_extension: Extension of the allowed files to transfer, all files will be transferable if none
        :type file_extension: str
        :param filter_by_extension: Determines if a filter needs to be applied to filter by file_extension
        :type filter_by_extension: bool
        :param compare_with_md5: Determines if a comparison between local and remote files using md5 will be used.
        :type compare_with_md5: bool
        :param init_models: Determines if the trees will be explored and loaded in the initial models
        :type init_models: bool
        """

        super(SFTPConnectionDialog, self).__init__(parent, Qt.Dialog)
        self.setupUi(self)
        self.sftp_connection = None
        self.transfer_progress_dialog = None
        self.date_format = "%d/%m/%y \t %H:%M:%S"

        if file_extension is not None:
            self.allowed_files_label.setText(file_extension)
            self.filter_by_extension = filter_by_extension
            self.compare_with_md5 = compare_with_md5
        else:
            self.allowed_files_label.setText("All")
            self.filter_by_extension = False
            self.compare_with_md5 = False

        self.file_extension = file_extension

        self.minimum_height_expanded = 605
        self.minimum_height_collapsed = 450

        self.local_file_list = list()
        self.remote_file_list = list()

        self.remote_user = user
        self.remote_ip = ip
        self.remotepath = remote_ini_path

        # Fix remote pathing
        if remote_ini_path[:2] == "~/":
            self.remotepath = "/home/"+user+remote_ini_path[1:]
        elif remote_ini_path[:6] != "/home/":
            self.remotepath = "/home/"+user+"/"+remote_ini_path

        list_paths = list(pysftp.path_advance(self.remotepath))
        self.base_remote_path = list_paths[0]
        list_paths = list(pysftp.path_advance(local_ini_path))
        self.localpath = list_paths[len(list_paths)-2]  # get the directory where the project is located
        if path.exists(self.localpath+'/missions'):
            self.localpath += '/missions'
        self.base_local_path = list_paths[0]

        # Defining icons for the Items in the views
        self.closed_folder_icon = self.style().standardIcon(getattr(QStyle, "SP_DirClosedIcon"))
        self.open_folder_icon = self.style().standardIcon(getattr(QStyle, "SP_DialogOpenButton"))
        self.file_icon = self.style().standardIcon(getattr(QStyle, "SP_FileIcon"))

        # Defining marker icons
        pixmap = QPixmap(10, 10)
        pixmap.fill(QColor(255, 100, 100))
        self.file_not_found_marker_icon = QIcon(pixmap)
        pixmap.fill(QColor(0, 125, 200))
        self.newer_file_marker_icon = QIcon(pixmap)
        pixmap.fill(QColor(255, 255, 100))
        self.older_file_marker_icon = QIcon(pixmap)
        pixmap.fill(QColor(100, 255, 100))
        self.same_file_marker_icon = QIcon(pixmap)

        pixmap_size = 20

        # Coloring legend
        if file_extension is not None and self.compare_with_md5:
            self.same_file_label.setPixmap(self.same_file_marker_icon.pixmap(pixmap_size))
            self.not_found_label.setPixmap(self.file_not_found_marker_icon.pixmap(pixmap_size))
            self.older_file_label.setPixmap(self.older_file_marker_icon.pixmap(pixmap_size))
            self.newer_file_label.setPixmap(self.newer_file_marker_icon.pixmap(pixmap_size))
        else:
            self.legend_widget.hide()

        # Defining button icons
        help_btn_icon = self.style().standardIcon(getattr(QStyle, "SP_MessageBoxInformation"))
        self.help_btn.setIcon(help_btn_icon)

        # Creating tree views and  table views
        self.local_treeView = SFTPTreeView()
        self.remote_treeView = SFTPTreeView()
        self.local_tableView = SFTPTableView()
        self.remote_tableView = SFTPTableView()

        # Getting models from views
        self.local_tree_model = self.local_treeView.model()
        self.remote_tree_model = self.remote_treeView.model()
        self.local_table_model = self.local_tableView.model()
        self.remote_table_model = self.remote_tableView.model()

        # Naming tree view models
        self.local_tree_model.setHorizontalHeaderItem(0, QStandardItem("Local tree view"))
        self.remote_tree_model.setHorizontalHeaderItem(0, QStandardItem("AUV tree view"))

        # Replace tree view placeholders with custom tree view widgets
        ph1 = self.local_groupbox_layout.replaceWidget(self.local_tree_placeholder, self.local_treeView)
        ph2 = self.remote_groupbox_layout.replaceWidget(self.remote_tree_placeholder, self.remote_treeView)
        self.local_tree_placeholder.setParent(None)  # deletes the widget
        self.remote_tree_placeholder.setParent(None)

        # Replace table view placeholders with custom table view widgets
        ph1 = self.table_view_h_layout.replaceWidget(self.local_table_placeholder, self.local_tableView)
        ph2 = self.table_view_h_layout.replaceWidget(self.remote_table_placeholder, self.remote_tableView)
        self.local_table_placeholder.setParent(None)
        self.remote_table_placeholder.setParent(None)

        # setup connections
        self.remote_tableView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.remote_tableView.customContextMenuRequested.connect(self.create_table_context_menu)
        self.remote_tableView.pressed.connect(self.clear_selection_other_table)
        self.local_tableView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.local_tableView.customContextMenuRequested.connect(self.create_table_context_menu)
        self.local_tableView.pressed.connect(self.clear_selection_other_table)

        # button connections
        self.help_btn.pressed.connect(self.show_help)
        self.transfer_to_remote_btn.pressed.connect(self.transfer_local_selected_files_to_remote)
        self.transfer_to_local_btn.pressed.connect(self.transfer_remote_selected_files_to_local)

        # start connection and load default paths
        self.start_sftp_connection()

        # Read initial folders
        self.initialising = True
        if self.sftp_connection is not None:
            if init_models:
                self.load_init_models()
        else:
            self.close()

        # connect interactivity signals (expand and show a folder)
        self.remote_treeView.expanded.connect(self.expand_remote_folder_with_index)
        self.remote_treeView.clicked.connect(self.show_remote_selection)
        self.local_treeView.expanded.connect(self.expand_local_folder_with_index)
        self.local_treeView.clicked.connect(self.show_local_selection)

        # connect groupbox signals
        self.remote_GroupBox.collapsedStateChanged.connect(self.expand_collapse_trees)
        self.local_GroupBox.collapsedStateChanged.connect(self.expand_collapse_trees)

    def start_sftp_connection(self):
        """
        Starts SFTP connection with the vehicle
        """
        t = busywidget.TaskThread(pysftp.Connection, self.remote_ip, self.remote_user)
        QTimer.singleShot(30000, t.terminate)
        bw = busywidget.BusyWidget("Connecting...", t)
        bw.on_start()
        bw.exec_()
        if t.has_error():
            QMessageBox.critical(None, "Connection error",
                                 "Error trying to connect to the AUV. Check connection settings.\n"
                                 "{}".format(t.get_error()))

        elif bw.get_info() is None:
            QMessageBox.warning(None, "Connection error", "Connection was closed unexpectedly.")
            return

        else:
            self.sftp_connection = bw.get_info()

    def exec_(self):
        if self.sftp_connection is None:
            self.close()
        else:
            return super().exec_()

    def expand_remote_folder_with_index(self, index):
        """
        Expands the folder specified by index located in the remote tree model. Creates a busy widget to show loading
        :param index: index of the folder to be expanded
        :type index: QModelIndex
        """
        item = self.remote_tree_model.itemFromIndex(index)
        item_path = item.get_path(base_path=self.base_remote_path)
        flags = {"abort_flag": False}
        t = busywidget.TaskThread(self.explore_remote_tree, self.sftp_connection, [item_path], flags=flags)
        bw = busywidget.BusyWidget("Expanding folder...", t)
        bw.on_start()
        bw.exec_()

        if t.has_error():
            QMessageBox.critical(None, "Connection error", "{}".format(t.get_error()))

        elif bw.get_info() is None:
            QMessageBox.warning(None, "Connection error", "Connection was closed unexpectedly.")
            return

        else:
            folder_list = bw.get_info()
            for folder in folder_list:
                self.remote_treeView.add_folder(folder)

        self.remote_treeView.expand_folder(index)

    def expand_local_folder_with_index(self, index):
        """
        Expands the folder specified by index located in the local tree model
        :param index: index of the folder to be expanded
        :type index: QModelIndex
        """
        item = self.local_tree_model.itemFromIndex(index)
        item_path = item.get_path(base_path=self.base_local_path)
        logger.debug("expanding " + item_path)
        try:
            self.expand_local_folder_with_path(item_path)
        except IOError as ex:
            logger.error(ex)
            QMessageBox.critical(None, "Connection error", "{}".format(ex))
        self.local_treeView.expand_folder(index)

    def explore_remote_tree(self, sftp_connection, list_remote_paths, flags=None):
        """
        Explores and expands the remote tree to all the remote paths specified in the list
        :param sftp_connection: connection used to explore
        :type sftp_connection: pysftp.Connection
        :param list_remote_paths: list of paths to expand
        :type list_remote_paths: list
        :param flags: Dict with flags, will use 'abort_flag' to abort the operations
        :type flags: dict
        :return: a list of all the folders found in the exploration
        :rtype: list
        """
        if flags is None:
            flags = {}
        folder_list = list()
        for folder_path in list_remote_paths:
            try:
                if "abort_flag" in flags and flags["abort_flag"]:
                    return None

                for entry in sftp_connection.listdir(folder_path):
                    pathname = path.join(folder_path, entry)
                    try:
                        if "abort_flag" in flags and flags["abort_flag"]:
                            return None

                        mode = sftp_connection.stat(pathname).st_mode
                        if S_ISDIR(mode):
                            folder_list.append(pathname)

                    except Exception as e:
                        logger.warning("Couldn't read file " + pathname + " {}".format(e))
            except FileNotFoundError as e:
                logger.info("Can't expand "+folder_path+" {}".format(e))
                break

        return folder_list

    def expand_local_folder_with_path(self, folder_path):
        """
        Expands the folder specified by the folder path, located in the local tree model. Adds the folders inside it
        to the local tree view
        :param folder_path: path of the folder
        :type folder_path: str
        """
        try:
            for entry in listdir(folder_path):
                pathname = path.join(folder_path, entry)
                try:
                    mode = stat(pathname).st_mode
                    if S_ISDIR(mode):
                        self.local_treeView.add_folder(pathname)
                except Exception as e:
                    logger.warning("Couldn't read file "+pathname+" {}".format(e))
        except FileNotFoundError as e:
            logger.info("Can't expand "+folder_path+" {}".format(e))

    def add_file_to_remote_table_model(self, pathname):
        """
        Adds the file specified by the pathname to the remote table model
        :param pathname: path name of the file
        :type pathname: str
        """
        if self.filter_by_extension:  # Apply filter by extension if needed
            file_name = path.basename(pathname)
            if file_name[-len(self.file_extension):] != self.file_extension:
                return
        info = self.sftp_connection.stat(pathname)
        size = info.st_size
        date = datetime.fromtimestamp(info.st_mtime)
        self.remote_tableView.add_file(pathname, size, date)
        self.remote_file_list.append(path.basename(pathname))

    def add_file_to_local_table_model(self, pathname):
        """
        Adds the file specified by the pathname to the local table model
        :param pathname: path name of the file
        :type pathname: str
        """
        if self.filter_by_extension:  # Apply filter by extension if needed
            file_name = path.basename(pathname)
            if file_name[-len(self.file_extension):] != self.file_extension:
                return
        info = stat(pathname)
        size = info.st_size
        date = datetime.fromtimestamp(info.st_mtime)
        self.local_tableView.add_file(pathname, size, date)
        self.local_file_list.append(path.basename(pathname))

    def load_init_models(self):
        """
        Initializes the table views and tree views for the first time
        """
        # expand paths from remote tree view. A busy widget is created to show the loading
        list_remote_paths = list(pysftp.path_advance(self.remotepath))
        flags = {"abort_flag": False}
        t = busywidget.TaskThread(self.explore_remote_tree, self.sftp_connection, list_remote_paths, flags=flags)
        bw = busywidget.BusyWidget("Exploring remote tree...", t)
        bw.on_start()
        bw.exec_()

        if t.has_error():
            QMessageBox.warning(None, "Connection error",
                                "Error trying to connect to the AUV. Check connection settings.\n"
                                "{}".format(t.get_error()))
            return

        elif bw.get_info() is None:
            QMessageBox.warning(None, "Connection error", "Connection was closed unexpectedly.")
            return

        else:
            folder_list = bw.get_info()
            for folder in folder_list:
                self.remote_treeView.add_folder(folder)

        # expand paths from local tree view
        list_local_paths = list(pysftp.path_advance(self.localpath))
        for p in list_local_paths:
            self.expand_local_folder_with_path(p)

        # select the deepest folder in the remote path from remote tree view
        list_remote_paths.remove(list_remote_paths[0])
        item = self.remote_tree_model
        for p in list_remote_paths:
            if item is not None:
                item = item.get_folder(path.basename(p))
        if type(item) == SFTPFolder:
            self.remote_treeView.selectionModel().select(item.index(), QItemSelectionModel.Select)
            self.remote_treeView.scrollTo(item.index())

        # select the deepest folder in the local path from local tree view
        list_local_paths.remove(list_local_paths[0])
        item = self.local_tree_model
        for p in list_local_paths:
            if item is not None:
                item = item.get_folder(path.basename(p))
        if type(item) == SFTPFolder:
            self.local_treeView.selectionModel().select(item.index(), QItemSelectionModel.Select)
            self.local_treeView.scrollTo(item.index())

        # show selected items in their respective tables
        selected_indexes = self.remote_treeView.selectedIndexes()
        if len(selected_indexes) > 0:
            self.show_remote_selection(selected_indexes[0])
        else:
            self.remote_GroupBox.setTitle("Incorrect Vehicle Package")
        selected_indexes = self.local_treeView.selectedIndexes()
        if len(selected_indexes) > 0:
            self.show_local_selection(selected_indexes[0])

        if self.file_extension is not None and self.compare_with_md5:
            self.update_diff_colors()
        self.adjustSize()
        self.initialising = False

    def load_remote_folder(self, sftp_connection, folder_path, flags=None):
        """
        Reads all entries found in the specified folder path and saves them in a folder list and a file list
        :param sftp_connection:
        :type sftp_connection:
        :param folder_path: path where the folder is located
        :type folder_path: str
        :param flags: Dict with flags, will use 'abort_flag' to abort the operations
        :type flags: dict
        :return: Returns both the folder and the file lists
        :rtype: list, list
        """
        if flags is None:
            flags = {}
        folder_list = list()
        file_list = list()
        for entry in sftp_connection.listdir(folder_path):
            pathname = path.join(folder_path, entry)
            try:
                if "abort_flag" in flags and flags["abort_flag"]:
                    return None
                mode = sftp_connection.stat(pathname).st_mode
                if S_ISDIR(mode):
                    folder_list.append(pathname)
                elif S_ISREG(mode):
                    file_list.append(pathname)
            except Exception as e:
                logger.warning("Couldn't read file " + pathname + " {}".format(e))

        return folder_list, file_list

    def show_remote_selection(self, index):
        """
        Creates a busy widget to show loading while reading the contents of the folder specified by index
        and updates the remote table model with them
        :param index: index of the folder
        :type index: QModelIndex
        """
        # delete all rows currently in the model
        self.remote_table_model.remove_all_rows()

        # empty remote file list
        self.remote_file_list.clear()

        # get the selected item and its path
        selected_item = self.remote_tree_model.itemFromIndex(index)

        if type(selected_item) == SFTPFolder:
            item_path = selected_item.get_path(base_path=self.base_remote_path)
            self.selected_remote_path = item_path
            self.remote_GroupBox.setTitle(self.selected_remote_path)

            # Inspec all the directory and add files and folders to the table
            flags = {"abort_flag": False}
            t = busywidget.TaskThread(self.load_remote_folder, self.sftp_connection, item_path, flags=flags)
            bw = busywidget.BusyWidget("Loading folder contents...", t)
            bw.on_start()
            bw.exec_()

            if t.has_error():
                QMessageBox.critical(None, "Connection error", "{}".format(t.get_error()))

            elif bw.get_info() is None:
                QMessageBox.warning(None, "Connection error", "Connection was closed unexpectedly.")
                return

            else:
                # Get items and add them to the table view
                folder_list, file_list = bw.get_info()
                for folder in folder_list:
                    self.remote_tableView.add_folder(folder)
                for file in file_list:
                    self.add_file_to_remote_table_model(file)

                # resize model headers
                self.remote_tableView.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
                self.remote_tableView.horizontalHeader().setStretchLastSection(True)

                # sort all the items of the remote table by name
                self.remote_tableView.sort_table_by_name()

                # resize marker header
                self.remote_tableView.horizontalHeader().resizeSection(0, 15)

                if self.file_extension is not None and self.compare_with_md5:
                    self.remote_md5_list = self.get_remote_md5_list()
                    if not self.initialising:
                        self.update_diff_colors()

    def show_local_selection(self, index):
        """
        Reads the contents of the folder specified by index from the local tree view
        and updates the local table model with them
        :param index: index of the folder
        :type index: QModelIndex
        """
        # delete all rows currently in the model
        self.local_table_model.remove_all_rows()

        # empty local file list
        self.local_file_list.clear()

        # get the selected item and its path
        selected_item = self.local_tree_model.itemFromIndex(index)

        if type(selected_item) == SFTPFolder:
            item_path = selected_item.get_path(base_path=self.base_local_path)
            self.selected_local_path = item_path
            self.local_GroupBox.setTitle(self.selected_local_path)

            # Inspec all the directory and add files and folders to the table
            try:
                for entry in listdir(item_path):
                    pathname = path.join(item_path, entry)
                    try:
                        mode = stat(pathname).st_mode
                        if S_ISDIR(mode):
                            self.local_tableView.add_folder(pathname)
                        elif S_ISREG(mode):
                            self.add_file_to_local_table_model(pathname)
                    except Exception as e:
                        logger.warning("Couldn't read file " + pathname + " {}".format(e))

                # resize model headers
                self.local_tableView.horizontalHeader().resizeSections(QHeaderView.ResizeToContents)
                self.local_tableView.horizontalHeader().setStretchLastSection(True)

                # sort all the items of the local table by name
                self.local_tableView.sort_table_by_name()
            except IOError as ex:
                logger.error(ex)
                QMessageBox.critical(None, "Connection error", "{}".format(ex))

            # resize marker header
            self.local_tableView.horizontalHeader().resizeSection(0, 15)

            if self.file_extension is not None and self.compare_with_md5:
                self.local_md5_list = self.get_local_md5_list()
                if not self.initialising:
                    self.update_diff_colors()

    def get_remote_md5_list(self):
        """
        Checks the md5 sum of all the files with the specified extension in of the selected remote folder and returns
        them in a list of pairs where the first element is the md5 sum and the second element is the name of the filee
        :return: list of pairs
        :rtype: list
        """
        cmd = "md5sum \""+self.selected_remote_path+"\"/*"+self.file_extension
        md5_list = list()
        try:
            t = busywidget.TaskThread(self.sftp_connection.execute, cmd)
            bw = busywidget.BusyWidget("Checking remote files' md5", task=t)
            bw.on_start()
            bw.exec_()

            ret_list = bw.get_info()

            if ret_list is not None:
                logger.debug("Checking md5 from remote files")
                if str.encode("No such file or directory") in ret_list[0]:
                    logger.info("No xml files in the selected remote folder")
                    return md5_list

                for row in ret_list:
                    row = row[:-1]  # remove '\n' from the end of the path
                    str_row = row.decode("utf-8")
                    pair = str_row.split("  ")
                    pair[1] = path.basename(pair[1])
                    md5_list.append(pair)

                if not t.isFinished():
                    t.wait()
            else:
                QMessageBox.warning(None, "Connection error",
                                    "Connection was closed unexpectedly. Remote md5 could not be checked")

        except Exception as e:
            # something went wrong
            logger.error("Error checking md5 of the remote files. {}".format(e))

        return md5_list

    def get_local_md5_list(self):
        """
        Checks the md5 sum of all the files with the specified extension in of the selected local folder and returns
        them in a list of pairs where the first element is the md5 sum and the second element is the name of the file
        :return: list of pairs
        :rtype: list
        """
        cmd = "md5sum \""+self.selected_local_path+"\"/*"+self.file_extension
        md5_list = list()
        try:
            t = busywidget.CmdThread(cmd)
            bw = busywidget.BusyWidget("Checking local files' md5", task=t)
            bw.on_start()
            bw.exec_()

            ret = bw.get_info()

            if ret is not None:
                str_ret = ret.decode()

                logger.debug("Checking md5 from local files")
                if len(str_ret) == 0:
                    logger.info("No xml files in the selected local folder")
                # convert list of bytes to str and parse return
                for row in str_ret.split('\n'):
                    if row != '':
                        pair = row.split("  ")
                        if len(pair) == 2:
                            pair[1] = path.basename(pair[1])
                            md5_list.append(pair)

                if not t.isFinished():
                    t.wait()

            else:
                QMessageBox.warning(None, "Connection error",
                                    "Connection was closed unexpectedly. Local md5 could not be checked")

        except subprocess.CalledProcessError as e:
            logger.error("Error checking md5 from local files. {}".format(e.stderr))
        return md5_list

    def update_diff_colors(self):
        """
        This method reads the content of the remote and local table rows and depending on the md5 sum and the
        last modification date, it will mark each entry depending on whether the referred file is the same in
        both tables, is only found in one of the tables, is newer than the one on the other table or is older
        than the one on the other table. Rows will be marked with a icon on the first column.
        """
        remote_file_rows = self.remote_table_model.get_file_rows()
        local_file_rows = self.local_table_model.get_file_rows()

        # clear all previous marks and icons
        for row in remote_file_rows:
            row[0].setIcon(QIcon())
        for row in local_file_rows:
            row[0].setIcon(QIcon())

        if len(self.local_md5_list) == 0 or len(self.remote_md5_list) == 0:
            # md5 values could not be checked. Do not mark files
            return

        # Mark all files
        for remote_row in remote_file_rows:
            # Only mark files with the extension specified in the constructor
            if remote_row[1].text()[-len(self.file_extension):] == self.file_extension:
                # find local file with the same name
                local_row = None
                for row in local_file_rows:
                    if row[1].text() == remote_row[1].text():
                        local_row = row
                        break

                if local_row is None:
                    remote_row[0].setIcon(self.file_not_found_marker_icon)

                else:
                    # File is in both tables, check if md5sum is the same in both files
                    remote_md5 = None
                    for pair in self.remote_md5_list:
                        if remote_row[1].text() == pair[1]:
                            remote_md5 = pair[0]
                            break

                    local_md5 = None
                    for pair in self.local_md5_list:
                        if local_row[1].text() == pair[1]:
                            local_md5 = pair[0]

                    if remote_md5 == local_md5:
                        remote_row[0].setIcon(self.same_file_marker_icon)
                        local_row[0].setIcon(self.same_file_marker_icon)

                    else:
                        # If md5sum is not the same, check which file is newer
                        local_date = datetime.strptime(local_row[3].text(), self.date_format)
                        remote_date = datetime.strptime(remote_row[3].text(), self.date_format)
                        if local_date <= remote_date:
                            remote_row[0].setIcon(self.newer_file_marker_icon)
                            local_row[0].setIcon(self.older_file_marker_icon)
                        else:
                            remote_row[0].setIcon(self.older_file_marker_icon)
                            local_row[0].setIcon(self.newer_file_marker_icon)

        # mark all local rows that remain unmarked
        for local_row in local_file_rows:
            if local_row[1].text()[-len(self.file_extension):] == self.file_extension and local_row[0].icon().isNull():
                local_row[0].setIcon(self.file_not_found_marker_icon)

    def end_sftp_connection(self):
        """
        Closes the sftp connection
        """
        if self.sftp_connection is not None:
            self.sftp_connection.close()

    def reject(self):
        """
        Overridden method from QDialog. Called on closure of the dialog and when pressing 'esc'.
        """
        self.end_sftp_connection()
        self.local_GroupBox.setCollapsed(True)  # remote group box will collapse too
        super().reject()

    def show_help(self):
        """ Creates a help dialog """
        widget = SFTPHelpDialog(self.same_file_marker_icon,
                                self.file_not_found_marker_icon,
                                self.older_file_marker_icon,
                                self.newer_file_marker_icon)
        widget.exec_()

    def transfer_local_selected_files_to_remote(self):
        """
        Transfers the selected files from the local table view to the selected remote path
        """
        # selected indexes returns a list with all the items of the selected rows
        indexes = self.local_tableView.selectedIndexes()

        # Check if any files were selected
        if len(indexes) == 0:
            QMessageBox.warning(None, "Nothing selected",
                                "Please, select one or more files to transfer.")
            return False

        # if a row has 4 items, we only need the one with the file name (second one)
        name_list = list()
        for i in range(0, len(indexes)):
            if i % 4 == 1:
                item = self.local_table_model.itemFromIndex(indexes[i])
                name_list.append(item.text())

        # check if selected items are files and end with the extension specified by the constructor
        if self.file_extension is not None:
            for name in name_list:
                if name not in self.local_file_list or name[-len(self.file_extension):] != self.file_extension:
                    QMessageBox.warning(None, "Warning",
                                        "Selected items must be only files with " + self.file_extension+" extension")
                    return

        # check if files already exist in the remote folder and ask for permission to override them
        for name in name_list:
            if name in self.remote_file_list:
                if len(name_list) == 1:
                    reply = QMessageBox.question(None, 'Override files',
                                                 "The file you are trying to transfer already exists in the"
                                                 " remote folder. Override existing file?",
                                                 QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.No:
                        return
                    else:
                        break
                else:
                    reply = QMessageBox.question(None, 'Override files',
                                                 "Some of the files you are trying to transfer already exists in the"
                                                 " remote folder. Override existing files?",
                                                 QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.No:
                        return
                    else:
                        break

        # create transfer progress dialog
        self.create_transfer_progress_dialog(len(name_list))

        # Save current working directory and change it to the selected remote path
        pwd = self.sftp_connection.pwd
        self.sftp_connection.cwd(self.selected_remote_path)

        # send files to the selected remote folder
        transfer_canceled = False
        for name in name_list:
            full_file_name = self.selected_local_path + "/" + name
            logger.info("transfering " + full_file_name + " to " + self.sftp_connection.pwd)
            try:
                self.sftp_connection.put(full_file_name, callback=self.update_transfer_progress_dialog, preserve_mtime=True)
            except OSError as e:
                logger.error("Transfer canceled. {}".format(e))
                self.start_sftp_connection()
                file_name = self.selected_remote_path + "/" + name
                self.delete_remote_file(file_name)
                transfer_canceled = True
                break

        # Restore initial working directory
        self.sftp_connection.cwd(pwd)

        # Reload remote table view
        selected_indexes = self.remote_treeView.selectedIndexes()
        if len(selected_indexes) > 0:
            self.show_remote_selection(selected_indexes[0])

        self.close_transfer_progress_dialog()
        if transfer_canceled:
            QMessageBox.information(None, "Transfer canceled", "File transfer was canceled. \nLast file being sent was deleted.")
        else:
            QMessageBox.information(None, "Transfer complete", "File transfer was completed successfully")

    def transfer_remote_selected_files_to_local(self):
        """
        Transfers the selected files from the remote table view to the selected local path
        :return: True if transfer successful, False otherwise.
        :rtype: bool
        """
        # selected indexes returns a list with all the items of the selected rows
        indexes = self.remote_tableView.selectedIndexes()

        # Check if any files were selected
        if len(indexes) == 0:
            QMessageBox.warning(None, "Nothing selected",
                                "Please, select one or more files to transfer.")
            return False

        # if a row has 4 items, we only need the one with the file name (second one)
        name_list = list()
        for i in range(0, len(indexes)):
            if i % 4 == 1:
                item = self.remote_table_model.itemFromIndex(indexes[i])
                name_list.append(item.text())

        # check if selected items are files and end with the extension specified by the constructor
        if self.file_extension is not None:
            for name in name_list:
                if name not in self.remote_file_list or name[-len(self.file_extension):] != self.file_extension:
                    QMessageBox.warning(None, "Warning",
                                        "Selected items must be only files with " + self.file_extension+" extension")
                    return False

        # check if files already exist in the local folder and ask for permission to override them
        for name in name_list:
            if name in self.local_file_list:
                if len(name_list) == 1:
                    reply = QMessageBox.question(None, 'Override files',
                                                 "The file you are trying to transfer already exists in the"
                                                 " local folder. Override existing file?",
                                                 QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.No:
                        return False
                    else:
                        break
                else:
                    reply = QMessageBox.question(None, 'Override files',
                                                 "Some of the files you are trying to transfer already exists in the"
                                                 " local folder. Override existing files?",
                                                 QMessageBox.Yes, QMessageBox.No)
                    if reply == QMessageBox.No:
                        return False
                    else:
                        break

        # create transfer progress dialog
        self.create_transfer_progress_dialog(len(name_list))

        # send files to the selected local folder
        transfer_canceled = False
        for name in name_list:
            full_file_name = self.selected_remote_path + "/" + name
            full_local_name = self.selected_local_path + "/" + name
            logger.info("transfering " + full_file_name + " to " + self.selected_local_path)
            try:
                self.sftp_connection.get(full_file_name, full_local_name,
                                         callback=self.update_transfer_progress_dialog, preserve_mtime=True)
            except (OSError, SSHException) as e:
                logger.error("Transfer canceled. {}".format(e))
                self.start_sftp_connection()
                file_name = self.selected_local_path + "/" + name
                self.delete_local_file(file_name)
                transfer_canceled = True
                break

        # Reload local table view
        selected_indexes = self.local_treeView.selectedIndexes()
        if len(selected_indexes) > 0:
            self.show_local_selection(selected_indexes[0])

        self.close_transfer_progress_dialog()

        if transfer_canceled:
            QMessageBox.information(None, "Transfer canceled", "File transfer was canceled. \nLast file being sent was deleted.")
        else:
            QMessageBox.information(None, "Transfer complete", "File transfer was completed successfully")
        return True

    def expand_collapse_trees(self):
        """
        Only callable by the signals of the tree view group boxes. Expands and collapses both treeviews
        and resizes the dialog if needed.
        """
        sender = self.sender()

        if sender == self.local_GroupBox:
            other = self.remote_GroupBox
        elif sender == self.remote_GroupBox:
            other = self.local_GroupBox

        if sender.isCollapsed():
            other.setCollapsed(True)
            self.setMinimumHeight(self.minimum_height_collapsed)
        else:
            other.setCollapsed(False)
            self.setMinimumHeight(self.minimum_height_expanded)
            # Scroll to selected folders in tree views
            if len(self.local_treeView.selectedIndexes()) > 0:
                self.local_treeView.scrollTo(self.local_treeView.selectedIndexes()[0])
            if len(self.remote_treeView.selectedIndexes()) > 0:
                self.remote_treeView.scrollTo(self.remote_treeView.selectedIndexes()[0])

        if self.height() <= self.minimumHeight():
            maximum = self.maximumHeight()
            self.setFixedHeight(self.minimumHeight())
            self.setMaximumHeight(maximum)

    def clear_selection_other_table(self):
        """ Only callable by signals of tableView. Clears the selection of the other tableView of the Dialog """
        sender = self.sender()
        if sender == self.local_tableView:
            other = self.remote_tableView
        elif sender == self.remote_tableView:
            other = self.local_tableView
        other.clearSelection()

    def create_table_context_menu(self, pos):
        """
        Only callabe by signals of tableView. Creates a context menu for the elements selected in the tableView
        :param pos: position to create context menu from the tableView perspective
        :type pos: QPoint
        :return: Returns the popup menu if created, else returns None
        :rtype: QMenu
        """
        sender = self.sender()
        model = sender.model()
        file_list = list()
        if sender == self.local_tableView:
            file_list = self.local_file_list
        else:
            file_list = self.remote_file_list

        # get all item names
        indexes = sender.selectedIndexes()
        name_list = list()
        for i in range(0, len(indexes)):
            if i % 4 == 1:
                item = model.itemFromIndex(indexes[i])
                name_list.append(item.text())

        # Check if there are folders selected
        folders_selected = False
        for name in name_list:
            if name not in file_list:
                folders_selected = True
                break
        if not folders_selected:
            # Create menu with it's actions
            menu = QMenu(sender)
            if sender == self.local_tableView:
                if len(name_list) == 1:
                    menu.addAction("Rename", self.rename_local_file)
                menu.addAction("Transfer", self.transfer_local_selected_files_to_remote)
                menu.addAction("Delete", self.delete_local_selected_files)
            elif sender == self.remote_tableView:
                if len(name_list) == 1:
                    menu.addAction("Rename", self.rename_remote_file)
                menu.addAction("Transfer", self.transfer_remote_selected_files_to_local)
                menu.addAction("Delete", self.delete_remote_selected_files)
            menu.popup(sender.mapToGlobal(pos))
            return menu
        return None

    def delete_local_selected_files(self):
        """ Deletes selected files from local table view """
        indexes = self.local_tableView.selectedIndexes()
        name_list = list()
        for i in range(0, len(indexes)):
            if i % 4 == 1:
                item = self.local_table_model.itemFromIndex(indexes[i])
                name_list.append(item.text())

        reply = QMessageBox.question(None, 'Deleting files',
                                     ("Are you sure you want to delete this files? \n\n"
                                      + '\n'.join(['- {}']*len(name_list))).format(*name_list),
                                     QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            try:
                for name in name_list:
                    full_name = self.selected_local_path+"/"+name
                    subprocess.run(["rm", full_name], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

                QMessageBox.information(None, "Deleted files", "All selected files were deleted successfully")
                self.show_local_selection(self.local_treeView.selectedIndexes()[0])
            except subprocess.CalledProcessError as e:
                logger.error("Error deleting files. {}".format(e.stdout))

    def delete_remote_selected_files(self):
        """ Deletes selected files form remote table view """
        indexes = self.remote_tableView.selectedIndexes()
        name_list = list()
        for i in range(0, len(indexes)):
            if i % 4 == 1:
                item = self.remote_table_model.itemFromIndex(indexes[i])
                name_list.append(item.text())

        reply = QMessageBox.question(None, 'Deleting files',
                                     ("Are you sure you want to delete this files? \n\n"
                                      + '\n'.join(['- {}'] * len(name_list))).format(*name_list),
                                     QMessageBox.Yes, QMessageBox.No)

        if reply == QMessageBox.Yes:
            try:
                for name in name_list:
                    full_name = self.selected_remote_path+"/"+name
                    self.sftp_connection.remove(full_name)
                QMessageBox.information(None, "Deleted files", "All selected files were deleted successfully")
                self.show_remote_selection(self.remote_treeView.selectedIndexes()[0])
            except IOError as e:
                logger.error("Error deleting files. {}".format(e.stdout))

    def create_transfer_progress_dialog(self, n_files):
        """ Creates transfer progress dialog and shows it """
        self.transfer_progress_dialog = SFTPTransferProgressDialog(n_files, self)
        self.transfer_progress_dialog.cancel_transfer_signal.connect(self.cancel_transfer)
        self.transfer_progress_dialog.show()

    def update_transfer_progress_dialog(self, transferred_bytes, total_bytes):
        """
        Updates transfer progress dialog status with the bytes transferred so far and the total bytes to be transferred
        :param transferred_bytes: bytes transferred so far
        :type transferred_bytes: int
        :param total_bytes: total bytes to be transferred
        :type total_bytes: int
        """
        self.transfer_progress_dialog.update_values(transferred_bytes, total_bytes)
        QgsApplication.processEvents()

    def close_transfer_progress_dialog(self):
        """ Closes transfer progress dialog """
        if self.transfer_progress_dialog is not None:
            self.transfer_progress_dialog.cancel_transfer_signal.disconnect(self.cancel_transfer)
            self.transfer_progress_dialog.close()

    def cancel_transfer(self):
        """ Cancels the current transfer closing the channel """
        logger.info("Transfer cancelled by user")
        channel = self.sftp_connection.sftp_client.get_channel()
        # channel.shutdown_read()
        transport = channel.get_transport()
        transport.stop_thread()
        channel.close()

    def rename_local_file(self):
        """ Renames local selected file. Creates a dialog to ask the new name """
        indexes = self.local_tableView.selectedIndexes()
        name_item = self.local_table_model.itemFromIndex(indexes[1])
        file_name = name_item.text()

        ret = QInputDialog.getText(self, "Rename file", "New file name:",
                                        QLineEdit.Normal, file_name)
        while ret[1] and ret[0] in self.local_file_list:
            QMessageBox.information(None, "File already exists",
                                    "Another file already has that name, please choose a different name")
            ret = QInputDialog.getText(self, "Rename file", "New file name:",
                                       QLineEdit.Normal, file_name)

        if ret[1]:  # True if pressed ok in the dialog
            new_name = ret[0]
            full_file_name = self.selected_local_path + "/" + file_name
            full_new_name = self.selected_local_path + "/" + new_name
            try:
                logger.info("Renaming " + file_name + " to " + new_name)
                subprocess.run(["mv", full_file_name, full_new_name], check=True,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                self.show_local_selection(self.local_treeView.selectedIndexes()[0])
            except subprocess.CalledProcessError as e:
                logger.error("Error renaming file. {}".format(e))

    def rename_remote_file(self):
        """ Renames remote selected file. Creates a dialog to ask the new name """
        indexes = self.remote_tableView.selectedIndexes()
        name_item = self.remote_table_model.itemFromIndex(indexes[1])
        file_name = name_item.text()

        ret = QInputDialog.getText(self, "Rename file", "New file name:",
                                   QLineEdit.Normal, file_name)
        while(ret[1] and ret[0] in self.remote_file_list):
            QMessageBox.information(None, "File already exists",
                                    "Another file already has that name, please choose a different name")
            ret = QInputDialog.getText(self, "Rename file", "New file name:",
                                       QLineEdit.Normal, file_name)

        if ret[1]:  # True if pressed ok in the dialog
            new_name = ret[0]
            full_file_name = self.selected_remote_path + "/" + file_name
            full_new_name = self.selected_remote_path + "/" + new_name
            try:
                logger.info("Renaming " + file_name + " to " + new_name)
                self.sftp_connection.rename(full_file_name, full_new_name)
                self.show_remote_selection(self.remote_treeView.selectedIndexes()[0])
            except IOError as e:
                logger.error("Error renaming file. {}".format(e))

    def get_selected_remote_path(self):
        """
        Get selected remote path
        :return: selected remote path
        :rtype: str
        """
        return self.selected_remote_path

    def delete_remote_file(self, file_name):
        """
        Deletes the specified remote file
        :param file_name: name of the file
        :type file_name: str
        """
        try:
            logger.info("Deleting " + file_name + " from remote")
            self.sftp_connection.remove(file_name)
        except IOError as e:
            logger.error("Error deleting file. {}".format(e))

    def delete_local_file(self, file_name):
        """
        Deletes the specified local file
        :param file_name: name of the file
        :type file_name: str
        """
        try:
            logger.info("Deleting "+file_name+" from local")
            subprocess.run(["rm", file_name], check=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            logger.error("Error deleting file. {}".format(e))

    def show(self):
        #  If it didn't initialise correctly, do not show dialog
        if self.initialising or self.sftp_connection is None:
            return
        super().show()

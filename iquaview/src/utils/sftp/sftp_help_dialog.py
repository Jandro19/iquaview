"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QDialog, QWidgetItem

from iquaview.src.ui.ui_sftp_help_dialog import Ui_SFTP_Help_Dialog


class SFTPHelpDialog(QDialog, Ui_SFTP_Help_Dialog):
    def __init__(self, same_file_i, not_found_i, older_file_i, newer_file_i, current_mission_c=None, parent=None):
        super(SFTPHelpDialog, self).__init__(parent)
        self.setupUi(self)

        self.same_file_icon = same_file_i
        self.not_found_icon = not_found_i
        self.newer_file_icon = newer_file_i
        self.older_file_icon = older_file_i
        self.current_mission_color = current_mission_c

        pixmap_size = 20
        self.same_file_label.setPixmap(self.same_file_icon.pixmap(pixmap_size))
        self.not_found_label.setPixmap(self.not_found_icon.pixmap(pixmap_size))
        self.older_file_label.setPixmap(self.older_file_icon.pixmap(pixmap_size))
        self.newer_file_label.setPixmap(self.newer_file_icon.pixmap(pixmap_size))

        if current_mission_c is not None:
            p = self.current_mission_label.palette()
            p.setColor(QPalette.Background, self.current_mission_color)
            self.current_mission_label.setAutoFillBackground(True)
            self.current_mission_label.setPalette(p)
        else:
            # Remove current mission help
            while self.curr_mission_layout.count() > 1:
                item = self.curr_mission_layout.takeAt(0)
                item.widget().setParent(None)

        self.pushButton.pressed.connect(self.close)

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt5.QtGui import QStandardItem


class SFTPFolder(QStandardItem):
    def __init__(self, name):
        """
        Init of the object SFTPFolder
        :param name: name of the folder
        :type name: str
        """
        super(SFTPFolder, self).__init__(name)
        self.row_list = list()
        self.dummy_row = QStandardItem("")
        self.appendRow(self.dummy_row)

    def add_row(self, row):
        """
        Adds a row to the item
        :param row: row to be added
        :type row: QStandardItem
        """
        if self.dummy_row is not None:
            self.removeRow(0)
            self.dummy_row = None
        self.row_list.append(row)
        self.appendRow(row)

    def add_msg(self, msg):
        """
        Adds a message to the item
        :param msg: message to be added
        :type msg = str
        """
        if self.rowCount() > 0:
            self.removeRow(0)
            self.dummy_row = None
        self.appendRow(QStandardItem(str(msg)))

    def get_folder(self, name):
        """
        Returns the folder specified by name
        :param name: name of the folder
        :type name: str
        :return: folder item
        :rtype: SFTPFolder
        """
        for r in self.row_list:
            if name == r.text():
                return r
        return None

    def get_path(self, base_path=""):
        """
        Returns the path of the folder item
        :param base_path: base path to add to the folder path
        :type base_path: str
        :return: path of the folder
        :rtype: str
        """
        path_names = list()
        path_names.append('/'+self.text())

        parent = self.parent()
        while parent is not None:
            path_names.append('/'+parent.text())
            parent = parent.parent()

        path_names.append(base_path)

        item_path = ""
        for p in reversed(path_names):
            item_path += p

        return item_path

    def is_empty(self):
        """
        :return: Returns if the folder is empty and has no folders inside
        :rtype: bool
        """
        if self.dummy_row is None:
            return False
        else:
            return True

    def remove_dummy(self):
        """
        Removes the dummy row
        """
        if self.dummy_row is not None:
            self.takeRow(0)
            self.dummy_row = None

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""


import pysftp
from os import path

from PyQt5.QtWidgets import QTreeView, QStyle

from iquaview.src.utils.sftp.sftp_folder import SFTPFolder
from iquaview.src.utils.sftp.sftp_model import SFTPModel


class SFTPTreeView(QTreeView):
    def __init__(self, parent=None):
        super(SFTPTreeView, self).__init__(parent)
        self.closed_folder_icon = self.style().standardIcon(getattr(QStyle, "SP_DirClosedIcon"))
        self.open_folder_icon = self.style().standardIcon(getattr(QStyle, "SP_DialogOpenButton"))

        self.setModel(SFTPModel())

        self.collapsed.connect(self.collapse_folder)

    def add_folder(self, pathname):
        """
        Adds a folder to the model of the tree view
        :param pathname: path name of the folder
        :type pathname: str
        """
        tree_model = self.model()
        basename = path.basename(pathname)
        # ignore folders that start with dot ('.')
        if basename[0] != '.':
            # create a list of paths to iterate
            list_paths = list(pysftp.path_advance(pathname))
            # remove last element of the list
            list_paths.remove(list_paths[len(list_paths)-1])

            # when there is only one element, the items must be appended to the model
            if len(list_paths) == 1:
                folder = SFTPFolder(basename)
                folder.setIcon(self.closed_folder_icon)
                folder.setEditable(False)
                tree_model.add_row(folder)
            else:
                list_paths.remove(list_paths[0])
                folder = tree_model

                # find recursively the last folder
                for p in list_paths:
                    if folder is not None:
                        folder = folder.get_folder(path.basename(p))

                if folder is not None:
                    if not self.isExpanded(folder.index()):
                        self.setExpanded(folder.index(), True)

                    # if no other folder exists with the same name, add the new folder
                    if folder.get_folder(basename) is None:
                        new_folder = SFTPFolder(basename)
                        new_folder.setIcon(self.closed_folder_icon)
                        new_folder.setEditable(False)
                        folder.add_row(new_folder)

    def collapse_folder(self, index):
        """
        Collapses the folder specified by index
        :param index: index of the folder to be expanded
        :type index: QModelIndex
        """
        item = self.model().itemFromIndex(index)
        item.setIcon(self.closed_folder_icon)

    def expand_folder(self, index):
        """
        Expands the folder specified by index
        :param index: index of the folder to be expanded
        :type index: QModelIndex
        """
        item = self.model().itemFromIndex(index)
        if item.is_empty():
            # remove item dummy to show folder empty in treeview
            item.remove_dummy()
        else:
            item.setIcon(self.open_folder_icon)

# -*- coding: utf-8 -*-
"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Utils to perform computations with points and angles
"""

from qgis.core import QgsPointXY
import math


def wrap_angle(angle):
    """
    Wraps any angle (radians) over π to an angle between -π and π
    :param angle: angle to wrap
    :return: angle wraped between -π and π
    :rtype: float
    """
    return angle + (2.0 * math.pi * math.floor((math.pi - angle) / (2.0 * math.pi)))


def get_angle_of_line_between_two_points(p1, p2, angle_unit="degrees"):
    """
    Return the angle of the line represents by two points : p1 and p2

    :param p1: The first point
    :param p2: The second point
    :param angle_unit: desired angle units
    :type p1: QgsPointXY
    :type p2: QgsPointXY
    :type angle_unit: str
    :return: Return the angle (degree by default)
    :rtype: float
    """
    x_diff = p2.x() - p1.x()
    y_diff = p2.y() - p1.y()

    if angle_unit == "radians":
        return math.atan2(y_diff, x_diff)
    else:
        return math.degrees(math.atan2(y_diff, x_diff))


def calc_slope(p1, p2):
    """
    Return the slope of the line represents by two points : p1 and p2

    :param p1: The first point
    :param p2: The second point
    :type p1: QgsPointXY
    :type p2: QgsPointXY
    :return: Return the slope, can be infinity
    :rtype: float
    """

    num = p1.y() - p2.y()
    den = p1.x() - p2.x()

    if num == 0:
        return 0.0
    # Avoid division by zero
    elif den == 0:
        # Depending on num, result can be inf or -inf
        if num > 0:
            return -math.inf
        else:
            return math.inf
    else:
        return num / den


def calc_is_collinear(p0, p1, p2):
    """
    Test if point p2 is on left/on/right of the line made by p0 and p1
    :param p0: first point
    :type p0: QgsPointXY
    :param p1: second point
    :type p1: QgsPointXY
    :param p2: point to test
    :type p2: QgsPointXY
    :return: 1 left, 0 collinear, -1 right
    :rtype: int
    """

    sens = ((p1.x() - p0.x()) * (p2.y() - p0.y()) - (p1.y() - p0.y()) * (p2.x() - p0.x()))

    if sens > 0:
        return -1
    elif sens < 0:
        return 1
    else:
        return 0


def calc_middle_point(p1, p2):
    """
    Find the middle point between two points
    :param p1: first point
    :type p1: QgsPointXY
    :param p2: second point
    :type p2: QgsPointXY
    :return: Middle point found
    :rtype: QgsPointXY
    """

    return QgsPointXY((p1.x() + p2.x()) / 2.0, (p1.y() + p2.y()) / 2.0)


# from cadtools (c) Stefan ZIegler
def calc_parallel_segment(p1, p2, dist):
    """
    Find the points that form a parallel segment to the segment formed by p1 and p2 at a distance of dist
    (to the left if dist > 0, otherwise to the right)
    :param p1: first point
    :type p1: QgsPointXY
    :param p2: second point
    :type p2: QgsPointXY
    :param dist: distance to the segment of the two points
    :return: two points that form a segment parallel to the initial points
    :rtype: QgsPointXY
    """
    if dist == 0:
        return p1, p2

    # dist between the two initial points
    dn = distance_plane(p1, p2)

    # find the new points
    x3 = p1.x() + dist * (p1.y() - p2.y()) / dn
    y3 = p1.y() - dist * (p1.x() - p2.x()) / dn
    p3 = QgsPointXY(x3, y3)

    x4 = p2.x() + dist * (p1.y() - p2.y()) / dn
    y4 = p2.y() - dist * (p1.x() - p2.x()) / dn
    p4 = QgsPointXY(x4, y4)

    return p3, p4


def distance_ellipsoid(start, end):
    """
    Finds the distance between two points in the globe using WGS84 CRS and an ellipsoid approximation
    :param start: starting point
    :type start: QgsPointXY
    :param end: end point
    :type end: QgsPointXY
    :return: distance between the two points in meters
    :rtype: float
    """
    # Assumes points are WGS 84 lat/long
    # Returns great circle distance in meters
    radius = 6378137  # meters
    flattening = 1 / 298.257223563

    # Convert to radians with reduced latitudes to compensate
    # for flattening of the earth as in Lambert's formula
    start_lon = start.x() * math.pi / 180
    start_lat = math.atan2((1 - flattening) * math.sin(start.y() * math.pi / 180), math.cos(start.y() * math.pi / 180))
    end_lon = end.x() * math.pi / 180
    end_lat = math.atan2((1 - flattening) * math.sin(end.y() * math.pi / 180), math.cos(end.y() * math.pi / 180))

    # Haversine formula
    arc_distance = (math.sin((end_lat - start_lat) / 2) ** 2) + \
                   (math.cos(start_lat) * math.cos(end_lat) * (math.sin((end_lon - start_lon) / 2) ** 2))

    return 2 * radius * math.atan2(math.sqrt(arc_distance), math.sqrt(1 - arc_distance))


# http://www.movable-type.co.uk/scripts/latlong.html
def bearing(start, end):
    """
    Finds the bearing between two points in the globe using WGS84 CRS
    :param start: starting point
    :type start: QgsPointXY
    :param end: end point
    :type end: QgsPointXY
    :return: bearing between the two points in degrees
    :rtype: float
    """
    # Assumes points are WGS 84 lat/long

    start_lon = start.x() * math.pi / 180
    start_lat = start.y() * math.pi / 180
    end_lon = end.x() * math.pi / 180
    end_lat = end.y() * math.pi / 180

    return math.atan2(math.sin(end_lon - start_lon) * math.cos(end_lat),
                      (math.cos(start_lat) * math.sin(end_lat))
                      - (math.sin(start_lat) * math.cos(end_lat) * math.cos(end_lon - start_lon))) * 180 / math.pi


# http://www.movable-type.co.uk/scripts/latlong.html
def endpoint_sphere(start, dist, degrees_bearing):
    """
    Finds an end point given a start point, a distance and an angle in the globe using sphere approximation.
    :param start: start point (WGS84 CRS)
    :type start: QgsPointXY
    :param dist: distance to the end point (meters)
    :type dist: float
    :param degrees_bearing: bearing between start point and end point (degrees)
    :type degrees_bearing: float
    :return: end point
    :rtype: QgsPointXY
    """

    radius = 6378137.0  # meters

    start_lon = start.x() * math.pi / 180
    start_lat = start.y() * math.pi / 180
    bearing = degrees_bearing * math.pi / 180

    end_lat = math.asin((math.sin(start_lat) * math.cos(dist / radius)) +
                        (math.cos(start_lat) * math.sin(dist / radius) * math.cos(bearing)))
    end_lon = start_lon + math.atan2(math.sin(bearing) * math.sin(dist / radius) * math.cos(start_lat),
                                     math.cos(dist / radius) - (math.sin(start_lat) * math.sin(end_lat)))

    return QgsPointXY(end_lon * 180 / math.pi, end_lat * 180 / math.pi)


# https://www.movable-type.co.uk/scripts/latlong-vincenty.html
def endpoint_ellipsoid(start, dist, degrees_bearing):
    """
    Finds an end point given a start point, a distance and an angle in the globe using ellipsoid approximation.
    :param start: start point (WGS84 CRS)
    :type start: QgsPointXY
    :param dist: distance to the end point (meters)
    :type dist: float
    :param degrees_bearing: bearing between start point and end point (degrees)
    :type degrees_bearing: float
    :return: end point
    :rtype: QgsPointXY
    """

    start_lon = start.x() * math.pi / 180
    start_lat = start.y() * math.pi / 180
    bearing = degrees_bearing * math.pi / 180
    d = dist

    a = 6378137
    f = 0.003352813
    b = 6356752.3142

    sin_angle1 = math.sin(bearing)
    cos_angle1 = math.cos(bearing)

    tan_U1 = (1-f) * math.tan(start_lat)
    cos_U1 = 1 / math.sqrt((1+ tan_U1*tan_U1))
    sin_U1 = tan_U1 * cos_U1
    o1 = math.atan2(tan_U1, cos_angle1)
    sin_angle = cos_U1 * sin_angle1
    cosSq_angle = 1 - sin_angle*sin_angle
    uSq = cosSq_angle * (a*a - b*b) / (b*b)
    A = 1 + uSq / 16384 * (4096 + uSq * (-768 + uSq * (320 - 175 * uSq)))
    B = uSq / 1024 * (256 + uSq * (-128 + uSq * (74 - 47 * uSq)))

    r1 = d / (b*A)
    r2 = 0.0
    iteration = 0
    while abs(r1 - r2 > 1e-12) and iteration < 100:
        cos2roM = math.cos(2*o1 + r1)
        sin_ro = math.sin(r1)
        cos_ro = math.cos(r1)
        dif_ro = B * sin_ro * (cos2roM + B/4 * (cos_ro * (-1 + 2*cos2roM*cos2roM) -
                                                B/6 * cos2roM * (-3 +4*sin_ro*sin_ro) *
                                                (-3 + 4*cos2roM*cos2roM)))
        r2 = r1
        r1 = d / (b*A) + dif_ro
        iteration += 1

    x = sin_U1*sin_ro - cos_U1*cos_ro*cos_angle1
    lon = math.atan2(sin_ro*sin_angle1, cos_U1*cos_ro - sin_U1*sin_ro*cos_angle1)
    C = f/16 * cosSq_angle*(4+f * (4 - 3*cosSq_angle))
    L = lon - (1-C) * f * sin_angle * (r1 + C*sin_ro * (cos2roM+C*cos_ro * (-1 + 2*cos2roM*cos2roM)))

    end_lon = (start_lon + L + 3*math.pi) % (2*math.pi) - math.pi
    end_lat = math.atan2(sin_U1 * cos_ro + cos_U1 * sin_ro * cos_angle1, (1 - f) * math.sqrt(sin_angle * sin_angle + x * x))

    return QgsPointXY(end_lon * 180 / math.pi, end_lat * 180 / math.pi)


def distance_plane(p1, p2):
    """
    Finds the distance between two points in a plane
    :param p1: The first point
    :param p2: The second point
    :type p1: QgsPointXY
    :type p2: QgsPointXY
    :return: Return distance between p1 and p2
    :rtype: float
    """
    vect_x = p2.x() - p1.x()
    vect_y = p2.y() - p1.y()
    return math.sqrt(vect_x ** 2 + vect_y ** 2)


def project_point_to_line(point, line_start, line_end):
    """
    Projects a point onto a line where the normal of the line that passes through the point intersects with the line
    :param point: The point that will be projected on the line
    :param line_start: The first point of line
    :param line_end: The second point of line
    :type point: QgsPointXY
    :type line_start: QgsPointXY
    :type line_end: QgsPointXY
    :return: return the point projected onto the line
    :rtype: QgsPointXY
    """
    if point == line_start or point == line_end:
        return point

    # distances between vertex
    start_to_p = distance_plane(line_start, point)
    end_to_p = distance_plane(line_end, point)
    start_to_end = distance_plane(line_start, line_end)
    if start_to_end == 0:
        # start and end are the same point, cant form a line
        return line_start  # Projection of a point onto another point is the point itself
    # cosines theorem, angle in radians
    r = ((end_to_p ** 2) - (start_to_p ** 2) - (start_to_end ** 2)) / (-2 * start_to_p * start_to_end)
    if r > 1:
        r = 1
    elif r < -1:
        r = -1
    angle_s = math.acos(r)

    # get small angle of vertex-point
    angle_p_small = math.pi/2 - angle_s
    # get distance between start and projection
    c = start_to_p * math.sin(angle_p_small)

    # get projection position from line
    xp = line_start.x() + (c / start_to_end) * (line_end.x() - line_start.x())
    yp = line_start.y() + (c / start_to_end) * (line_end.y() - line_start.y())

    return QgsPointXY(xp, yp)


def distance_to_segment(point, segment_start, segment_end):
    """
    Computes the minimum distance between a point C and a line segment with endpoints A and B.
    :param segment_start: endpoint 1
    :type segment_start: QgsPointXY
    :param segment_end: endpoint 2
    :type segment_end: QgsPointXY
    :param point: point
    :type point: QgsPointXY
    :return: minimum distance between point and line segment
    :rtype: float
    """
    p = project_point_to_line(point, segment_start, segment_end)

    if is_between(p, segment_start, segment_end):
        return distance_plane(point, p)

    else:
        d1 = distance_plane(point, segment_start)
        d2 = distance_plane(point, segment_end)
        if d1 < d2:
            return d1
        else:
            return d2


def is_between(a, b, c):
    """
    Calculate if point A is between points B and C. In other words, a straight line passes through all 3 points
    :param a: point A
    :type a: QgsPointXY
    :param b: point B
    :type b: QgsPointXY
    :param c: point C
    :type c: QgsPointXY
    :return: return True if A is between B and C, otherwise False
    :rtype: bool
    """
    cross_product = (a.y() - b.y()) * (c.x() - b.x()) - (a.x() - b.x()) * (c.y() - b.y())
    if abs(cross_product) == 0:
        dist_a_to_b = math.sqrt((a.x() - b.x()) ** 2 + (a.y() - b.y()) ** 2)
        dist_a_to_c = math.sqrt((a.x() - c.x()) ** 2 + (a.y() - c.y()) ** 2)
        dist_b_to_c = math.sqrt((c.x() - b.x()) ** 2 + (c.y() - b.y()) ** 2)
        if not math.isclose(dist_a_to_b + dist_a_to_c, dist_b_to_c):  # to better compare floats, use isclose()
            return False

    dot_product = (a.x() - b.x()) * (c.x() - b.x()) + (a.y() - b.y()) * (c.y() - b.y())
    if dot_product < 0:
        return False

    squared_length_ba = (c.x() - b.x()) ** 2 + (c.y() - b.y()) ** 2
    if dot_product > squared_length_ba:
        return False
    return True

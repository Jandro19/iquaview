"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to control the processes that are launched/terminated inside the vehicle.
"""

import logging

from PyQt5.QtWidgets import QMessageBox, QWidget
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QBrush, QColor, QPixmap
from iquaview.src.ui.ui_auvprocesses import Ui_AUVProcessesWidget
from iquaview.src.connection.connectionclient import ConnectionClient
from iquaview.src.connection.cola2status import Cola2Status

logger = logging.getLogger(__name__)


class AUVProcessesWidget(QWidget, Ui_AUVProcessesWidget):
    processchanged = pyqtSignal(str)

    def __init__(self, config, vehicle_info, vehicle_data, parent=None):
        super(AUVProcessesWidget, self).__init__(parent)
        self.setupUi(self)
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.send_button.clicked.connect(self.send)
        self.terminate_button.clicked.connect(self.terminate)
        self.cc = ConnectionClient(self.vehicle_info.get_vehicle_ip(), self.vehicle_info.get_vehicle_port())
        self.cc.connection_failure.connect(self.connection_failed)
        self.cc.connection_ok.connect(self.connection_ok)
        self.cc.update_list.connect(self.set_terminate_process_items)
        self.cc.start_cc_thread()
        self.connected = False

        self.connection_label.setText("Vehicle")
        self.cola2_label.setText("COLA2")

        self.update_connection_indicator()
        self.cola2status = Cola2Status(self.config, self.vehicle_info, self.cola2_label, self.cola2_indicator)

    def set_start_process_items(self):
        """ Set start processes to  combobox"""

        vehicle_launch_dict = self.vehicle_data.get_vehicle_launch_dict()
        processes = list()
        processes_str = ""
        for item in vehicle_launch_dict:
            processes.append(item)
            processes_str += vehicle_launch_dict[item]

            if len(processes) != len(vehicle_launch_dict):
                processes_str += ","

        self.cc.send("list " + processes_str)
        logger.info(vehicle_launch_dict)
        processes.insert(0, "restart vehicle pc")
        model = QStandardItemModel(len(processes), 1)

        first_item = QStandardItem("-- Start Processes --")
        first_item.setBackground(QBrush(QColor(200, 200, 200)))
        model.setItem(0, 0, first_item)

        for i, processes in enumerate(processes):
            item = QStandardItem(processes)
            item.setData(Qt.Unchecked, Qt.CheckStateRole)
            model.setItem(i + 1, 0, item)

        self.startprocess_combo.setModel(model)

    def set_terminate_process_items(self):
        """ Set terminate processes to combobox"""
        data = self.cc.send("on")
        logger.info(data)
        processes = data[:-1].split(',')
        model = QStandardItemModel(len(processes), 1)
        first_item = QStandardItem("-- Terminate Processes --")
        first_item.setBackground(QBrush(QColor(200, 200, 200)))
        model.setItem(0, 0, first_item)

        for i, processes in enumerate(processes):
            for element in self.vehicle_data.get_vehicle_launch_dict():
                if processes == self.vehicle_data.get_vehicle_launch_dict()[element]:
                    item = QStandardItem(element)
                    item.setData(Qt.Unchecked, Qt.CheckStateRole)
                    model.setItem(i + 1, 0, item)

        self.terminateprocess_combo.setModel(model)

    def connect(self):
        # Update connection with current values
        self.cc.ip = self.vehicle_info.get_vehicle_ip()
        self.cc.port = self.vehicle_info.get_vehicle_port()

    def disconnect_auvprocesses(self):
        """ Disconnect auvprocesses"""
        self.cc.disconnect()
        self.connected = False
        self.update_connection_indicator()
        self.startprocess_combo.setEnabled(False)
        self.terminateprocess_combo.setEnabled(False)
        self.send_button.setEnabled(False)
        self.terminate_button.setEnabled(False)

        self.cola2status.disconnect_cola2status()

    def send(self):
        """ Send start process to auv"""
        logger.info(self.startprocess_combo.currentText())
        if self.startprocess_combo.currentIndex() != 0:
            if self.terminateprocess_combo.findText(self.startprocess_combo.currentText()) != -1:
                reply = QMessageBox.information(self,
                                                "Process already running",
                                                "The process {} is already running in the vehicle. ".format(
                                                    self.startprocess_combo.currentText()),
                                                QMessageBox.Ok)
            else:
                if self.startprocess_combo.currentIndex() == 1:
                    reply = QMessageBox.question(self,
                                                 "Reboot request",
                                                 "You are about to reboot the vehicle PC. Do you want to continue?",
                                                 QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
                    if reply == QMessageBox.Yes:
                        self.cc.send("restart")
                        self.processchanged.emit("Vehicle PC is rebooting")
                else:
                    process = self.vehicle_data.get_vehicle_launch_dict()[self.startprocess_combo.currentText()]
                    self.cc.send(process)
                    self.terminateprocess_combo.removeItem(
                        self.terminateprocess_combo.findText("No running processes", Qt.MatchFixedString))
                    self.terminateprocess_combo.addItem(self.startprocess_combo.currentText())
                    self.processchanged.emit(
                        "Process {} has been started".format(self.startprocess_combo.currentText()))
                    self.set_terminate_process_items()

    def terminate(self):
        """ send terminate process to auv"""
        logger.info(self.terminateprocess_combo.currentText())
        if self.terminateprocess_combo.currentIndex() != 0:
            process = self.vehicle_data.get_vehicle_launch_dict()[self.terminateprocess_combo.currentText()]
            self.cc.send("terminate " + process)
            self.processchanged.emit(
                "Process {} has been terminated".format(self.terminateprocess_combo.currentText()))
            self.terminateprocess_combo.removeItem(self.terminateprocess_combo.currentIndex())
            self.set_terminate_process_items()

    def connection_failed(self):
        """ disable items if connection failed"""
        self.connected = False
        self.update_connection_indicator()
        self.startprocess_combo.setEnabled(False)
        self.terminateprocess_combo.setEnabled(False)
        self.send_button.setEnabled(False)
        self.terminate_button.setEnabled(False)
        self.connect()

    def connection_ok(self):
        """ enable items if connection with auv is established"""
        self.connected = True
        self.update_connection_indicator()
        self.set_start_process_items()
        self.set_terminate_process_items()
        self.startprocess_combo.setEnabled(True)
        self.terminateprocess_combo.setEnabled(True)
        self.send_button.setEnabled(True)
        self.terminate_button.setEnabled(True)

    def update_connection_indicator(self):
        """ update indicators according with the connection with AUV"""
        if not self.connected:
            self.connection_indicator.setPixmap(QPixmap(":/resources/red_led.svg"))
            self.connection_label.setStyleSheet('font:italic; color:red')
        else:
            self.connection_indicator.setPixmap(QPixmap(":/resources/green_led.svg"))
            self.connection_label.setStyleSheet('font:italic; color:green')

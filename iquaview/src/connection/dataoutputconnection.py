"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to export auv and usbl position by serial or ethernet
"""

import os
import socket
import time
import threading
import select
from importlib import util
import logging
import serial
from math import floor, degrees

from PyQt5.QtCore import pyqtSignal, QTimer, QThreadPool
from PyQt5.QtWidgets import QWidget, QInputDialog, QLineEdit
from PyQt5.QtGui import QValidator

from iquaview.src.utils.workerthread import Worker
from iquaview.src.utils.textvalidator import (validate_ip,
                                              validate_port,
                                              get_color)
from iquaview.src.ui.ui_dataoutputconnection import Ui_DataOutputConnectionWidget

logger = logging.getLogger(__name__)


class DataOutputConnection(QWidget, Ui_DataOutputConnectionWidget):
    refresh_signal = pyqtSignal()
    reconnect_signal = pyqtSignal()
    disconnect_signal = pyqtSignal(bool)
    remove_connection_signal = pyqtSignal()

    def __init__(self, identifier, connection_name, config, vehicle_data, usbl_module=None):
        super(DataOutputConnection, self).__init__()
        self.setupUi(self)

        self.id = identifier
        self.name = connection_name
        self.connected = False
        self.keepgoing = False
        self.socket_udp = None
        self.socket_tcp = None
        self.last_auv_time = None
        self.last_usbl_time = None
        self.config = config
        self.vehicle_data = vehicle_data
        self.usbl_module = usbl_module
        self.output_type = None
        self.output_is_serial = None
        self.output_protocol = None
        self.output_ip = None
        self.output_port = None
        self.output_serial_device = None
        self.output_serial_baudrate = None
        self.thread_clients = None
        self.r_channel, self.w_channel = os.pipe()
        self.client_connection_list = list()

        self.groupBox.setTitle(connection_name)

        self.threadpool = QThreadPool()

        self.timer = QTimer()
        self.timer.timeout.connect(self.start_timer)
        self.refresh_signal.connect(self.refresh)
        self.disconnect_signal.connect(self.disconnect_widget)
        self.reconnect_signal.connect(self.start_connection_thread)

        self.ip_lineEdit.textChanged.connect(self.ip_validator)
        self.ggaport_lineEdit.textChanged.connect(self.port_validator)
        self.remove_pushButton.clicked.connect(self.remove_connection)

        self.ethernet_radioButton.toggled.connect(self.change_enable)
        self.serial_radioButton.toggled.connect(self.change_enable)

        self.change_enable()

    def get_identifier(self):
        return self.id

    def mouseDoubleClickEvent(self, event) -> None:

        new_name = QInputDialog.getText(self, "Rename file", "New connection name:",
                                        QLineEdit.Normal, self.groupBox.title())
        if new_name[1]:
            self.groupBox.setTitle(new_name[0])
            self.name = new_name[0]

    def reload(self):
        """ Reload values on widget"""
        if (self.config.csettings.get('data_output_connection')
                and self.config.csettings['data_output_connection'].get(self.id)):
            self.output_type = self.config.csettings['data_output_connection'][self.id]['output_type']
            self.output_is_serial = self.config.csettings['data_output_connection'][self.id]['output_is_serial']
            self.output_protocol = self.config.csettings['data_output_connection'][self.id]['ethernet_protocol']
            try:
                self.output_ip = self.config.csettings['data_output_connection'][self.id]['ethernet_ip']
            except:
                self.output_ip = ""
            try:
                self.output_port = str(self.config.csettings['data_output_connection'][self.id]['ethernet_port'])
            except:
                self.output_port = ""
            self.output_serial_device = self.config.csettings['data_output_connection'][self.id]['serial_device']
            self.output_serial_baudrate = str(
                self.config.csettings['data_output_connection'][self.id]['serial_baudrate'])

            self.ggaport_lineEdit.setText(self.output_port)
            self.ip_lineEdit.setText(self.output_ip)
            self.device_lineEdit.setText(self.output_serial_device)
            index_protocol = self.protocol_comboBox.findText(self.output_protocol)
            if index_protocol != -1:
                self.protocol_comboBox.setCurrentIndex(index_protocol)

            index_type = self.position_comboBox.findText(self.output_type)
            if index_type != -1:
                self.position_comboBox.setCurrentIndex(index_type)

            index_baudrate = self.baudrate_comboBox.findText(self.output_serial_baudrate)
            if index_baudrate != -1:
                self.baudrate_comboBox.setCurrentIndex(index_baudrate)

            if self.output_is_serial:
                self.serial_radioButton.setChecked(True)
            else:
                self.ethernet_radioButton.setChecked(True)
            self.change_enable(self.output_is_serial)

    def start_connection_thread(self):
        """ Start cola2 thread """
        worker = Worker(self.connect)  # Any other args, kwargs are passed to the run function
        self.threadpool.start(worker)
        self.timer.start(1000)

    def connect(self):
        """ Starts connexion. Serial or Ethernet (TCP or UDP)"""
        try:
            self.connected = True
            if not self.output_is_serial:
                if self.output_protocol == "UDP":
                    self.socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
                    self.socket_udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                else:
                    if self.socket_tcp is not None:
                        self.socket_tcp.close()
                    self.socket_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.socket_tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    # Bind the socket to the address given on the command line
                    server_address = (self.output_ip, int(self.output_port))
                    self.socket_tcp.bind(server_address)
                    self.socket_tcp.listen(5)  # 5 denotes the number of clients can queue

                    self.keepgoing = True
                    self.thread_clients = threading.Thread(target=self.manage_clients)
                    self.thread_clients.daemon = True
                    self.thread_clients.start()

        except Exception as e:
            logger.error("Data Output error trying to establish connection: {}".format(e))
            self.disconnect_signal.emit(True)
            time.sleep(5)
            self.reconnect_signal.emit()

    def disconnect_widget(self, reconnect=True):
        """
        Sets the widget as disconnected
        :param reconnect: if reconnect is False, disconnect reconnect signal
        :type reconnect: bool
        """
        if not reconnect:
            self.reconnect_signal.disconnect(self.start_connection_thread)
        self.connected = False
        self.keepgoing = False
        self.timer.stop()
        for conn in self.client_connection_list:
            conn.close()

        if self.socket_tcp is not None:
            self.socket_tcp.close()

        if self.socket_udp is not None:
            self.socket_udp.close()

        os.write(self.w_channel, '!'.encode())  # write something, just to wake up the select call
        self.threadpool.clear()

    def manage_clients(self):
        """ Manage client connection"""
        try:
            self.client_connection_list = list()
            while self.keepgoing:
                rfds, _, _ = select.select([self.socket_tcp.fileno(), self.r_channel], [], [])
                logger.debug('Waiting for a connection')
                connection, client_address = self.socket_tcp.accept()
                connection.settimeout(5.0)

                self.client_connection_list.append(connection)
        except OSError as e:
            logger.error("OSError: {}".format(e))
            self.disconnect_widget()
            time.sleep(5)
            self.reconnect_signal.emit()
        except Exception as e:
            logger.error(e)
            self.disconnect_widget()
            time.sleep(5)
            self.reconnect_signal.emit()

    def start_timer(self):
        """ emits refresh_signal if connected """
        if self.connected:
            self.refresh_signal.emit()

    def refresh(self):
        """ Refresh data. Get position and send position"""
        try:
            gga_message, hdt_message = self.get_position()
            if gga_message is not None:
                self.send_data(gga_message)

            if hdt_message is not None:
                self.send_data(hdt_message)
        except Exception as e:
            logger.error(e)

    def get_position(self):
        """
        Get data from auv or usbl and create a gga and hdt messages
        """
        gga_message = None
        hdt_message = None

        if self.connected and (validate_ip(self.output_ip) == QValidator.Acceptable
                               and validate_port(self.output_port) == QValidator.Acceptable):
            if self.output_type == "AUV Position":
                if self.vehicle_data.is_subscribed():
                    data = self.vehicle_data.get_nav_sts()
                    if data is not None and data['valid_data'] == 'new_data':
                        lat = float(data['global_position']['latitude'])
                        lon = float(data['global_position']['longitude'])
                        heading = float(data['orientation']['yaw'])
                        depth = float(data['position']['depth'])
                        # altitude = float(data['altitude'])

                        gga_message = self.gen_gga(time.gmtime(), lat, "N", lon, "E", 1, 0, 1.5, -depth, 0, 0, 0)
                        hdt_message = self.gen_hdt(degrees(heading))

                    elif (util.find_spec('iquaview_evologics_usbl') is not None
                          and self.usbl_module.get_usbl_widget() is not None
                          and self.usbl_module.get_usbl_widget().is_connected()):
                        auv_data = self.usbl_module.get_usbl_widget().get_auv_data()
                        if auv_data is not None:
                            auv_time = float(auv_data['time'])
                            if auv_time != 0.0 and auv_time != self.last_auv_time:  # if valid data
                                logger.info("Received data back from AUV")
                                auv_lat = float(auv_data['latitude'])
                                auv_lon = float(auv_data['longitude'])
                                auv_depth = float(auv_data['depth'])
                                auv_heading = float(auv_data['heading'])
                                auv_error = int(auv_data['error'])
                                self.last_auv_time = auv_time

                                gga_message = self.gen_gga(time.gmtime(), auv_lat, "N", auv_lon, "E", 1, 0, 1.5,
                                                           -auv_depth, 0, 0, 0)
                                hdt_message = self.gen_hdt(degrees(auv_heading))

            else:
                if (util.find_spec('iquaview_evologics_usbl') is not None
                        and self.usbl_module.get_usbl_widget() is not None
                        and self.usbl_module.get_usbl_widget().is_connected()):
                    usbl_data = self.usbl_module.get_usbl_widget().get_usbl_data()
                    if usbl_data is not None:
                        # get usbl data
                        usbl_time = float(usbl_data['time'])
                        if usbl_time != 0.0 and usbl_time != self.last_usbl_time:  # if valid and new data
                            usbl_lat = float(usbl_data['latitude'])
                            usbl_lon = float(usbl_data['longitude'])
                            usbl_height = -float(usbl_data['depth'])  # negative
                            usbl_time = float(usbl_data['time'])

                            gga_message = self.gen_gga(time.gmtime(), usbl_lat, "N", usbl_lon, "E", 1, 0, 1.5,
                                                       usbl_height, 0, 0, 0)

                            self.last_usbl_time = usbl_time

        return gga_message, hdt_message

    def send_data(self, message):
        """
        Send position via serial or ethernet
        :param message: message to send
        :type message: str
        """
        if self.output_is_serial:
            device = self.output_serial_device
            baudrate = self.output_serial_baudrate
            with serial.Serial(device, baudrate, timeout=2) as ser:
                ser.write((message + "\n").encode())
                ser.close()
        else:
            if self.output_protocol == "UDP":
                self.socket_udp.sendto(message.encode(), (self.output_ip, int(self.output_port)))
            else:
                for connection in self.client_connection_list:
                    try:
                        connection.sendall((message + "\r\n").encode())
                    except socket.timeout:
                        logger.error('Timeout error')
                        connection.close()
                        self.client_connection_list.remove(connection)
                    except socket.error as ex:
                        logger.error('Error {}'.format(ex))
                        connection.close()
                        self.client_connection_list.remove(connection)
                    except Exception as ex:
                        logger.error("Exception {}".format(ex))
                        connection.close()
                        self.client_connection_list.remove(connection)

    def is_connected(self):
        """
        Returns whether the widget is set as connected
        :return: True if connected
        :rtype: bool
        """
        return self.connected

    def gen_hdt(self, heading):
        string = 'GPHDT,%s,T' % (heading)
        crc = 0
        for c in string:
            crc = crc ^ ord(c)
        crc = crc & 0xFF

        return '$%s*%0.2X' % (string, crc)

    def gen_gga(self, time_t, lat, lat_pole, lng, lng_pole, fix_quality, num_sats, hdop, alt_m, geoidal_sep_m,
                dgps_age_sec,
                dgps_ref_id):
        hhmmssss = '%02d%02d%02d%s' % (time_t.tm_hour, time_t.tm_min, time_t.tm_sec, '.%02d' if 0 != 0 else '')

        lat_abs = abs(lat)
        lat_deg = lat_abs
        lat_min = (lat_abs - floor(lat_deg)) * 60
        lat_sec = round((lat_min - floor(lat_min)) * 10000000)
        lat_pole_prime = ('S' if lat_pole == 'N' else 'N') if lat < 0 else lat_pole
        lat_format = '%02d%02d.%07d' % (lat_deg, lat_min, lat_sec)

        lng_abs = abs(lng)
        lng_deg = lng_abs
        lng_min = (lng_abs - floor(lng_deg)) * 60
        lng_sec = round((lng_min - floor(lng_min)) * 10000000)
        lng_pole_prime = ('W' if lng_pole == 'E' else 'E') if lng < 0 else lng_pole
        lng_format = '%03d%02d.%07d' % (lng_deg, lng_min, lng_sec)

        dgps_format = '%s,%s' % ('%.1f' % dgps_age_sec if dgps_age_sec is not None else '',
                                 '%04d' % dgps_ref_id if dgps_ref_id is not None else '')

        string = 'GPGGA,%s,%s,%s,%s,%s,%d,%02d,%.1f,%.1f,M,%.1f,M,%s' % (
            hhmmssss, lat_format, lat_pole_prime, lng_format, lng_pole_prime, fix_quality, num_sats, hdop, alt_m,
            geoidal_sep_m, dgps_format)
        crc = 0
        for c in string:
            crc = crc ^ ord(c)
        crc = crc & 0xFF

        return '$%s*%0.2X' % (string, crc)

    def remove_connection(self):
        self.remove_connection_signal.emit()

    def change_enable(self, enable=False):
        ethernet_state = self.ethernet_radioButton.isChecked()
        serial_state = self.serial_radioButton.isChecked()

        self.protocol_label.setEnabled(ethernet_state)
        self.protocol_comboBox.setEnabled(ethernet_state)
        self.ip_label.setEnabled(ethernet_state)
        self.ip_lineEdit.setEnabled(ethernet_state)
        self.ggaport_lineEdit.setEnabled(ethernet_state)
        self.ggaport_label.setEnabled(ethernet_state)

        self.device_label.setEnabled(serial_state)
        self.device_lineEdit.setEnabled(serial_state)
        self.baudrate_label.setEnabled(serial_state)
        self.baudrate_comboBox.setEnabled(serial_state)

    def ip_validator(self):
        """ Validate text(ip) of sender"""
        sender = self.sender()
        state = validate_ip(sender.text())
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def port_validator(self):
        """ Validate text(port) of sender"""
        sender = self.sender()
        state = validate_port(sender.text())
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)

    def is_valid_data(self):
        """ Check if gps data is valid"""
        return (self.serial_radioButton.isChecked()
                or (self.ethernet_radioButton.isChecked()
                    and validate_ip(self.ip_lineEdit.text()) == QValidator.Acceptable
                    and validate_port(self.ggaport_lineEdit.text()) == QValidator.Acceptable))

    def on_apply(self):
        if self.is_valid_data():
            self.output_type = self.position_comboBox.currentText()
            self.output_is_serial = self.serial_radioButton.isChecked()
            self.output_ip = self.ip_lineEdit.text()
            self.output_port = self.ggaport_lineEdit.text()
            self.output_protocol = self.protocol_comboBox.currentText()
            self.output_serial_device = self.device_lineEdit.text()
            self.output_serial_baudrate = self.baudrate_comboBox.currentText()

            if not self.config.csettings['data_output_connection']:
                self.config.csettings['data_output_connection'] = {}

            if not self.config.csettings['data_output_connection'].get(self.id):
                self.config.csettings['data_output_connection'][self.id] = {}
            self.config.csettings['data_output_connection'][self.id]['connection_name'] = self.name
            self.config.csettings['data_output_connection'][self.id]['output_type'] = self.output_type
            self.config.csettings['data_output_connection'][self.id]['output_is_serial'] = self.output_is_serial
            self.config.csettings['data_output_connection'][self.id]['ethernet_protocol'] = self.output_protocol
            if validate_ip(self.ip_lineEdit.text()) == QValidator.Acceptable:
                self.config.csettings['data_output_connection'][self.id]['ethernet_ip'] = self.output_ip
            if validate_port(self.ggaport_lineEdit.text()) == QValidator.Acceptable:
                self.config.csettings['data_output_connection'][self.id]['ethernet_port'] = int(self.output_port)
            self.config.csettings['data_output_connection'][self.id]['serial_device'] = self.output_serial_device
            self.config.csettings['data_output_connection'][self.id]['serial_baudrate'] = int(
                self.output_serial_baudrate)

    def on_accept(self):
        self.on_apply()
        if self.is_valid_data():
            self.config.settings['data_output_connection'][self.id]['connection_name'] = self.name
            self.config.settings['data_output_connection'][self.id]['output_type'] = self.output_type
            self.config.settings['data_output_connection'][self.id]['output_is_serial'] = self.output_is_serial
            self.config.settings['data_output_connection'][self.id]['ethernet_protocol'] = self.output_protocol
            if validate_ip(self.ip_lineEdit.text()) == QValidator.Acceptable:
                self.config.settings['data_output_connection'][self.id]['ethernet_ip'] = self.output_ip
            if validate_port(self.ggaport_lineEdit.text()) == QValidator.Acceptable:
                self.config.settings['data_output_connection'][self.id]['ethernet_port'] = int(self.output_port)
            self.config.settings['data_output_connection'][self.id]['serial_device'] = self.output_serial_device
            self.config.settings['data_output_connection'][self.id]['serial_baudrate'] = int(
                self.output_serial_baudrate)

            self.config.save()

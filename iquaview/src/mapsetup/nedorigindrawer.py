"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to automatically generate a layer with the NED origin point to display in the map canvas
 The NED origin is retrieved from the navigator ROS params.
 """

import logging

from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import pyqtSignal, QObject

from qgis.core import (QgsVectorLayer,
                       QgsSymbol,
                       QgsSingleSymbolRenderer,
                       QgsFeature,
                       QgsGeometry,
                       QgsPointXY,
                       QgsSvgMarkerSymbolLayer,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransform,
                       QgsProject)

from iquaview.src.cola2api import cola2_interface
from iquaview.src.utils import busywidget

logger = logging.getLogger(__name__)


class NEDOriginDrawer(QObject):
    map_tool_change_signal = pyqtSignal()

    def __init__(self, proj, vehicle_info, canvas, parent= None):
        """
        Init of the object NEDOriginDrawer
        :param proj: project to add ned origin layer into
        :type proj: QgsProject
        :param vehicle_info: vehicle info to extract the ned position from
        :type vehicle_info: VehicleInfo
        """
        super(NEDOriginDrawer, self).__init__(parent)
        self.proj = proj
        self.vehicle_info = vehicle_info
        self.canvas = canvas
        self.ned_origin_layer = QgsVectorLayer(
            "Point?crs=epsg:4326",
            "NED Origin",
            "memory")
        self.feat = QgsFeature()
        self.ned_origin_layer.dataProvider().addFeature(self.feat)
        self.ned_origin_layer.setRenderer(self.ned_origin_renderer())
        self.ned_origin_layer.setCustomProperty("ned_origin", "NED Origin")

        self.ned_lat = None
        self.ned_lon = None

    def ned_origin_renderer(self):
        """
        Creates a renderer for the ned origin layer
        :return: renderer for the ned origin layer
        :rtype: QgsSingleSymbolRenderer
        """
        symbol = QgsSymbol.defaultSymbol(self.ned_origin_layer.geometryType())
        svg_style = {'fill': '# 0000ff',
                     'name': ':/resources/Star2.svg',
                     'outline': '#000000',
                     'outline - width': '6.8',
                     'size': '6'}
        # create svg symbol layer
        sym_lyr1 = QgsSvgMarkerSymbolLayer.create(svg_style)
        # Replace the default layer with our custom layer
        symbol.deleteSymbolLayer(0)
        symbol.appendSymbolLayer(sym_lyr1)
        # Replace the renderer of the current layer
        renderer = QgsSingleSymbolRenderer(symbol)
        return renderer

    def update_ned_point(self):
        """
        Updates ned origin point with the data from vehicle info.
        Adds the ned origin layer if it's not already in the project.
        """
        try:
            self.ned_lat = float(cola2_interface.get_ros_param(self.vehicle_info.get_vehicle_ip(), 9091,
                                                               self.vehicle_info.get_vehicle_namespace() + '/navigator/ned_latitude')[
                                     'value'])
            self.ned_lon = float(cola2_interface.get_ros_param(self.vehicle_info.get_vehicle_ip(), 9091,
                                                               self.vehicle_info.get_vehicle_namespace() + '/navigator/ned_longitude')[
                                     'value'])

            # layer is not yet in the project, add it
            if len(self.proj.mapLayersByName('NED Origin')) == 0:
                self.feat.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(self.ned_lon, self.ned_lat)))
                self.ned_origin_layer.dataProvider().addFeatures([self.feat])
                self.proj.addMapLayer(self.ned_origin_layer, True)
            else:
                self.ned_origin_layer.startEditing()
                self.ned_origin_layer.moveVertex(self.ned_lon, self.ned_lat, self.feat.id() + 1, 0)
                self.ned_origin_layer.commitChanges()

        except:
            QMessageBox.warning(None,
                                "NED Origin update",
                                "Could not read NED origin topic.",
                                QMessageBox.Close)

    def move_ned_origin(self, point):
        """
        Moves the NED origin to the specified point and asks if the user wants to save this as the default NED point
        :param point: point to move NED origin to
        :type point: QgsPointXY
        :return:
        """
        # Transform point if comes form a different crs
        default_crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
        if default_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
            trans = QgsCoordinateTransform(self.canvas.mapSettings().destinationCrs(),
                                           default_crs,
                                           QgsProject.instance().transformContext())
            point = trans.transform(point.x(), point.y())

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_name = self.vehicle_info.get_vehicle_namespace()

        # Change ned lat and lon parameters
        logger.info("Changing NED Origin to {}, {}".format(point.y(), point.x()))
        ned_latitude = "/navigator/ned_latitude"
        ned_longitude = "/navigator/ned_longitude"

        try:
            cola2_interface.set_ros_param(ip, port, vehicle_name + ned_latitude, str(point.y()))
            cola2_interface.set_ros_param(ip, port, vehicle_name + ned_longitude, str(point.x()))
        except ConnectionRefusedError:
            logger.error("Changing NED Origin failed")
            QMessageBox.critical(self.canvas, "Changing NED origin failed.", "Connection Refused", QMessageBox.Close)
        except Exception as e:
            logger.error("Changing NED Origin failed")
            QMessageBox.critical(self.canvas, "Changing NED origin failed.", e.args[0], QMessageBox.Close)
        else:
            # Introduce a delay between changing parameters and resetting navigation
            bw = busywidget.BusyWidget(title="Changing NED origin...")
            bw.on_start()
            bw.exec_()

            # Do a reset navigation to apply this changes
            action_service = "/navigator/reload_ned"
            logger.info("sending action service: {}".format(action_service))

            try:
                response = cola2_interface.send_trigger_service(ip, port, vehicle_name + action_service)
                try:
                    if not response['values']['success']:
                        QMessageBox.critical(self,
                                             "Reset navigation failed",
                                             response['values']['message'],
                                             QMessageBox.Close)
                # back compatibility
                except Exception as e:
                    logger.warning("The disable goto response can not be read")
            except ConnectionRefusedError:
                logger.error("Reset navigation failed")
                QMessageBox.critical(self.canvas, "Reset navigation failed.", "Connection Refused", QMessageBox.Close)
            except Exception as e:
                logger.error("Reset navigation failed")
                QMessageBox.critical(self.canvas, "Reset navigation failed.", e.args[0], QMessageBox.Close)
            else:
                logger.info("Result: {}".format(response))

                self.update_ned_point()

                # Ask if the user wants to set this new location as default
                msg = "Do you want to save this new position as the default NED origin position?"
                reply = QMessageBox.question(self.canvas, 'Save as default NED origin', msg,
                                             QMessageBox.Yes, QMessageBox.No)

                # If yes, send service to update yamls params. This saves all params as defaults, not only NED origin.
                if reply == QMessageBox.Yes:
                    try:
                        logger.info("Saving parameters as defaults...")

                        string = "/default_param_handler/update_param_in_yaml"
                        cola2_interface.send_string_service(ip, port,
                                                            vehicle_name + string, vehicle_name + ned_latitude)
                        cola2_interface.send_string_service(ip, port,
                                                            vehicle_name + string, vehicle_name + ned_longitude)
                        logger.info("Result: {}".format(response))
                        if not response['values']['success']:
                            QMessageBox.critical(self,
                                                 "Error saving parameters",
                                                 response['values']['message'],
                                                 QMessageBox.Close)
                    except ConnectionRefusedError:
                        logger.error("Saving parameters failed")
                        QMessageBox.critical(self.canvas, "Saving parameters failed,", "Connection Refused",
                                             QMessageBox.Close)
                    except Exception as e:
                        logger.error("Saving parameters failed")
                        QMessageBox.critical(self.canvas, "Saving parameters failed,", e.args[0], QMessageBox.Close)

        self.map_tool_change_signal.emit()

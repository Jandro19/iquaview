"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""Set of functions and classes to communicate with COLA2 through rosbridge."""

import json
import socket
import threading
import time
import logging
import queue

logger = logging.getLogger(__name__)


def send_action_service(ip, port, action_id, params):
    """
    Call a ROS service of type action.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param action_id: action service id
    :type action_id: str
    :param params: list of parameters
    :type params: list
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": action_id, "op": "call_service",
                    "args": {"param": params}}
    message = json.dumps(message_dict)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data['result']


def send_string_service(ip, port, action_id, param):
    """
    Call a ROS service of type string.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param action_id: action service id
    :type action_id: str
    :param param: list of parameters
    :type param: str
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": action_id, "op": "call_service",
                    "args": {"data": param}}
    message = json.dumps(message_dict)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data['result']


def send_service(ip, port, action_id, timeout=5):
    """
    Call a ROS service.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param action_id: action service id
    :type action_id: str
    :param timeout: socket timeout
    :type timeout: int
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": action_id, "op": "call_service",
                    "args": {}}
    message = json.dumps(message_dict)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.settimeout(timeout)
    s.connect((ip, port))
    s.send(message.encode())

    json_detector = JSONDetector()
    new_data = True
    data = ""
    while new_data:
        response = s.recv(1024).decode()
        result = json_detector.parse(response)
        if result:
            data = json.loads(result[-1])
            new_data = False
    s.close()
    return data


def send_empty_service(ip, port, action_id):
    """
    Call a ROS service of type Empty.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param action_id: action service id
    :type action_id: str
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": action_id, "op": "call_service",
                    "args": {}}
    message = json.dumps(message_dict)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data['result']


def send_trigger_service(ip, port, action_id):
    """Call a ROS service of type Trigger.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param action_id: action service id
    :type action_id: str
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": action_id, "op": "call_service",
                    "args": {}}
    message = json.dumps(message_dict)

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data


def send_mission_service(ip, port, service, mission):
    """
    Call a ROS service of type Mission
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param service: service id
    :type service: str
    :param mission: name of the mission
    :type mission: str
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": service,
                    "op": "call_service",
                    "args":
                        {"mission": mission}
                    }
    message = json.dumps(message_dict)
    logger.info('send: {} '.format(message))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data


def send_goto_service(ip, port, service, altitude, altitude_mode, x, y, z, surge, tolerance_x, tolerance_y,
                      tolerance_z):
    """
    Call a ROS service of type GOTO.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param service: service id
    :type service: str
    :param altitude: altitude from bottom
    :type altitude: float
    :param altitude_mode: True if altitude mode is enabled, otherwise False
    :type altitude_mode: bool
    :param x: x position
    :type x: float
    :param y: y position
    :type y: float
    :param z: z position
    :type z: float
    :param surge: surge velocity
    :type surge: float
    :param tolerance_x: tolerance on x to reach position
    :type tolerance_x: float
    :param tolerance_y: tolerance on y to reach position
    :type tolerance_y: float
    :param tolerance_z: tolerance on z to reach position
    :type tolerance_z: float
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": service,
                    "op": "call_service",
                    "args":
                        {"yaw": 0.0,
                         "altitude": altitude,
                         "altitude_mode": altitude_mode,
                         "blocking": False,
                         "priority": 10,
                         "reference": 1,
                         "position": {
                             "x": x,
                             "y": y,
                             "z": z
                         },
                         "disable_axis": {
                             "x": False,
                             "y": True,
                             "z": False,
                             "roll": True,
                             "pitch": True,
                             "yaw": False
                         },
                         "position_tolerance": {
                             "x": tolerance_x,
                             "y": tolerance_y,
                             "z": tolerance_z
                         },
                         "linear_velocity": {
                             "x": surge,
                             "y": 0.0,
                             "z": 0.5
                         }
                         }
                    }
    message = json.dumps(message_dict)
    logger.info('send: {} '.format(message))
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data


def get_ros_param(ip, port, name):
    """
    Obtain param from ROS param server.
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param name: name of the ros param
    :type name: str
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": "/rosapi/get_param", "op": "call_service",
                    "args": {"name": name}}
    message = json.dumps(message_dict)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data['values']


def set_ros_param(ip, port, name, value):
    """
    Set param to ROS param server.
    value if number = "1", "1234,12" ... if boolean "True" or "False"
       if string "\"string\"".
    :param ip: the ip address
    :type ip: str
    :param port: the port
    :type port: int
    :param name: name of the ros param
    :type name: str
    :param value: value of the ros param get by name
    :type value: str
    :return: return the dict received
    :rtype: dict
    """
    message_dict = {"service": "/rosapi/set_param", "op": "call_service",
                    "args": {"name": name, "value": value}}
    message = json.dumps(message_dict)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((ip, port))
    s.send(message.encode())
    response = s.recv(1024)
    data = json.loads(response.decode())
    s.close()
    return data['result']


class JSONDetector:
    def __init__(self):
        self.depth = 0
        self.in_field = False
        self.escape = False
        self.previously_closing = False
        self.json_str = ""

    def parse(self, data):
        json_list = []
        for c in data:
            self.json_str += c
            if not self.in_field:
                if c == "}":
                    self.depth -= 1
                    self.previously_closing = True
                else:
                    if c == "{":
                        if self.previously_closing and self.depth != 0:
                            logger.warning("Unexpected new JSON")
                            self.depth = 0
                            self.json_str = "{"
                        self.depth += 1
                    elif c == "\"":
                        self.in_field = True
                    elif not c in ",:.[]-+ e0123456789falstrun":
                        # Only numbers, lists, false, true and null are allowed
                        logger.warning("Unexpected char between fields: {}".format(c))
                        self.in_field = True
                    self.previously_closing = False
            else:
                if self.escape:
                    self.escape = False
                else:
                    if c == "\"":
                        self.in_field = False
                    elif c == "\\":
                        self.escape = True
            if self.depth == 0:
                json_list.append(self.json_str)
                self.json_str = ""
        return json_list


class SubscribeToTopic:
    """Class helper to subscribe to ROS topic using rosbridge."""

    def __init__(self, ip, port, topic, timeout=5, buffer_enabled=False):
        try:
            """Class constructor."""
            self.retries = 0
            self.ip = ip
            self.port = port
            message_dict = {"op": "subscribe", "topic": topic, "throttle_rate": 1}
            self.message = json.dumps(message_dict)
            self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.s.settimeout(timeout)
            self.buffer = list()
            self.buffer_enabled = buffer_enabled
            self.data = dict()
            self.response_queue = queue.Queue()
            self.keepgoing = True
            self.json_detector = JSONDetector()

            self.t_read = threading.Thread(target=self.read_topic)
            self.t_read.daemon = True

            self.t_decode = threading.Thread(target=self.decode_message)
            self.t_decode.daemon = True

        except:
            raise

    def subscribe(self):
        """ Connect socket and start threads to read and decode message"""
        self.s.connect((self.ip, self.port))
        self.t_read.start()
        self.t_decode.start()

    def read_topic(self):
        """Infinite loop that recive messages from socket"""
        self.s.send(self.message.encode())
        while self.keepgoing:
            try:
                response = self.s.recv(4096).decode()
                self.response_queue.put(response)

            except Exception as e:
                send_decoded = json.loads(self.message)
                logger.warning("{} {} ".format(send_decoded['topic'], e))
                time.sleep(0.05)
                self.data['valid_data'] = 'old_data'
                self.retries += 1
                if self.retries > 5:
                    self.data['valid_data'] = 'disconnected'

    def decode_message(self):
        """ Loop that updates last topic data."""
        while self.keepgoing:
            d = dict()
            response = self.response_queue.get()
            if response:

                result = self.json_detector.parse(response)
                if result:
                    self.data = json.loads(result[-1])['msg']
                    self.data['valid_data'] = 'new_data'

                    if self.buffer_enabled:
                        for i in result:
                            item = json.loads(i)['msg']
                            item['valid_data'] = "new_data"
                            self.buffer.append(item)
            else:
                self.retries += 1
                if self.retries > 25:
                    self.data['valid_data'] = 'disconnected'

    def get_data(self):
        """Return the last topic data received."""
        return self.data

    def get_buffer(self):
        """Return the last topic data received in a buffer."""
        buffer = self.buffer[:]
        return buffer

    def clear_buffer(self):
        """Clear the data in the buffer."""
        self.buffer.clear()

    def get_keepgoing(self):
        """ Get keepgoing state."""
        return self.keepgoing

    def close(self):
        """ Set values to False and close socket"""
        self.keepgoing = False
        self.buffer_enabled = False
        # put element on queue to finish get on decode_message
        self.response_queue.put(False, False)
        self.s.close()


if __name__ == "__main__":
    IP = "127.0.0.1"
    port = 9090
    total_time = SubscribeToTopic(IP, port, "/cola2_safety/total_time")
    total_time.subscribe()
    time.sleep(3)
    logger.info(total_time.get_data()['total_time'])

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to define canvas markers to display positions in the map
"""

import math
from iquaview.src.utils.calcutils import distance_plane
from qgis.core import QgsPointXY, QgsDistanceArea, QgsProject
from qgis.gui import QgsMapCanvasItem
from PyQt5.QtCore import Qt, QRectF, QLine, QPoint, QFile, QIODevice, QByteArray
from PyQt5.QtGui import QBrush, QPen, QPainter, QPolygonF, QPainterPath
from PyQt5.QtSvg import QSvgRenderer


class CanvasMarker(QgsMapCanvasItem):
    def __init__(self,
                 canvas,
                 color,
                 svg=None,
                 width=0.0,
                 length=0.0,
                 orientation=True,
                 is_icon=False,
                 marker_mode=False,
                 config=None):
        """
        Init of the object CanvasMarker

        :param canvas: Canvas where the marker will be painted
        :type canvas: QgsMapCanvas
        :param color: Color that the marker will have
        :type color: Qt.GlobalColor
        :param svg: svg to render
        :type svg: str
        :param width: desired width
        :type width: float
        :param length: desired length
        :type length: float
        :param orientation: determines whether the marker will have orientation
        :type orientation: bool
        :param is_icon: determines if svg is an icon
        :type is_icon: bool
        :param marker_mode: determines if marker will read a marker mode
        :type marker_mode: bool
        :param config: config to read the marker mode, if any
        :type config: Config
        """

        super(CanvasMarker, self).__init__(canvas)
        self.canvas = canvas
        self.config = config
        self.size = 20
        self.changing_scale = 400
        self.is_icon = is_icon
        self.marker_mode = marker_mode
        self.color = color
        self.pointbrush = QBrush(self.color)
        self.pointpen = QPen(Qt.black)
        self.pointpen.setWidth(1)
        self.map_pos = QgsPointXY(0.0, 0.0)
        self.heading = 0
        self.width = width
        self.length = length
        self.orientation = orientation
        self.svg = svg
        self.svg_data = None
        if svg is not None:
            # set crs and ellipsoid
            crs = self.canvas.mapSettings().destinationCrs()
            self.canvas.destinationCrsChanged.connect(self.canvas_crs_changed)
            self.distance_calc = QgsDistanceArea()
            self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
            self.distance_calc.setEllipsoid(crs.ellipsoidAcronym())

            # Load the svg file
            svg_file = QFile(svg)
            svg_file.open(QIODevice.ReadOnly)
            self.svg_data = svg_file.readAll()

            # find the base color in the metadata. If it is not found, change_marker_color will have no effect.
            metadata_id = "base_color"
            pos = self.svg_data.indexOf("base_color")
            if pos != -1:
                metadata_value = self.svg_data.mid(pos + len(metadata_id) + 2, 7)
                self.svg_color = bytes(metadata_value).decode()
                self.set_color(self.color)
            else:
                self.svg_color = None

    def canvas_crs_changed(self):
        """
        If canvas crs changed updates crs
        """
        crs = self.canvas.mapSettings().destinationCrs()
        self.distance_calc.setSourceCrs(crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid(crs.ellipsoidAcronym())

    def set_size(self, size):
        """
        Sets the size of the marker
        :param size: Desired size of the marker (in pixels)
        :type size: int
        """
        self.size = size

    def set_color(self, color):
        """
        Sets the color of the marker. If the marker is from an svg, changing the color will only work
        if svg_color was found in the metadata of the svg when constructing this object.
        :param color: Desired color of the marker
        :type color: QColor
        """
        self.color = color

        # For markers that render with svg and can change color
        if self.svg is not None and self.svg_color is not None:
            self.svg_data.replace(QByteArray().append(self.svg_color), QByteArray().append(color.name()))
            self.svg_color = color.name()

    def set_marker_mode(self, canvas_marker_mode):
        """
        Sets marker_mode to the value specified
        :param canvas_marker_mode: Value to set marker_mode
        :type canvas_marker_mode: bool
        """
        self.marker_mode = canvas_marker_mode

    def paint(self, painter, option=None, widget=None):
        """
        Overrides paint method of QgsMapCanvasItem, paints the marker to the canvas

        :param painter:
        :type painter: QPainter
        :param option: describes the parameters needed to draw a QGraphicsItem
        :type option: QStyleOptionGraphicsItem
        :param widget: A concrete widget to paint over, None by default
        :type widget: QWidget
        """
        pos = self.toCanvasCoordinates(self.map_pos)
        self.setPos(pos)

        if self.marker_mode:
            mode = self.config.csettings["canvas_marker_mode"]

            if mode == 'auto':
                self.set_size(20)
                transform = self.canvas.getCoordinateTransform()
                start_point = transform.toMapCoordinates(pos.x(), pos.y())

                map_end_point_width = self.distance_calc.measureLineProjected(start_point, self.width, math.radians(90 + math.degrees(self.heading)))[1]
                map_end_point_length = self.distance_calc.measureLineProjected(start_point, self.length, self.heading)[1]

                # to canvas coordinates
                canvas_end_point_width = self.toCanvasCoordinates(map_end_point_width)
                canvas_end_point_length = self.toCanvasCoordinates(map_end_point_length)

                width = distance_plane(QgsPointXY(self.toCanvasCoordinates(start_point)), QgsPointXY(canvas_end_point_width))
                height = distance_plane(QgsPointXY(self.toCanvasCoordinates(start_point)), QgsPointXY(canvas_end_point_length))

                if width < self.size and height < self.size:
                    self.changing_scale = self.canvas.scale()
                else:
                    self.changing_scale = self.canvas.scale() * 2

            elif mode == 'manual':
                self.changing_scale = self.config.csettings["canvas_marker_scale"]
        else:
            self.changing_scale = 400

        if self.svg_data is None or (not self.is_icon and self.canvas.scale() >= self.changing_scale):
            self.set_size(20)
            half_size = self.size / 2.0
            rect = QRectF(0 - half_size, 0 - half_size, self.size, self.size)
            painter.setRenderHint(QPainter.Antialiasing)

            self.pointpen.setColor(Qt.black)
            self.pointpen.setWidth(2)
            self.pointbrush.setColor(self.color)

            painter.setBrush(self.pointbrush)
            painter.setPen(self.pointpen)
            y = 0 - half_size
            x = rect.width() / 2 - half_size
            line = QLine(x, y, x, rect.height() - half_size)
            y = rect.height() / 2 - half_size
            x = 0 - half_size
            line2 = QLine(x, y, rect.width() - half_size, y)

            # Arrow
            p = QPolygonF()
            p.append(QPoint(0 - half_size, 0))
            p.append(QPoint(0, -self.size))
            x = rect.width() - half_size
            p.append(QPoint(x, 0))
            p.append(QPoint(0, 0))

            offsetp = QPolygonF()
            offsetp.append(QPoint(0 - half_size, 0))
            offsetp.append(QPoint(0, -self.size))
            x = rect.width() - half_size
            offsetp.append(QPoint(x, 0))
            offsetp.append(QPoint(0, 0))

            painter.save()
            painter.rotate(math.degrees(self.heading) + self.canvas.rotation())
            if self.orientation:
                path = QPainterPath()
                path.addPolygon(p)
                painter.drawPath(path)
            painter.restore()
            painter.drawEllipse(rect)
            painter.drawLine(line)
            painter.drawLine(line2)

        # svg read correctly
        elif self.svg_data is not None and len(self.svg_data) > 0:

            # get rotation
            rotation = self.canvas.rotation()

            painter.save()

            transform = self.canvas.getCoordinateTransform()
            start_point = transform.toMapCoordinates(pos.x(), pos.y())
            map_end_point_width = self.distance_calc.measureLineProjected(start_point, self.width,  math.radians(90 + math.degrees(self.heading)))[1]
            map_end_point_length = self.distance_calc.measureLineProjected(start_point, self.length, self.heading)[1]
            # to canvas coordinates
            canvas_end_point_width = self.toCanvasCoordinates(map_end_point_width)
            canvas_end_point_length = self.toCanvasCoordinates(map_end_point_length)

            if self.is_icon:
                width = self.width
                height = self.length
            else:
                width = distance_plane(self.toCanvasCoordinates(start_point), QgsPointXY(canvas_end_point_width))
                height = distance_plane(self.toCanvasCoordinates(start_point), QgsPointXY(canvas_end_point_length))

            if width > height:
                self.set_size(width)
            else:
                self.set_size(height)

            if width != 0 and height != 0:
                center_x = width / 2.0
                center_y = height / 2.0
                # work out how to shift the image so that it rotates
                #           properly about its center
                # ( x cos a + y sin a - x, -x sin a + y cos a - y)
                myradians = math.radians(rotation + math.degrees(self.heading))
                xshift = int(((center_x * math.cos(myradians)) +
                              (center_y * math.sin(myradians)))
                             - center_x)
                yshift = int(((-center_x * math.sin(myradians)) +
                              (center_y * math.cos(myradians)))
                             - center_y)

                painter.translate(-width / 2, -height / 2)
                painter.rotate(math.degrees(self.heading) + self.canvas.rotation())

                svg_renderer = QSvgRenderer(self.svg_data)
                if svg_renderer.isValid():
                    svg_renderer.render(painter, QRectF(xshift, yshift, width, height))

            painter.restore()

    def boundingRect(self):
        """
        Overrides boundingRect method of QgsMapCanvasItem, gives the bounding rectangle of the marker
        :return: Returns the bounding rectangle of the marker
        :rtype: QRectF
        """
        size = self.size * 2
        return QRectF(-size, -size, 2.0 * size, 2.0 * size)

    def updatePosition(self):
        """ Updates the position of the marker """
        self.set_center(self.map_pos, self.heading)

    def set_center(self, map_pos, heading=None):
        """
        Sets the position of the center of the marker
        :param map_pos: new position of the center
        :type map_pos: QgsPointXY
        :param heading: new heading of the marker
        :type heading: float (in Radians)
        """
        if heading is not None:
            self.heading = heading
        self.map_pos = map_pos
        self.setPos(self.toCanvasCoordinates(self.map_pos))

    def set_width(self, width):
        """
        Sets the width of the marker
        :param width: Desired width
        :type width: float
        """
        self.width = width

    def set_length(self, length):
        """
        Sets the length of the marker
        :param length: Desired length
        :type length: float
        """
        self.length = length

    def get_size(self):
        """
        Returns the size of the marker
        :return: Size of the marker
        :rtype: int
        """
        return self.size

    def get_color(self):
        """
        Returns the color of the marker
        :return: Color of the marker
        :rtype: QColor
        """
        return self.color

    def get_marker_mode(self):
        """
        Returns the marker mode
        :return: Mode of the marker
        :rtype: bool
        """
        return self.marker_mode

    def get_center(self):
        """
        Returns the center position of the marker
        :return: center of the marker
        :rtype: QPointF
        """
        return self.pos()

    def get_width(self):
        """
        Returns the width of the marker
        :return: width of the marker
        :rtype: float
        """
        return self.width

    def get_length(self):
        """
        Returns the length of the marker
        :return: length of the marker
        :rtype: float
        """
        return self.length
"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""
"""
 Widget to configure track appearance (color) and allow to clear track
 history and center track on the map.
"""

import logging
import math

from PyQt5.QtGui import QColor, QIcon
from PyQt5.QtWidgets import QWidget, QFileDialog

from qgis.core import (QgsRectangle,
                       QgsWkbTypes,
                       QgsVectorLayer,
                       QgsFeature,
                       QgsVectorFileWriter,
                       QgsCoordinateReferenceSystem,
                       QgsCoordinateTransform,
                       QgsProject)
from qgis.gui import QgsRubberBand, QgsColorButton

from iquaview.src.ui.ui_track import Ui_Track
from iquaview.src.canvastracks.canvasmarker import CanvasMarker


logger = logging.getLogger(__name__)


class TrackWidget(QWidget, Ui_Track):

    def __init__(self, parent=None):
        """ Init of the object TrackWidget """
        super(TrackWidget, self).__init__(parent)
        self.setupUi(self)
        self.title = "Display track"
        self.canvas = None
        self.band = None
        self.center = False
        self.position = None
        self.geom_type = None
        self.marker = None
        self.hidden = False
        self.current_crs = None
        self.connection_lost_marker = None
        self.has_connection_lost_marker = False
        self.centerButton.setEnabled(False)
        icon = QIcon(":/resources/mActionSave.svg")
        self.save_track_pushButton.setIcon(icon)
        self.save_track_pushButton.setToolTip("Save Track")
        self.save_track_pushButton.clicked.connect(self.save_track)
        icon = QIcon(":/resources/mActionZoomToArea.svg")
        self.centerButton.setIcon(icon)
        self.centerButton.setToolTip("Center and Zoom to Map")
        icon = QIcon(":/resources/eraser.svg")
        self.clearTrackButton.setIcon(icon)
        self.clearTrackButton.setToolTip("Clear Track")

    def init(self, title, canvas, default_color, geom_type, marker, has_connection_lost_marker=False):
        """
        Initialize variables

        :param title: Title that will be asigned to the widget
        :type title: str
        :param canvas: Canvas where the rubber band will be drawn
        :type canvas: QgsMapCanvas
        :param geom_type: Geometry type of the band that will be drawn
        :type geom_type: GeometryType
        :param default_color: Default color of the band that will be drawn
        :type default_color: QColor
        :param marker: Marker to be drawn
        :type marker: CanvasMarker
        :param has_connection_lost_marker: bool to know if trackwidget will have connection lost marker
        :type has_connection_lost_marker: bool
        """
        self.canvas = canvas
        self.current_crs = self.canvas.mapSettings().destinationCrs()
        self.geom_type = geom_type
        self.track_label.setText(title)
        self.has_connection_lost_marker = has_connection_lost_marker
        if marker:
            # Add marker
            self.marker = marker
            self.with_marker = True
        else:
            self.with_marker = False
        # Add rubber band
        self.band = QgsRubberBand(self.canvas, self.geom_type)
        if self.geom_type == QgsWkbTypes.PointGeometry:
            self.band.setIcon(QgsRubberBand.ICON_CIRCLE)
            self.band.setIconSize(12)
        else:
            self.band.setWidth(3)

        if has_connection_lost_marker:
            self.connection_lost_marker = CanvasMarker(canvas,
                                                       default_color,
                                                       ":/resources/connectionLost.svg",
                                                       float(20),
                                                       float(20),
                                                       is_icon=True,
                                                       orientation=False,
                                                       )
            self.connection_lost_marker.hide()

        # Add color button widget for picking color
        self.color_btn.colorChanged.connect(self.color_changed)
        self.color_btn.setDefaultColor(default_color)
        self.color_btn.setColor(default_color)
        # Set signals
        self.centerButton.clicked.connect(self.center_to_location)
        self.clearTrackButton.clicked.connect(self.clear_track)

        self.canvas.destinationCrsChanged.connect(self.canvas_crs_changed)

    def canvas_crs_changed(self):
        """
        Canvas crs changed. update points to new crs
        """
        if self.band.asGeometry().type() == QgsWkbTypes.LineGeometry:
            list_wp = self.band.asGeometry().asPolyline()
        else:
            list_wp = self.band.asGeometry().asMultiPoint()

        if self.current_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
            trans = QgsCoordinateTransform(self.current_crs,
                                           self.canvas.mapSettings().destinationCrs(),
                                           QgsProject.instance().transformContext())
        for wp in range(0, len(list_wp)):
            point = list_wp[wp]
            if point is not None:

                    pos = trans.transform(point.x(), point.y())
                    self.band.movePoint(wp, pos)

        if self.position is not None:
            last_pos = trans.transform(self.position.x(), self.position.y())
            self.position = last_pos
            if self.with_marker:
                self.marker.set_center(self.position)

            if self.has_connection_lost_marker:
                self.connection_lost_marker.set_center(self.position, math.radians(-self.canvas.rotation()))

        self.current_crs = self.canvas.mapSettings().destinationCrs()

    def color_changed(self):
        """
        Changes the color of the marker to match that of color_btn. Called when color_btn has its color changed.
        """
        transparent_color = QColor(self.color_btn.color())
        transparent_color.setAlpha(80)
        self.band.setColor(transparent_color)
        if self.with_marker:
            self.marker.set_color(QColor(self.color_btn.color()))

    def add_position(self, position):
        """
        Adds position to the band
        :param position: point to be added
        :type position: QgsPointXY
        """
        self.band.addPoint(position)

    def track_update_canvas(self, position, heading):
        """
        Updates position and heading of the marker if its used and adds the position to the band
        :param position: Position to update to
        :type position: QgsPointXY
        :param heading: Heading to update to (in Radians)
        :type heading: float
        """
        self.centerButton.setEnabled(True)
        self.position = position
        self.add_position(position)
        if self.with_marker:
            self.marker.set_center(position, heading)
        if self.isHidden():
            if self.with_marker:
                self.marker.hide()
            self.band.hide()
        else:
            if self.with_marker:
                self.marker.show()
            self.band.show()

        if self.has_connection_lost_marker:
            self.connection_lost_marker.set_center(position, math.radians(-self.canvas.rotation()))

    def center_to_location(self):
        """
        Center to last received position on the map.
        """
        rect = QgsRectangle(self.position, self.position)
        self.canvas.setExtent(rect)
        self.canvas.zoomScale(400)
        self.canvas.refresh()

    def hide_band(self):
        """ Hides the rubber band """
        self.band.hide()

    def hide_marker(self):
        """ Hides the marker if it is being used """
        if self.with_marker:
            self.marker.hide()

    def clear_track(self):
        """ Resets rubber band track, removing all its points """
        self.band.reset(self.geom_type)

    def save_track(self):
        """
        Save the track to disk
        """
        layer_name, selected_filter = QFileDialog.getSaveFileName(None, 'Save Track', "",
                                                                  'Shapefile (*.shp);;KML (*.kml);;GPX (*.gpx)')

        if layer_name != '':

            if self.geom_type == QgsWkbTypes.PointGeometry:
                geometric_object = "MultiPoint"
            else:
                geometric_object = "LineString"

            geometric_object += "?crs="+self.canvas.mapSettings().destinationCrs().authid()

            layer = QgsVectorLayer(
                geometric_object,
                layer_name,
                "memory")
            feature = QgsFeature()
            feature.setGeometry(self.band.asGeometry())
            layer.dataProvider().addFeatures([feature])

            if selected_filter == "Shapefile (*.shp)":

                if not layer_name.endswith('.shp'):
                    layer_name = layer_name + '.shp'
                ret = QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                              QgsCoordinateReferenceSystem(4326,
                                                                                           QgsCoordinateReferenceSystem.EpsgCrsId),
                                                              "ESRI Shapefile")
                if ret == QgsVectorFileWriter.NoError:
                    logger.info(layer.name() + " saved to " + layer_name)

            elif selected_filter == "KML (*.kml)":

                if not layer_name.endswith('.kml'):
                    layer_name = layer_name + '.kml'

                QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                        QgsCoordinateReferenceSystem(4326,
                                                                                     QgsCoordinateReferenceSystem.EpsgCrsId),
                                                        "KML")
            elif selected_filter == "GPX (*.gpx)":

                if not layer_name.endswith('.gpx'):
                    layer_name = layer_name + '.gpx'
                ds_options = list()
                ds_options.append("GPX_USE_EXTENSIONS=TRUE")
                QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                        QgsCoordinateReferenceSystem(4326,
                                                                                     QgsCoordinateReferenceSystem.EpsgCrsId),
                                                        "GPX",
                                                        datasourceOptions=ds_options)

    def get_band(self):
        """
        :return: Returns the track band
        :rtype: QgsRubberBand
        """
        return self.band

    def get_color_btn(self):
        """
        :return: Returns the button that sets the color of the track
        :rtype: QgsColorButton
        """
        return self.color_btn

    def close(self):
        """ Overrides close method from QWidget. Hides bands and markers"""
        self.hide_band()
        self.hide_marker()
        if self.has_connection_lost_marker:
            self.connection_lost_marker.hide()

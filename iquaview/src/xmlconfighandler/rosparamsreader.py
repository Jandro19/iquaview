"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Helper classes to read the xml structure associated to the ros_params tag in the AUV config file
"""

import logging
from iquaview.src.xmlconfighandler.xmlconfigparser import XMLConfigParser
from iquaview.src.cola2api import cola2_interface

logger = logging.getLogger(__name__)


class Field(object):
    def __init__(self, field_name=None, field_type=None):
        """
        Constructor
        :param field_name: name of the field
        :type field_name: str
        :param field_type: type of the field
        :type: str
        """
        self.field_name = field_name
        self.field_type = field_type
        self.field_range = list()
        self.field_min_limit = None
        self.field_max_limit = None

    def get_name(self):
        """
        Get field name
        :return: return field name
        :rtype: str
        """
        return self.field_name

    def get_type(self):
        """
        Get field type
        :return: return field type
        :rtype: str
        """
        return self.field_type

    def get_range(self):
        """
        Get range values
        :return: return range values in list
        :rtype: list
        """
        return self.field_range

    def get_min_limit(self):
        """
        Get min limit
        :return: return min limit
        :rtype: int/float
        """
        return self.field_min_limit

    def get_max_limit(self):
        """
        Get max limit
        :return: return max limit
        :rtype: int/float
        """
        return self.field_max_limit

    def has_range(self):
        """
        :return: returns true if field has a range, otherwise false
        """
        return len(self.field_range) != 0

    def has_limits(self):
        """
        :return: returns true if field has a limits, otherwise false
        """
        return ((self.field_min_limit is not None) and (self.field_max_limit is not None))

    def set_range(self, field_range):
        """
        Set range of values
        :param field_range: range of values
        :type field_range: list
        """
        self.field_range = field_range

    def set_min_limit(self, min_value):
        """
        Set min limit
        :param min_value: min limit
        :type min_value: float
        """
        self.field_min_limit = min_value

    def set_max_limit(self, max_value):
        """
        Set max limit
        :param max_value: max limit
        :type max_value: float
        """
        self.field_max_limit = max_value


class FieldArray(object):
    def __init__(self, field_array_name=None, field_array_type=None, field_array_size=None):
        """
        Constructor
        :param field_array_name: field array name
        :type field_array_name: str
        :param field_array_type: field array type
        :type field_array_type: str
        :param field_array_size: field array size
        :type field_array_size: str
        """
        self.field_array_name = field_array_name
        self.field_array_type = field_array_type
        self.field_array_size = field_array_size

    def get_name(self):
        """
        Get name
        :return: return field array name
        :rtype; str
        """
        return self.field_array_name

    def get_type(self):
        """
        Get type
        :return: return field array type
        :rtype: str
        """
        return self.field_array_type

    def get_size(self):
        """
        Get size
        :return: return field array size
        :rtype: str
        """
        return self.field_array_size


class Param(object):
    def __init__(self, description=None, param_value=None):
        """
        Constructor
        :param description: description of the parameter
        :type description: str:
        :param param_value: value of the parameter
        :type param_value: str
        """
        self.description = description
        self.field = None
        self.field_array = None
        self.param_value = param_value

    def get_description(self):
        """
        Get description
        :return: return description of the parameter
        :rtype: str
        """
        return self.description

    def get_value(self):
        """
        Get value
        :return: return value of the parameter
        :rtype: str
        """
        return self.param_value

    def get_name(self):
        """
        Get name of the field
        :return: name of the field
        :rtype: str
        """
        if self.field is not None:
            return self.field.get_name()
        else:
            return self.field_array.get_name()

    def get_type(self):
        """
        Get type of the field
        :return: return the type of the field
        :rtype : str
        """
        if self.field is not None:
            return self.field.get_type()
        else:
            return self.field_array.get_type()

    def get_array_size(self):
        """
        Get array size
        :return: return array size
        :rtype: str
        """
        if self.field_array is not None:
            return self.field_array.get_size()

    def get_range(self):
        """
        Get field range values
        :return: return range values in list
        :rtype: list
        """
        return self.field.get_range()

    def get_min_limit(self):
        """
        Get field min limit
        :return: return min limit
        :rtype: int/float
        """
        return self.field.get_min_limit()

    def get_max_limit(self):
        """
        Get field max limit
        :return: return max limit
        :rtype: int/float
        """
        return self.field.get_max_limit()

    def set_field(self, field_name, field_type):
        """
        Set field
        :param field_name: name of the field
        :type field_name: str
        :param field_type: type of the field
        :type field_type: str
        """
        self.field = Field(field_name, field_type)

    def set_field_array(self, field_array_name, field_array_type, field_array_size):
        """
        Set field  array
        :param field_array_name: field array name
        :type field_array_name:  str
        :param field_array_type: field array type
        :type field_array_type: str
        :param field_array_size: field array size
        :type field_array_size: str
        """
        self.field_array = FieldArray(field_array_name, field_array_type, field_array_size)

    def is_array(self):
        """

        :return: return True if field is a array, otherwise False
        :rtype: bool
        """
        return self.field_array is not None

    def has_range(self):
        """
        :return: returns true if field has a range, otherwise false
        """
        return self.field.has_range()

    def has_limits(self):
        """

        :return: returns true if field has a limits, otherwise false
        """
        return self.field.has_limits()

    def set_value(self, value):
        """

        :param value: value to set
        :type value: str
        """
        self.param_value = value

    def set_field_range(self, field_range):
        """
        Set field range
        :param field_range: range of values
        :type field_range: str
        """
        range_splitted = field_range.split(',')
        if self.field.get_type() == "double":
            range_list = [float(x) for x in range_splitted]
        else:
            range_list = [int(x) for x in range_splitted]

        self.field.set_range(range_list)

    def set_field_limits(self, minimum, maximum):
        """
        Set min/max limits
        :param minimum: min value
        :type minimum: str
        :param maximum: max value
        :type maximum: str
        """
        if self.field.get_type() == "int":
            self.field.set_min_limit(int(minimum))
            self.field.set_max_limit(int(maximum))
        else:
            self.field.set_min_limit(float(minimum))
            self.field.set_max_limit(float(maximum))


class Section(object):
    def __init__(self, description=None, params=None, action_id=None):
        """
        Constructor
        :param description: description of the section
        :type description: str
        :param params: parameters
        :type params: list
        :param action_id: id of the action
        :type action_id: str
        """
        self.description = description
        self.params = list()
        self.action_id = action_id

        if params is not None:
            for param in params:
                self.params.append(param)

    def set_description(self, description):
        """
        Set description
        :param description: description of the section
        :type description: str
        """
        self.description = description

    def set_action_id(self, action_id):
        """
        Set action ID
        :param action_id: action id
        :type action_id: str
        """
        self.action_id = action_id

    def add_param(self, param):
        """
        Add param
        :param param: param to add
        :type param: Param
        """
        self.params.append(param)

    def get_description(self):
        """
        Get description
        :return: return the description
        :rtype: str
        """
        return self.description

    def get_params(self):
        """
        Get list of parameters
        :return: list of parameters
        :rtype: list
        """
        return self.params

    def get_action_id(self):
        """
        Get action ID
        :return: return action id
        :rtype: str
        """
        return self.action_id


class RosParamsReader(object):
    def __init__(self, config_filename, ip, port, vehicle_namespace):
        """
        Constructor
        :param config_filename: configuration filename
        :type config_filename: str
        :param ip: ip address
        :type ip: str
        :param port: the port
        :type port: int
        :param vehicle_namespace: vehicle namespace
        :type vehicle_namespace: str
        """

        self.filename = config_filename
        self.ip = ip
        self.port = port
        self.vehicle_namespace = vehicle_namespace

    def read_configuration(self):
        """Read the configuration of the XML"""
        logger.debug("Reading  ros_params XML...")
        config_parser = XMLConfigParser(self.filename)
        # get ros_params
        ros_params = config_parser.first_match(config_parser.root, "ros_params")
        # all sections in ros_params
        sections = config_parser.all_matches(ros_params, "section")

        # initialize empty list of sections
        section_list = list()

        # fill section values by reading xml and corresponding param values in the param server
        for section in sections:
            sect = Section()
            logger.debug("section.tag")
            for value in section:
                # description
                if value.tag == 'description':
                    logger.debug("     {} {}".format(value.tag, value.text))
                    sect.set_description(value.text)
                # param
                if value.tag == 'param':
                    logger.debug("     {}".format(value.tag))
                    desc = config_parser.first_match(value, "description").text
                    param = Param(desc)

                    field = config_parser.first_match(value, "field")
                    if field is not None:
                        f_name_text = config_parser.first_match(field, "field_name").text
                        f_type_text = config_parser.first_match(field, "field_type").text

                        param.set_field(f_name_text, f_type_text)

                        f_range = config_parser.first_match(field, "field_range")
                        f_limits = config_parser.first_match(field, "field_limits")

                        if f_range is not None:
                            f_range_text = f_range.text
                            param.set_field_range(f_range_text)

                        elif f_limits is not None:
                            minimum = config_parser.first_match(f_limits, "min").text
                            maximum = config_parser.first_match(f_limits, "max").text
                            param.set_field_limits(minimum, maximum)

                    else:
                        field_array = config_parser.first_match(value, "field_array")
                        f_name_text = config_parser.first_match(field_array, "field_array_name").text
                        f_type_text = config_parser.first_match(field_array, "field_array_type").text
                        f_size_text = config_parser.first_match(field_array, "field_array_size").text
                        param.set_field_array(f_name_text, f_type_text, f_size_text)

                    p_value = cola2_interface.get_ros_param(self.ip, self.port,
                                                            self.vehicle_namespace + f_name_text)['value']

                    if f_type_text == 'ip':
                        p_value = p_value.replace("\"", "")
                    param.set_value(p_value)
                    logger.debug("         {}".format(desc))
                    logger.debug("         {}".format(f_name_text))
                    logger.debug("         {}".format(p_value))
                    logger.debug("         {}".format(f_type_text))

                    sect.add_param(param)
                # action_id
                if value.tag == 'action_id':
                    logger.debug("     {} {}".format(value.tag, value.text))
                    sect.set_action_id(value.text)

            section_list.append(sect)
        return section_list

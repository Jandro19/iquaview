"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to configure app.config parameters.
 It includes last_auv_config_xml, remote_vehicle_package, user, coordinate display.
"""

import logging

from PyQt5.QtWidgets import QDialog
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import Qt

from iquaview.src.ui.ui_options import Ui_OptionsDlg
from iquaview.src.generaloptions import GeneralOptionsWidget
from iquaview.src.styles import StylesWidget
from iquaview.src.tools.vesselpossystem import VesselPositionSystem
from iquaview.src.connection.settings.connectionsettings import ConnectionSettingsWidget
from iquaview.src.connection.dataoutputmanager import DataOutputManager
logger = logging.getLogger(__name__)

GENERAL = 0
CONNECTION_SETTINGS = 1
STYLES = 2
VESSEL_POSITION_SYSTEM = 3
DATA_OUTPUT_CONNECTION = 4


class OptionsDlg(QDialog, Ui_OptionsDlg):
    def __init__(self, config, vehicle_info, vehicle_data, canvas, mission_ctrl, north_arrow, scale_bar, usbl_module=None, parent=None):
        """
        Constructor
        :param config: iquaview configuration
        :type config: Config
        :param vehicle_info: Information about AUV
        :type vehicle_info:  VehicleInfo
        :param vehicledata: data received from the AUV
        :type vehicledata: VehicleData
        :param canvas: canvas where repaint events will be triggered
        :type QgsMapCanvas
        :param mission_ctrl: mission controller to get the missions from
        :type mission_ctrl: MissionController
        :param north_arrow: Canvas item to change color
        :type north_arrow: NorthArrow
        :param scale_bar: Canvas item to change color
        :type scale_bar: ScaleBar
        """
        super(OptionsDlg, self).__init__(parent)
        self.setupUi(self)
        self.setWindowTitle("Options")
        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.canvas = canvas
        self.mission_ctrl = mission_ctrl
        self.north_arrow = north_arrow
        self.scale_bar = scale_bar
        self.usbl_module = usbl_module

        # set icons
        self.mOptionsListWidget.item(GENERAL).setIcon(QIcon(":/resources/mActionOptions.svg"))
        self.mOptionsListWidget.item(CONNECTION_SETTINGS).setIcon(QIcon(":/resources/mActionConnectionSettings.svg"))
        self.mOptionsListWidget.item(STYLES).setIcon(QIcon(":/resources/mActionStyles.svg"))
        self.mOptionsListWidget.item(VESSEL_POSITION_SYSTEM).setIcon(QIcon(":/resources/mActionVesselPosSystem.svg"))
        self.mOptionsListWidget.item(DATA_OUTPUT_CONNECTION).setIcon(QIcon(":/resources/uploadPosition.svg"))

        self.general_options_widget = GeneralOptionsWidget(self.config, self.vehicle_info, self)
        self.connection_settings_widget = ConnectionSettingsWidget(self.config, self.vehicle_info, self)
        self.styles_widget = StylesWidget(self.config, self.canvas, self.mission_ctrl, self.north_arrow,
                                          self.scale_bar)
        self.vessel_pos_system_widget = VesselPositionSystem(self.config, parent=None)
        self.data_output_manager_widget = DataOutputManager(self.config, self.vehicle_data, self.usbl_module)

        self.mOptionsStackedWidget.addWidget(self.general_options_widget)
        self.mOptionsStackedWidget.addWidget(self.connection_settings_widget)
        self.mOptionsStackedWidget.addWidget(self.styles_widget)
        self.mOptionsStackedWidget.addWidget(self.vessel_pos_system_widget)
        self.mOptionsStackedWidget.addWidget(self.data_output_manager_widget)
        self.update_options()

        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)

        self.applyButton.clicked.connect(self.on_apply)
        self.closeButton.clicked.connect(self.on_cancel)
        self.saveButton.clicked.connect(self.on_accept)

    def set_current_page(self, item):
        """
        Change item in Stacked widget
        :param item: position of the page
        :type item: int
        """
        self.mOptionsListWidget.currentRowChanged.disconnect(self.set_current_page)
        self.mOptionsStackedWidget.setCurrentIndex(item)
        self.mOptionsListWidget.setCurrentRow(item)
        self.mOptionsListWidget.currentRowChanged.connect(self.set_current_page)
        self.update_options()

    def update_options(self):
        """Update and reload menu options"""
        self.general_options_widget.reload()
        self.connection_settings_widget.load_settings()
        self.styles_widget.reload()
        self.vessel_pos_system_widget.reload()
        self.data_output_manager_widget.reload()

    def keyPressEvent(self, e) -> None:
        """ Override event handler keyPressEvent"""
        if e.key() == Qt.Key_Escape:
            self.on_cancel()
            super(OptionsDlg, self).keyPressEvent(e)

    def closeEvent(self, event):
        """ Override of the closeEvent methond. Calls on_cancel to restore changed colors """
        self.on_cancel()

    def on_apply(self):

        if self.mOptionsStackedWidget.currentIndex() == GENERAL:
            self.general_options_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == CONNECTION_SETTINGS:
            self.connection_settings_widget.apply_settings()

        elif self.mOptionsStackedWidget.currentIndex() == STYLES:
            self.styles_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == VESSEL_POSITION_SYSTEM:
            self.vessel_pos_system_widget.on_apply()

        elif self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION:
            self.data_output_manager_widget.on_apply()

    def on_accept(self):
        """On click 'Save as Defaults' accept"""
        accept = True        
        if self.mOptionsStackedWidget.currentIndex() == GENERAL:
            accept = self.general_options_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == CONNECTION_SETTINGS:
            accept = self.connection_settings_widget.save()

        elif self.mOptionsStackedWidget.currentIndex() == STYLES:
            accept = self.styles_widget.save_as_defaults()

        elif self.mOptionsStackedWidget.currentIndex() == VESSEL_POSITION_SYSTEM:
            accept = self.vessel_pos_system_widget.on_accept()

        elif self.mOptionsStackedWidget.currentIndex() == DATA_OUTPUT_CONNECTION:
            accept = self.data_output_manager_widget.on_accept()

        if accept:
            self.accept()

    def on_cancel(self):

        if self.mOptionsStackedWidget.currentIndex() == STYLES:
            self.styles_widget.on_cancel()

        self.reject()

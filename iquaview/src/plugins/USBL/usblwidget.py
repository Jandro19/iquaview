"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Widget to connect the USBL device and display information from it.
 It displays the USBL track, GPS track and the track according to the AUV navigation
 that is transmitted back acoustically in the map canvas.
 Also shows some status information retrieved from the received acoustic communication.
"""

import math
import time
import logging
from importlib import util

from PyQt5.QtWidgets import QWidget, QMessageBox
from PyQt5.QtCore import QTimer, Qt, pyqtSignal
from PyQt5.QtGui import QColor

from qgis.core import QgsPointXY, QgsWkbTypes, QgsCoordinateTransform, QgsCoordinateReferenceSystem, QgsProject

from iquaview.src.ui.ui_usbl import Ui_USBLWidget
from iquaview.src.canvastracks.canvasmarker import CanvasMarker
from iquaview.src.cola2api.gps_driver import gps_fix_quality_to_string
from iquaview.src.utils import calcutils

if util.find_spec('iquaview_evologics_usbl') is not None:
    import iquaview_evologics_usbl

logger = logging.getLogger(__name__)


def version():
    return "iquaview_evologics_usbl " + iquaview_evologics_usbl.__version__


class USBLWidget(QWidget, Ui_USBLWidget):
    usbl_connected = pyqtSignal(bool)
    gpsconnectionfailed = pyqtSignal()
    mission_started = pyqtSignal()
    mission_stopped = pyqtSignal()

    def __init__(self, canvas, config, vehicle_info, mission_sts, parent=None):
        super(USBLWidget, self).__init__(parent)
        self.setupUi(self)

        self.canvas = canvas
        self.config = config
        self.vehicle_info = vehicle_info
        self.mission_sts = mission_sts
        self.data = None
        self.data_auv = None
        self.data_gps = None

        self.default_color_gps = QColor(Qt.darkGreen)
        width = self.config.csettings["vessel_width"]
        length = self.config.csettings["vessel_length"]
        self.marker_gps = CanvasMarker(self.canvas, self.default_color_gps, ":/resources/vessel.svg", width, length,
                                       marker_mode=True, config=config)

        self.trackwidget_gps.init("GPS track",
                                  self.canvas,
                                  self.default_color_gps,
                                  QgsWkbTypes.LineGeometry,
                                  self.marker_gps,
                                  has_connection_lost_marker=True)

        self.default_color_usbl = Qt.red
        self.marker_usbl = CanvasMarker(self.canvas, self.default_color_usbl,
                                        None, orientation=False)
        self.trackwidget_usbl.init("USBL track",
                                   self.canvas,
                                   self.default_color_usbl,
                                   QgsWkbTypes.PointGeometry,
                                   self.marker_usbl,
                                   has_connection_lost_marker=True)

        self.default_color_auv = QColor(Qt.darkYellow)
        self.default_color_auv.setAlpha(80)
        #":/resources/" + vehicle_info.get_vehicle_type() + "/vehicle.svg"
        self.marker_auv = CanvasMarker(self.canvas, self.default_color_auv,
                                       None,
                                       float(vehicle_info.get_vehicle_width()),
                                       float(vehicle_info.get_vehicle_length()))

        self.trackwidget_auv.init("AUV track",
                                  self.canvas,
                                  self.default_color_auv,
                                  QgsWkbTypes.LineGeometry,
                                  self.marker_auv,
                                  has_connection_lost_marker=True)
        # set signals
        self.connectButton.clicked.connect(self.connect)

        self.connected = False
        self.gps_connection_failed = False
        self.set_label_disconnected()
        self.timer = QTimer()
        self.timer.timeout.connect(self.usbl_update_canvas)
        self.gpsconnectionfailed.connect(self.stop_canvas_updates)

        self.last_ping = 0
        self.last_usbl_time = 0.0
        self.last_auv_time = 0.0


        # self.waiting_mission_start = True
        # self.waiting_mission_stop = True
        # self.t_check_mission_start = threading.Thread(target=self.check_mission_start)
        # self.t_check_mission_start.daemon = True
        # self.t_check_mission_stop = threading.Thread(target=self.check_mission_stop)
        # self.t_check_mission_stop.daemon = True

        self.controller = iquaview_evologics_usbl.EvologicsUSBLController(config, vehicle_info.get_vehicle_code())

    def connect(self):
        if not self.controller.is_connected():
            try:
                self.controller.connect()

                self.connectButton.setText("Disconnect")
                self.timer.start(1000)
                self.usbl_status_label.setText("Connected")
                self.usbl_status_label.setStyleSheet('font:italic; color:green')
                self.usbl_connected.emit(True)

                if self.trackwidget_auv.connection_lost_marker.isVisible():
                    self.trackwidget_auv.connection_lost_marker.hide()
                if self.trackwidget_gps.connection_lost_marker.isVisible():
                    self.trackwidget_gps.connection_lost_marker.hide()
                if self.trackwidget_usbl.connection_lost_marker.isVisible():
                    self.trackwidget_usbl.connection_lost_marker.hide()

                self.gps_connection_failed = False
                if not self.controller.e_usbl.get_gps_initialized():
                    self.stop_canvas_updates()

            except Exception as e:
                logger.error("No connection to USBL: {}".format(e))
                QMessageBox.critical(self, "USBL Connection Failed",
                                     "Connection with USBL could not be established: \n"+str(e),
                                     QMessageBox.Close)
                self.set_label_disconnected()
                # self.connected = False
                self.disconnect()
        else:
            self.disconnect()

    def usbl_update_canvas(self):
        if self.controller.is_connected():
            pos_auv = None
            pos_usbl = None
            auv_heading = None

            data = self.controller.usbl_update_sensed_on_surface_data()
            data_auv = self.controller.usbl_update_auv_data()
            data_gps = self.controller.usbl_update_gps_data()

            self.data = data
            self.data_auv = data_auv
            self.data_gps = data_gps

            usbl_time = float(data['time'])
            if usbl_time != 0.0 and usbl_time != self.last_usbl_time:  # if valid and new data
                usbl_lat = float(data['latitude'])
                usbl_lon = float(data['longitude'])
                usbl_height = -float(data['depth'])  # negative
                usbl_time = float(data['time'])

                usbl_crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
                if usbl_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():

                    trans = QgsCoordinateTransform(usbl_crs,
                                                   self.canvas.mapSettings().destinationCrs(),
                                                   QgsProject.instance().transformContext())
                    pos_usbl = trans.transform(usbl_lon, usbl_lat)
                else:
                    pos_usbl = QgsPointXY(usbl_lon, usbl_lat)

                usbl_position = "{:.5F}, {:.5F}".format(usbl_lat, usbl_lon)
                self.usbl_position_label.setText(usbl_position)
                self.usbl_depth_label.setText("{:.3F}".format(usbl_height))
                self.last_ping = 0
                self.last_usbl_time = usbl_time

            else:
                self.last_ping = self.last_ping + 1
            self.usbl_lastping_label.setText(str(self.last_ping))

            auv_time = float(data_auv['time'])
            if auv_time != 0.0 and auv_time != self.last_auv_time:  # if valid data
                logger.info("Received data back from AUV")
                auv_lat = float(data_auv['latitude'])
                auv_lon = float(data_auv['longitude'])
                auv_depth = float(data_auv['depth'])
                auv_heading = float(data_auv['heading'])
                auv_error = int(data_auv['error'])
                self.last_auv_time = auv_time

                auv_crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
                if auv_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():

                    trans = QgsCoordinateTransform(auv_crs,
                                                   self.canvas.mapSettings().destinationCrs(),
                                                   QgsProject.instance().transformContext())
                    pos_auv = trans.transform(auv_lon, auv_lat)
                else:
                    pos_auv = QgsPointXY(auv_lon, auv_lat)

                auv_position = "{:.5F}, {:.5F}".format(auv_lat, auv_lon)
                self.auv_position_label.setText(auv_position)
                self.auv_depth_label.setText("{:.3F}".format(auv_depth))

                code = self.vehicle_info.get_vehicle_code()
                if code == 'status_code':
                    self.mission_sts.assign_status(auv_error)
                else:
                    self.mission_sts.assign_status_error_code(auv_error)
                [status, status_color] = self.mission_sts.get_status()
                if status == "":
                    status = "connected"
                    status_color = 'green'
                self.usbl_status_label.setText(status)
                self.usbl_status_label.setStyleSheet('font:italic; color:{}'.format(status_color))

            if data_gps is not None and data_gps['status'] == 'new_data':

                if (data_gps['quality'] >= 1) and (data_gps['quality'] <= 5):
                    gps_lat = data_gps['latitude']
                    gps_lon = data_gps['longitude']
                    gps_heading = data_gps['heading']
                    gps_quality = data_gps['quality']

                    brng = (math.radians(gps_heading - self.config.csettings['gps_offset_heading'])
                            + math.atan2(self.config.csettings['gps_offset_y'] + self.config.csettings['vrp_offset_y'],
                                         self.config.csettings['gps_offset_x'] + self.config.csettings['vrp_offset_x']))
                    distance = - math.sqrt(
                        math.pow(self.config.csettings['gps_offset_x'] + self.config.csettings['vrp_offset_x'], 2)
                        + math.pow(self.config.csettings['gps_offset_y'] + self.config.csettings['vrp_offset_y'],
                                   2))  # Distance in m

                    gps_lon, gps_lat = calcutils.endpoint_sphere(QgsPointXY(gps_lon, gps_lat), distance,
                                                                 math.degrees(brng))

                    gps_crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
                    if gps_crs.authid() != self.canvas.mapSettings().destinationCrs().authid():
                        trans = QgsCoordinateTransform(gps_crs,
                                                       self.canvas.mapSettings().destinationCrs(),
                                                       QgsProject.instance().transformContext())
                        pos_gps = trans.transform(gps_lon, gps_lat)
                    else:
                        pos_gps = QgsPointXY(gps_lon, gps_lat)

                    # update canvas
                    self.trackwidget_gps.track_update_canvas(pos_gps,
                                                             math.radians(
                                                                 gps_heading - self.config.csettings['gps_offset_heading']))

                # update canvas
                if pos_usbl is not None:
                    self.trackwidget_usbl.track_update_canvas(pos_usbl, 0)
                if pos_auv is not None and auv_heading is not None:
                    self.trackwidget_auv.track_update_canvas(pos_auv, auv_heading)

                # Show gps quality
                self.gps_fix_quality_label.setText(gps_fix_quality_to_string(data_gps['quality']))

            elif data_gps['status'] == 'old_data':
                now = time.time()
                if (now - data_gps['time']) > 5.0:
                     self.gpsconnectionfailed.emit()

    def send_abort_and_surface(self):
        self.controller.send_abort_and_surface()
        self.usbl_status_label.setText("Sending Abort and Surface...")

    def send_emergency_surface(self):
        self.controller.send_emergency_surface()
        self.usbl_status_label.setText("Sending Emergency Surface...")

    def send_informative(self):
        logger.info("Setting USBL command to Informative")
        self.controller.send_informative()
        self.usbl_status_label.setText("Sending Informative Cmd...")

    def send_start_mission(self):
        logger.info("Start mission from  USBL...")
        self.usbl_status_label.setText("Sending Start Mission...")
        self.controller.send_start_mission()

    def send_stop_mission(self):
        logger.info("Stop mission from  USBL...")
        self.controller.send_stop_mission()
        self.usbl_status_label.setText("Sending Stop Mission...")

    def set_enable_update(self, enable):
        logger.info("Set ", enable, "updates from USBL...")
        self.controller.set_enable_update(enable)

    def send_reset_timeout(self):
        logger.info("Send reset timeout from USBL...")
        self.controller.send_reset_timeout()
        self.usbl_status_label.setText("Sending Reset Timeout...")

    def stop_canvas_updates(self):
        self.trackwidget_usbl.close()
        self.trackwidget_gps.close()
        self.trackwidget_auv.close()

        if not self.gps_connection_failed:
            QMessageBox.critical(self,
                                 "Gps connection failed",
                                 "Gps connection was lost. "
                                 "You have connection with the USBL but you can not show the position on the map",
                                 QMessageBox.Close)
            self.gps_connection_failed = True


    def clear_tracks(self):
        """ Clears the usbl, gps and auv tracks and markers """
        self.trackwidget_usbl.close()
        self.trackwidget_gps.close()
        self.trackwidget_auv.close()

    def set_label_disconnected(self):
        self.usbl_status_label.setText("Disconnected")
        self.usbl_status_label.setStyleSheet('font:italic; color:red')
        self.gps_fix_quality_label.setText("")

    def check_mission_start(self):
        while self.waiting_mission_start:
            logger.info("Waiting for mission to start...")
            mission_executing = self.mission_sts.is_mission_in_execution()
            if mission_executing:
                self.controller.send_informative()
                self.waiting_mission_start = False
                self.usbl_status_label.setText("Mission Started")
                self.mission_started.emit()
            time.sleep(1.0)

    def check_mission_stop(self):
        while self.waiting_mission_stop:
            mission_executing = self.mission_sts.is_mission_in_execution()
            if not mission_executing:
                self.controller.send_informative()
                self.waiting_mission_stop = False
                self.usbl_status_label.setText("Mission Stopped")
                self.mission_stopped.emit()
            time.sleep(1.0)

    def is_connected(self):
        return self.controller.is_connected()

    def update_width_and_length(self):
        width = self.config.csettings["vessel_width"]
        length = self.config.csettings["vessel_length"]
        self.marker_gps.set_width(width)
        self.marker_gps.set_length(length)
        self.usbl_update_canvas()

    def get_usbl_data(self):
        return self.data

    def get_auv_data(self):
        return self.data_auv

    def get_gps_data(self):
        return self.data_gps

    def disconnect(self):
        """ Sets the widget as disconnected """
        self.last_ping = 0
        self.last_usbl_time = 0.0
        self.last_auv_time = 0.0

        self.controller.disconnect()

        if self.timer:
            self.timer.stop()

        self.connectButton.setText("Connect")
        self.set_label_disconnected()
        self.usbl_connected.emit(False)

        if not self.trackwidget_auv.connection_lost_marker.isVisible():
            self.trackwidget_auv.connection_lost_marker.show()
        if not self.trackwidget_gps.connection_lost_marker.isVisible():
            self.trackwidget_gps.connection_lost_marker.show()
        if not self.trackwidget_usbl.connection_lost_marker.isVisible():
            self.trackwidget_usbl.connection_lost_marker.show()

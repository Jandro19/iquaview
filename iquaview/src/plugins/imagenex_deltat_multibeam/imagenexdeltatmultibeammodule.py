"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
    Class to add the Imagenex Deltat multibeam functions to the mainwindow
"""

from importlib import util

from PyQt5.QtWidgets import QAction
from PyQt5.QtCore import QObject, Qt

if util.find_spec('iquaview_imagenex_deltat_multibeam') is not None:
    import iquaview_imagenex_deltat_multibeam


class ImagenexDeltatMultibeamModule(QObject):

    def __init__(self, proj, canvas, config, vehicle_info, vehicle_data, menubar, parent=None):
        super(QObject, self).__init__(parent)

        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.proj = proj
        self.canvas = canvas
        self.imagenex_deltat_multibeam_action = QAction(
            "Imagenex Deltat Multibeam",
            self)
        menubar.addActions([self.imagenex_deltat_multibeam_action])
        self.imagenex_deltat_multibeam_action.triggered.connect(self.open_imagenex_deltat_multibeam)

        self.imagenex_deltat_multibeam_dockwidget = iquaview_imagenex_deltat_multibeam.ImagenexMultibeamDockWidget(self.vehicle_info)
        parent.addDockWidget(Qt.TopDockWidgetArea, self.imagenex_deltat_multibeam_dockwidget)
        self.imagenex_deltat_multibeam_dockwidget.hide()

    def open_imagenex_deltat_multibeam(self):
        """ Open imagenex deltat multibeam dockwidget"""
        self.imagenex_deltat_multibeam_dockwidget.connect()
        self.imagenex_deltat_multibeam_dockwidget.show()

    @staticmethod
    def version():
        return "iquaview_imagenex_deltat_multibeam " + iquaview_imagenex_deltat_multibeam.__version__

    def disconnect_module(self):
        """ Close dialog"""
        self.imagenex_deltat_multibeam_dockwidget.close()

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
    Class to add the Imagenex Sidescan functions to the mainwindow
"""

from importlib import util

from PyQt5.QtWidgets import QAction
from PyQt5.QtCore import QObject

if util.find_spec('iquaview_imagenex_sidescan') is not None:
    import iquaview_imagenex_sidescan


class ImagenexSidescanModule(QObject):

    def __init__(self, proj, canvas, config, vehicle_info, vehicle_data, menubar):
        super(QObject, self).__init__()

        self.config = config
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicle_data
        self.proj = proj
        self.canvas = canvas
        self.imagenex_sidescan_action = QAction(
            "Imagenex Sidescan",
            self)
        menubar.addActions([self.imagenex_sidescan_action])
        self.imagenex_sidescan_action.triggered.connect(self.open_imagenex_sidescan)

        self.imagenex_sidescan_dlg = iquaview_imagenex_sidescan.ImagenexSidescanDlg(self.vehicle_info)

    def open_imagenex_sidescan(self):
        """ Open imagenex sidescan dialog"""
        self.imagenex_sidescan_dlg.connect()
        self.imagenex_sidescan_dlg.showMaximized()

    @staticmethod
    def version():
        return "iquaview_imagenex_sidescan " + iquaview_imagenex_sidescan.__version__

    def disconnect_module(self):
        """ Close dialog"""
        self.imagenex_sidescan_dlg.close()

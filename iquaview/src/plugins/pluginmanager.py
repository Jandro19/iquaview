"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
    Class to manage multiple plugins
"""
from importlib import util

from PyQt5.QtCore import QObject

from iquaview.src.plugins.USBL.usblmodule import USBLModule
from iquaview.src.plugins.imagenex_sidescan.imagenexsidescanmodule import ImagenexSidescanModule
from iquaview.src.plugins.imagenex_deltat_multibeam.imagenexdeltatmultibeammodule import ImagenexDeltatMultibeamModule


class PluginManager(QObject):

    def __init__(self, proj, canvas, config, vehicle_info, vehicle_data, mission_sts, boat_pose_action, menubar, view_menu_toolbar, parent=None):
        super(QObject, self).__init__(parent)

        self.plugins_list = list()
        self.plugin_manager = menubar.addMenu("Plugins")

        # USBL
        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.usbl_module = USBLModule(canvas, config, vehicle_info, mission_sts,
                                          boat_pose_action, self.plugin_manager, view_menu_toolbar)
            self.plugins_list.append(self.usbl_module)

        if util.find_spec('iquaview_imagenex_sidescan') is not None:
            self.imagenex_sidescan = ImagenexSidescanModule(proj, canvas, config, vehicle_info, vehicle_data, self.plugin_manager)
            self.plugins_list.append(self.imagenex_sidescan)

        if util.find_spec('iquaview_imagenex_deltat_multibeam') is not None:
            self.imagenex_deltat_multibeam = ImagenexDeltatMultibeamModule(proj, canvas, config, vehicle_info, vehicle_data, self.plugin_manager, parent)
            self.plugins_list.append(self.imagenex_deltat_multibeam)

    def about(self):
        """ Get information about plugins. """
        about_message = ""
        if self.plugins_list:
            about_message = "<p> Plugins versions: </p>"
            about_message += "<ul>"
            for plugin in self.plugins_list:
                about_message += "<li>" + plugin.version() + "</li>"
            about_message += "</ul>"

        return about_message

    def get_usbl(self):
        if util.find_spec('iquaview_evologics_usbl') is not None:
            return self.usbl_module
        else:
            return None

    def disconnect_plugins(self):
        if util.find_spec('iquaview_evologics_usbl') is not None:
            self.usbl_module.disconnect_usbl_dw()

        if util.find_spec('iquaview_imagenex_sidescan') is not None:
            self.imagenex_sidescan.disconnect_module()

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to trigger the service to calibrate the vehicle's magnetometer
"""

import logging
import subprocess
import yaml as yaml

from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from PyQt5.QtWidgets import QDialog, QMessageBox
from PyQt5.QtGui import QPixmap

from iquaview.src.cola2api.cola2_interface import send_trigger_service, set_ros_param
from iquaview.src.ui.ui_imu_calibration import Ui_CalibrationIMU

logger = logging.getLogger(__name__)

ACTIVE = 0
SUCCESS = 1
FAILURE = 2
STOPPED = 3


class CalibrateMagnetometer(QObject):
    calibrate_magnetometer_signal = pyqtSignal(bool, str)
    success_signal = pyqtSignal(str)
    timer_disconnected_signal = pyqtSignal()

    def __init__(self, vehicle_info, vehicle_data):
        super(CalibrateMagnetometer, self).__init__()

        self.ip = vehicle_info.get_vehicle_ip()
        self.port = 9091
        self.vehicle_namespace = vehicle_info.get_vehicle_namespace()
        self.vehicle_data = vehicle_data
        self.vehicle_info = vehicle_info

        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh_feedback)

        self.success_signal.connect(self.on_success_calibration)

    def start_calibrate_magnetometer(self):
        """ Start calibrate magnetometer in a new thread"""

        #     self.threadCalibrateMagnetometer = threading.Thread(target=self.calibrate_magnetometer)
        #     self.threadCalibrateMagnetometer.daemon = True
        #     self.threadCalibrateMagnetometer.start()
        #
        # def calibrate_magnetometer(self):
        """ Send service to start calibrate magnetometer"""
        try:
            enable_thrusters = self.vehicle_data.get_enable_thrusters_service()
            calibrate_magnetometer = self.vehicle_data.get_calibrate_magnetometer_service()
            thruster = send_trigger_service(self.ip, self.port,
                                            self.vehicle_namespace + enable_thrusters)
            response = send_trigger_service(self.ip, self.port,
                                            self.vehicle_namespace + calibrate_magnetometer)

            self.calibrate_magnetometer_signal.emit(response['values']['success'], response['values']['message'])

        except:
            self.calibrate_magnetometer_signal.emit(False, "")

    def start_updating_data(self):
        self.timer.start(1000)

    def refresh_feedback(self):
        """ update state of state feedback"""

        # if self.vehicle_data.is_subscribed_to_topic('state feedback'):
        #     self.vehicle_data.unsubscribe_topic('state feedback')

        if not self.vehicle_data.is_subscribed_to_topic('state feedback'):
            self.vehicle_data.subscribe_topic('state feedback')

        state_feedback = self.vehicle_data.get_captain_state_feedback()

        if state_feedback is not None and state_feedback['valid_data'] == 'new_data':
            # file_name = state_feedback['kevalues']['file_name']
            state = state_feedback['state']
            # if state == ACTIVE:
            if state == SUCCESS:
                if state_feedback['keyvalues'][0]['key'] == 'file_name':
                    file_name = state_feedback['keyvalues'][0]['value']
                self.success_signal.emit(file_name)
                self.disconnect_timer()
                logger.info("SUCCESS")

            if state == FAILURE:
                self.disconnect_timer()
                logger.info("FAILURE")

            if state == STOPPED:
                self.disconnect_timer()
                logger.info("STOPPED")

    def on_success_calibration(self, filename):
        vehicle_package = self.vehicle_info.get_remote_vehicle_package()
        remotepath = vehicle_package + "/calibrations"
        remotefile = remotepath + "/" + filename
        temp = "/tmp"

        png_filename = filename.replace('.yaml', '.png')
        remote_png_filename = remotepath + "/" + png_filename
        temp_png_filename = temp + "/" + png_filename
        temp_yaml_filename = temp + "/" + filename

        subprocess.Popen(["scp",
                          "{}@{}:{}".format(self.vehicle_info.get_vehicle_user(),
                                            self.vehicle_info.get_vehicle_ip(),
                                            remote_png_filename),
                          "{}".format(temp)]).wait()

        calibration_imu_dialog = CalibrationIMUDialog(temp_png_filename)
        result = calibration_imu_dialog.exec_()
        if result == QDialog.Accepted:
            reply = QMessageBox.question(None,
                                         'Calibration Confirmation',
                                         "Do you want to use this calibration?",
                                         QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                # copiar fitxer a last_calibration.yaml
                remote_destination = remotepath + "/imu_last_calibration.yaml"
                subprocess.Popen(["scp",
                                  "{}@{}:{}".format(self.vehicle_info.get_vehicle_user(),
                                                    self.vehicle_info.get_vehicle_ip(),
                                                    remotefile),
                                  "{}@{}:{}".format(self.vehicle_info.get_vehicle_user(),
                                                    self.vehicle_info.get_vehicle_ip(),
                                                    remote_destination)]).wait()

                subprocess.Popen(["scp",
                                  "{}@{}:{}".format(self.vehicle_info.get_vehicle_user(),
                                                    self.vehicle_info.get_vehicle_ip(),
                                                    remotefile),
                                  "{}".format(temp)]).wait()

                with open(temp_yaml_filename, 'r') as f:
                    last_calibration_file = yaml.safe_load(f)
                    set_ros_param(self.ip, self.port,
                                  self.vehicle_namespace + "/imu_angle_estimator/hard_iron",
                                  str(last_calibration_file['hard_iron']))

                    set_ros_param(self.ip, self.port,
                                  self.vehicle_namespace + "/imu_angle_estimator/soft_iron",
                                  str(last_calibration_file['soft_iron']))

                response = send_trigger_service(self.ip,
                                                self.port,
                                                self.vehicle_namespace + "/imu_angle_estimator/reload_params")
                if not response['values']['success']:
                    QMessageBox.critical(self,
                                         "Reload params failed",
                                         response['values']['message'],
                                         QMessageBox.Close)

    def stop_magnetometer_calibration(self):
        """ Stop magnetometer calibration"""
        stop_magnetometer_calibration_service = self.vehicle_data.get_stop_magnetometer_calibration_service()
        response = send_trigger_service(self.ip, self.port,
                                        self.vehicle_namespace + stop_magnetometer_calibration_service)
        if not response['values']['success']:
            QMessageBox.critical(self,
                                 "Stop magnetometer calibration failed",
                                 response['values']['message'],
                                 QMessageBox.Close)

    def disconnect_timer(self):
        """Disconnect timer and unsubscribe state feedback"""
        self.timer.stop()
        if self.vehicle_data.is_subscribed_to_topic("state feedback"):
            self.vehicle_data.unsubscribe_topic('state feedback')
        self.timer_disconnected_signal.emit()


class CalibrationIMUDialog(QDialog, Ui_CalibrationIMU):

    def __init__(self, filename, parent=None):
        super(CalibrationIMUDialog, self).__init__(parent)
        self.setupUi(self)
        self.filename = filename

        pixmap = QPixmap(filename)
        self.image_label.setPixmap(pixmap)

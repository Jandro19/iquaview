"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
Sets the widget containing all the graphical vehicle widgets
including battery, compass, roll/pitch, depth/altitude, velocimeter,
and table of current and desired pose requests.
"""

import matplotlib

from PyQt5 import QtCore
from PyQt5.QtWidgets import QWidget, QMenu, QAction, QApplication, QRubberBand
from PyQt5.QtCore import QTimer, pyqtSignal, Qt, QMimeData, QPoint, QMargins
from PyQt5.QtGui import QDrag, QPixmap, QGuiApplication, QPainter

from iquaview.src.vehicle.vehiclewidgets import (compasswidget,
                                                 rollpitchwidget,
                                                 tablewidget,
                                                 resourceswidget,
                                                 setpointswidget)
from iquaview.src.vehicle.vehiclewidgets import depthaltitudewidget, velocimeterwidget

from iquaview.src.ui.ui_vehicle_widgets import Ui_VehicleWidgets

matplotlib.use("Qt5Agg")


class VehicleWidgets(QWidget, Ui_VehicleWidgets):
    """

    This class contains all subgraphicwidgets

    """

    refresh_signal = pyqtSignal()

    def __init__(self, vehicle_info, vehicledata, parent=None):
        """
        Init of the object VehicleWidgets
        :param vehicle_info: information about the AUV
        :type vehicle_info: VehicleInfo
        :param vehicledata: data received from the AUV
        :type vehicledata: VehicleData
        """
        super(VehicleWidgets, self).__init__(parent)
        self.setupUi(self)
        self.timer = QTimer()
        self.timer.timeout.connect(self.start_timer)

        self.table = None
        self.dragging = None
        self.rb = None
        self.area_mid_point = None
        self.start_drag_position = None
        self.setAcceptDrops(True)

        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.vehicle_info = vehicle_info
        self.vehicle_data = vehicledata
        self.layout = self.scrollAreaWidgetContents.layout()
        self.refresh_signal.connect(self.refresh)
        self.setMinimumHeight(250)

        # create QAction's to change Vehicle widgets state
        self.resources_action = QAction("Resources", self)
        self.resources_action.setCheckable(True)
        self.resources_action.setChecked(True)
        self.resources_action.setEnabled(False)
        self.resources_action.triggered.connect(self.resources_state_changed)

        self.compass_action = QAction("Compass", self)
        self.compass_action.setCheckable(True)
        self.compass_action.setChecked(True)
        self.compass_action.setEnabled(False)
        self.compass_action.triggered.connect(self.compass_state_changed)

        self.depth_altitude_action = QAction("Depth/Altitude", self)
        self.depth_altitude_action.setCheckable(True)
        self.depth_altitude_action.setChecked(True)
        self.depth_altitude_action.setEnabled(False)
        self.depth_altitude_action.triggered.connect(self.depthaltitude_state_changed)

        self.roll_pitch_action = QAction("Roll/Pitch", self)
        self.roll_pitch_action.setCheckable(True)
        self.roll_pitch_action.setChecked(True)
        self.roll_pitch_action.setEnabled(False)
        self.roll_pitch_action.triggered.connect(self.rollpitch_state_changed)

        self.velocimeter_action = QAction("Velocimeter", self)
        self.velocimeter_action.setCheckable(True)
        self.velocimeter_action.setChecked(True)
        self.velocimeter_action.setEnabled(False)
        self.velocimeter_action.triggered.connect(self.velocimeter_state_changed)

        self.table_action = QAction("Table", self)
        self.table_action.setCheckable(True)
        self.table_action.setChecked(True)
        self.table_action.setEnabled(False)
        self.table_action.triggered.connect(self.table_state_changed)

        self.setpoints_action = QAction("Setpoints", self)
        self.setpoints_action.setCheckable(True)
        self.setpoints_action.setChecked(True)
        self.setpoints_action.setEnabled(False)
        self.setpoints_action.triggered.connect(self.setpoints_state_changed)

        # Create menu to group all the actions together
        self.menu_widgets = QMenu("Vehicle Widgets")
        self.menu_widgets.addActions([self.resources_action,
                                      self.compass_action,
                                      self.roll_pitch_action,
                                      self.velocimeter_action,
                                      self.depth_altitude_action,
                                      self.table_action,
                                      self.setpoints_action])

    def get_menu_actions(self):
        """
        Returns a list with all the actions of the menu
        :return: list with all the actions of the menu
        :rtype: list
        """
        return self.menu_widgets.actions()

    def vehicle_widgets_dock_visibility_changed(self, visible):
        """ Change vehicle widgets dock visibility."""
        if not visible:
            self.resources_action.setEnabled(False)
            self.compass_action.setEnabled(False)
            self.depth_altitude_action.setEnabled(False)
            self.roll_pitch_action.setEnabled(False)
            self.velocimeter_action.setEnabled(False)
            self.table_action.setEnabled(False)
            self.setpoints_action.setEnabled(False)
        else:
            self.resources_action.setEnabled(True)
            self.compass_action.setEnabled(True)
            self.depth_altitude_action.setEnabled(True)
            self.roll_pitch_action.setEnabled(True)
            self.velocimeter_action.setEnabled(True)
            self.table_action.setEnabled(True)
            self.setpoints_action.setEnabled(True)

    def compass_state_changed(self):
        """ Toggles visibility of the compass widget """
        sender = self.sender()
        if sender.isChecked():
            self.compass.show()
        else:
            self.compass.hide()

    def depthaltitude_state_changed(self):
        """ Toggles visibility of the depth/altitude widget """
        sender = self.sender()
        if sender.isChecked():
            self.depthaltitudecanvas.show()
        else:
            self.depthaltitudecanvas.hide()

    def rollpitch_state_changed(self):
        """ Toggles visibility of the roll/pitch widget """
        sender = self.sender()
        if sender.isChecked():
            self.rollpitch.show()
        else:
            self.rollpitch.hide()

    def velocimeter_state_changed(self):
        """ Toggles visibility of the velocimeter widget """
        sender = self.sender()
        if sender.isChecked():
            self.velocimeter.show()
        else:
            self.velocimeter.hide()

    def table_state_changed(self):
        """ Toggles visibility of the table widget """
        sender = self.sender()
        if sender.isChecked():
            self.table.show()
        else:
            self.table.hide()

    def resources_state_changed(self):
        """ Toggles visibility of the resources widget """
        sender = self.sender()
        if sender.isChecked():
            self.resourcesusage.show()
        else:
            self.resourcesusage.hide()

    def setpoints_state_changed(self):
        """ Toggles visibility of the setpoints widget """
        sender = self.sender()
        if sender.isChecked():
            self.setpoints.show()
        else:
            self.setpoints.hide()

    def connect(self):
        """ Creates the widgets and initializes data """
        self.resourcesusage = resourceswidget.ResourcesWidget()
        self.depthaltitudecanvas = depthaltitudewidget.DepthAltitudeCanvas(self, f_width=3, f_height=2, dpi=100)
        self.compass = compasswidget.CompassWidget()
        self.rollpitch = rollpitchwidget.RollPitchWidget()
        self.velocimeter = velocimeterwidget.VelocimeterWidget()
        self.table = tablewidget.TablePoseWidget(self.vehicle_info, self.vehicle_data)
        self.setpoints = setpointswidget.SetpointsWidget()

        self.depthaltitudecanvas.setAttribute(Qt.WA_TransparentForMouseEvents)
        # self.table.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.table.start_drag_drop_signal.connect(self.start_drag_and_drop)

        self.layout.addWidget(self.resourcesusage)
        self.layout.addWidget(self.compass)
        self.layout.addWidget(self.rollpitch)
        self.layout.addWidget(self.velocimeter)
        self.layout.addWidget(self.depthaltitudecanvas)
        self.layout.addWidget(self.table)
        self.layout.addWidget(self.setpoints)
        self.scrollAreaWidgetContents.setFocus()

        if not self.resources_action.isChecked():
            self.resourcesusage.hide()
        if not self.compass_action.isChecked():
            self.compass.hide()
        if not self.roll_pitch_action.isChecked():
            self.rollpitch.hide()
        if not self.velocimeter_action.isChecked():
            self.velocimeter.hide()
        if not self.depth_altitude_action.isChecked():
            self.depthaltitudecanvas.hide()
        if not self.table_action.isChecked():
            self.table.hide()
        if not self.setpoints_action.isChecked():
            self.setpoints.hide()

        self.connected = True
        self.start_updating_data()

    def start_updating_data(self):
        """ Starts updating data of the table widget"""
        self.table.connect()
        self.timer.start(1000)

    def start_timer(self):
        """ emits refresh_signal if connected """
        if self.connected:
            self.refresh_signal.emit()

    def refresh(self):
        """ Updates all widgets with the info provided by vehicle_data """

        data = self.vehicle_data.get_nav_sts()
        if data is not None and data['valid_data'] == 'new_data':
            self.depthaltitudecanvas.set_data(data)
            self.compass.set_data(data)
            self.rollpitch.set_data(data)
            self.velocimeter.set_data(data)

        battery_charge = self.vehicle_data.get_battery_charge()
        cpu_usage = self.vehicle_data.get_cpu_usage()
        ram_usage = self.vehicle_data.get_ram_usage()
        self.resourcesusage.set_data(battery_charge, cpu_usage, ram_usage)

        thrusters_enabled = self.vehicle_data.get_thrusters_status()
        thruster_setpoints = self.vehicle_data.get_thruster_setpoints()

        if (not thrusters_enabled) and self.vehicle_data.is_subscribed_to_topic('thruster setpoints'):
            self.vehicle_data.unsubscribe_topic('thruster setpoints')
            self.setpoints.clear_setpoints()

        if thrusters_enabled and (not self.vehicle_data.is_subscribed_to_topic('thruster setpoints')):
            self.vehicle_data.subscribe_topic('thruster setpoints')

        if thruster_setpoints is not None and thruster_setpoints['valid_data'] == 'new_data':
            self.setpoints.set_data(thruster_setpoints)

    def mousePressEvent(self, event):
        """ Override mousePressEvent. Handles starting drag and drop and creating context menus """
        if event.button() == Qt.LeftButton:
            self.start_drag_position = event.pos()

        elif event.button() == Qt.RightButton:
            self.menu_widgets.popup(self.mapToGlobal(event.pos()))

    def mouseMoveEvent(self, event):
        if event.buttons() and Qt.LeftButton:
            if self.start_drag_position is not None and \
                    (event.pos() - self.start_drag_position).manhattanLength() < QApplication.startDragDistance():
                pos_global = self.mapToGlobal(event.pos())
                self.start_drag_and_drop(pos_global)

    def dragEnterEvent(self, event):
        """ Override dragEnterEvent. Accepts the event drag and drop action """
        event.acceptProposedAction()

    def dragLeaveEvent(self, event):
        """ Override dragLeaveEvent. Hides the rubber band """
        if self.rb is not None:
            self.rb.hide()

    def dragMoveEvent(self, event):
        """ Override dragMoveEvent. Calls in_between """
        if self.dragging is not None:
            self.in_between(event.pos())

    def dropEvent(self, event):
        """ Override dropEvent. Ends drag and drop event """
        event.acceptProposedAction()
        if self.dragging is not None:
            self.end_drag_and_drop(event.pos())
            self.dragging = None

    def start_drag_and_drop(self, pos):
        """
        If a widget is clicked, starts drag and drop for that widget.
        :param pos: Point of the drag and drop event in global position
        :type pos: QPoint
        """

        pos_container = self.scrollAreaWidgetContents.mapFromGlobal(pos)
        pos_vehiclewidgets = self.mapFromGlobal(pos)

        self.rb = QRubberBand(QRubberBand.Line)
        self.rb.resize(1, 150)
        self.area_mid_point = self.scrollArea_widgets.mapToGlobal(
            QPoint(self.scrollArea_widgets.width() / 2, self.scrollArea_widgets.height() / 2))

        if self.resourcesusage.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.resourcesusage)

        elif self.compass.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.compass)

        elif self.rollpitch.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.rollpitch)

        elif self.velocimeter.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.velocimeter)

        elif self.depthaltitudecanvas.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.depthaltitudecanvas)

        elif self.table.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.table)

        elif self.setpoints.geometry().contains(pos_container):
            self.start_drag_event(pos_vehiclewidgets, self.setpoints)

    def start_drag_event(self, pos, widget):
        """
        Creates a miniature image of the widget to indicate dragging action.
        :param pos: Point of the drag and drop event
        :type pos: QPoint
        :param widget: Widget being dragged
        :type: QWidget
        :return:
        """
        self.dragging = widget

        drag = QDrag(self)
        mdata = QMimeData()

        # todo: change to 'screen = QGuiApplication.screenAt(pos_global)' for Qt 5.10 and above
        screen_list = QGuiApplication.screens()
        screen_index = QApplication.desktop().screenNumber(widget)
        screen = screen_list[screen_index]

        mainwindow = self.parentWidget().parentWidget()
        top_left_corner = self.scrollAreaWidgetContents.mapToGlobal(widget.geometry().topLeft())
        tlc_from_mw = mainwindow.mapFromGlobal(top_left_corner)

        # grabWindow must be called for mainwindow to avoid visual bugs (ubuntu 18.04)
        img = screen.grabWindow(mainwindow.winId(), x=tlc_from_mw.x(), y=tlc_from_mw.y(),
                                height=(widget.height() - 5), width=widget.width())
        pixmap = QPixmap(img.size())
        pixmap.fill(Qt.transparent)
        painter = QPainter(pixmap)
        painter.setOpacity(0.7)
        painter.drawPixmap(0, 0, img)
        painter.end()

        drag.setMimeData(mdata)
        drag.setPixmap(pixmap)

        pos_global = self.mapToGlobal(pos)
        hotspot_x = pos_global.x() - top_left_corner.x()
        hotspot_y = pos_global.y() - top_left_corner.y()
        drag.setHotSpot(QPoint(hotspot_x, hotspot_y))

        drag.exec_()

    def in_between(self, pos):
        """
        Checks if the position is in between widgets or at the far left and far right side. If it is, shows the
        rubber band in that position
        :param pos: position to check
        :type pos: QPoint
        """
        pos_global = self.mapToGlobal(pos)
        pos_container = self.scrollAreaWidgetContents.mapFromGlobal(pos_global)

        w_list = list()
        for i in range(0, self.layout.count()):
            if not self.layout.itemAt(i).widget().isHidden():
                w_list.append(self.layout.itemAt(i).widget())

        found = False

        # check if pos is to the left of first widget
        right = w_list[0].geometry().bottomLeft()
        if pos_container.x() < right.x() + 20:
            point = self.scrollAreaWidgetContents.mapToGlobal(right)
            self.rb.move(point.x(), self.area_mid_point.y() - (self.rb.height() / 2))
            self.rb.show()
            found = True

        if not found:
            for i in range(0, len(w_list)-1):
                # check if pos is in between i and i+1

                left = w_list[i].geometry().bottomRight().x()
                right = w_list[i+1].geometry().bottomLeft().x()
                if left-20 < pos_container.x() < right+20:
                    mid_point_x = self.scrollAreaWidgetContents.mapToGlobal(w_list[i].geometry().bottomRight()).x()
                    self.rb.move(mid_point_x, self.area_mid_point.y() - (self.rb.height() / 2))
                    self.rb.show()
                    found = True
                    break

        # check if pos is to the right of last widget
        if not found:
            left = w_list[len(w_list)-1].geometry().bottomRight()
            if left.x() -20 < pos_container.x():
                point = self.scrollAreaWidgetContents.mapToGlobal(left)
                self.rb.move(point.x(), self.area_mid_point.y() - (self.rb.height() / 2))
                self.rb.show()
                found = True

        if not found:
            self.rb.hide()

    def end_drag_and_drop(self, pos):
        """
        If pos is over the far left and right side of a widget, insert dragging widget into that position
        :param pos: position to check
        :type pos: QPoint
        """
        pos_global = self.mapToGlobal(pos)
        pos_container = self.scrollAreaWidgetContents.mapFromGlobal(pos_global)
        # Find the widget where the dragged widget is dropped
        released_over = None
        if self.resourcesusage.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.resourcesusage
        elif self.compass.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.compass
        elif self.rollpitch.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.rollpitch
        elif self.velocimeter.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.velocimeter
        elif self.depthaltitudecanvas.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.depthaltitudecanvas
        elif self.table.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.table
        elif self.setpoints.geometry().marginsAdded(QMargins(7, 0, 7, 0)).contains(pos_container):
            released_over = self.setpoints

        if released_over is not None:
            # Find on which side of the widget is the dragged widget dropped
            if self.dragging != released_over:
                left = released_over.geometry().bottomLeft().x()
                right = released_over.geometry().bottomRight().x()
                if abs(pos_container.x() - left) < 25:
                    self.layout.takeAt(self.layout.indexOf(self.dragging))
                    self.layout.insertWidget(self.layout.indexOf(released_over), self.dragging)
                elif abs(pos_container.x() - right) < 25:
                    self.layout.takeAt(self.layout.indexOf(self.dragging))
                    self.layout.insertWidget(self.layout.indexOf(released_over)+1, self.dragging)
        self.rb.hide()

    def disconnect(self):
        # clear previous widgets
        for i in reversed(range(self.layout.count())):
            self.layout.itemAt(i).widget().setParent(None)
        # disconnect
        self.connected = False
        # close thread and timer
        self.timer.stop()
        if self.table:
            self.table.disconnect()

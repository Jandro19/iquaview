"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Dialog to configure AUV parameters, grouped in different sections.
 The dialog is dynamically created according to the items defined under the tag ros_params
 in the auv configuration xml file.
"""

import time
import logging
from copy import deepcopy

from PyQt5.QtWidgets import (QDialog,
                             QLabel,
                             QComboBox,
                             QLineEdit,
                             QMessageBox,
                             QWidget,
                             QSpinBox,
                             QDoubleSpinBox,
                             QApplication)

from PyQt5.QtGui import QValidator, QRegExpValidator, QColor, QGuiApplication
from PyQt5.QtCore import QRegExp, QEvent, pyqtSignal, Qt

from iquaview.src.ui.ui_auvconfigparams import Ui_AUVConfigParamsDlg
from iquaview.src.xmlconfighandler.rosparamsreader import RosParamsReader
from iquaview.src.cola2api import cola2_interface
from iquaview.src.utils.textvalidator import get_custom_double_validator, get_color, get_int_validator, get_ip_validator

logger = logging.getLogger(__name__)


class AUVConfigParamsDlg(QDialog, Ui_AUVConfigParamsDlg):
    applied_changes = pyqtSignal()

    def __init__(self, config_filename, vehicle_info, unique_section=False, parent=None):
        super(AUVConfigParamsDlg, self).__init__(parent)
        self.setupUi(self)
        self.config_filename = config_filename
        self.vehicle_info = vehicle_info
        self.rosparamsreader = None
        self.sections = None
        self.current_section = None
        self.old_values = None
        self.changes = False
        self.unique_section = unique_section
        self.current_index = 0
        self.ip = self.vehicle_info.get_vehicle_ip()
        self.port = 9091
        self.vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        # install event filter to catch wheel event
        self.section_comboBox.installEventFilter(self)

        self.base_height = self.layout().spacing() * 2 + self.layout().contentsMargins().bottom() * 2 + \
                           self.section_comboBox.height() + self.closeButton.height() + 5

        self.closeButton.clicked.connect(self.close)
        self.applyButton.clicked.connect(self.apply_params)
        self.saveDefaultButton.clicked.connect(self.save_params)
        self.section_comboBox.currentIndexChanged.connect(self.update_params_form)

    def load_sections(self):
        """
        Initialize RosParamsReader and load multiple sections
        """
        self.changes = False

        self.rosparamsreader = RosParamsReader(self.config_filename,
                                               self.ip,
                                               9091,
                                               self.vehicle_namespace)

        self.sections = self.rosparamsreader.read_configuration()
        self.section_comboBox.clear()
        for section in self.sections:
            self.section_comboBox.addItem(section.get_description())

    def load_section(self, section_name):
        """
        Initialize RosParamsReader and load section with section_name
        """

        self.changes = False

        self.rosparamsreader = RosParamsReader(self.config_filename,
                                               self.ip,
                                               9091,
                                               self.vehicle_namespace)

        self.hide_section_layout()

        self.sections = self.rosparamsreader.read_configuration()
        self.section_comboBox.clear()
        for section in self.sections:
            if section.get_description() == section_name:
                self.section_comboBox.addItem(section.get_description())

    def hide_section_layout(self):
        """ hide section layout"""
        if self.section_horizontalLayout is not None:
            for i in reversed(range(self.section_horizontalLayout.count())):
                item = self.section_horizontalLayout.takeAt(i)
                widget = item.widget()
                if widget is not None:
                    widget.hide()

    def update_params_form(self):
        """
        Updates the params form
        """
        success = True
        index = self.section_comboBox.currentIndex()

        if self.changes:
            reply = QMessageBox.question(self, 'Apply changes confirmation',
                                         "Some changes have not been applied. Do you want to apply them?",
                                         QMessageBox.Yes, QMessageBox.No)
            if reply == QMessageBox.Yes:
                success = self.apply_params()

            else:
                self.load_sections()

        if success:
            self.current_index = index

            self.section_comboBox.setCurrentIndex(self.current_index)

            for section in self.sections:
                if self.section_comboBox.currentText() == section.get_description():
                    self.current_section = section
                    self.old_values = deepcopy(section)

            form_layout = self.scrollAreaWidgetContents.layout()
            # Clear current widgets of the layout
            for i in reversed(range(form_layout.count())):
                widget_to_remove = form_layout.takeAt(i).widget()
                # remove it from the layout list
                form_layout.removeWidget(widget_to_remove)
                # remove it from the iquaview
                widget_to_remove.setParent(None)

            # Put new widgets in the form
            n_param = 0
            for param in self.current_section.get_params():
                label_param = QLabel(self.scrollAreaWidgetContents)
                label_param.setText(param.get_description() + ":")
                if not param.is_array() and param.get_type() == "boolean":
                    self.add_boolean_combobox_widget(param, n_param, label_param, form_layout)

                elif not param.is_array() and param.has_range():
                    self.add_range_combobox_widget(param, n_param, label_param, form_layout)

                elif not param.is_array() and param.has_limits():
                    self.add_spinbox_widget(param, n_param, label_param, form_layout)

                else:
                    self.add_lineedit_widget(param, n_param, label_param, form_layout)
                n_param = n_param + 1

            self.changes = False

            self.adjustSize()

        else:
            self.changes = False
            self.section_comboBox.setCurrentIndex(self.current_index)
            self.changes = True

    def adjustSize(self):
        """
        Override method of the class QDialog. Adjusts size of dialog according to the contents of the scroll area
        so initially no scroll bar will be shown.
        """

        form_layout = self.scrollAreaWidgetContents.layout()
        left_column_width = 0
        right_column_width = 0

        # get all widgets from the layout
        for i in range(form_layout.count()):
            widget = form_layout.itemAt(i).widget()
            if i % 2 == 0:
                label_width = widget.fontMetrics().boundingRect(widget.text()).width()
                if label_width > left_column_width:
                    left_column_width = label_width
            else:
                if type(widget) == QComboBox:
                    field_width = widget.fontMetrics().width(widget.currentText())
                else:
                    field_width = widget.fontMetrics().width(widget.text())
                if field_width > right_column_width:
                    right_column_width = field_width

        width = left_column_width + right_column_width + 80  # Horizontal margins and spacings are about 80px

        widget_height = form_layout.itemAt(0).widget().size().height()  # All widgets have the same height
        height = (widget_height + form_layout.verticalSpacing()) * (form_layout.count() / 2) + self.base_height

        # Get current screen where the dialog is
        screen_list = QGuiApplication.screens()
        screen_index = QApplication.desktop().screenNumber(self.scrollAreaWidgetContents)  # This needs a Qwidget
        screen = screen_list[screen_index]
        screen_rect = screen.geometry()

        # If size is too big to fit in the screen, change size
        if height > screen_rect.height() - 55:  # Top margin of the parent dialog is approximately 55px
            height = screen_rect.height()
        if width > screen_rect.width():
            width = screen_rect.width()

        self.resize(width, height)

    def add_boolean_combobox_widget(self, param, n_param, label_param, form_layout):
        """
        set boolean combobox widget with values
        :param param: parameter
        :param param: Param
        :param n_param: position in form_layout
        :param n_param: int
        :param label_param: description of the param
        :param label_param: QLabel
        :param form_layout: form layout to insert widget
        :param form_layout: QFormLayout
        """
        value = param.get_value()
        combo_box = QComboBox(self.scrollAreaWidgetContents)
        combo_box.installEventFilter(self)
        combo_box.setObjectName(str(n_param))
        combo_box.currentIndexChanged.connect(self.on_box_changed)

        combo_box.addItem("true")
        combo_box.addItem("false")
        if value == "false":
            combo_box.setCurrentIndex(1)
        else:
            combo_box.setCurrentIndex(0)
        form_layout.addRow(label_param, combo_box)

    def add_range_combobox_widget(self, param, n_param, label_param, form_layout):
        """
        set range combobox widget with values
        :param param: parameter
        :param param: Param
        :param n_param: position in form_layout
        :param n_param: int
        :param label_param: description of the param
        :param label_param: QLabel
        :param form_layout: form layout to insert widget
        :param form_layout: QFormLayout
        """
        range_index = 0
        mismatch = True
        combo_box = QComboBox(self.scrollAreaWidgetContents)
        combo_box.installEventFilter(self)
        combo_box.setObjectName(str(n_param))

        for range_value in param.get_range():
            combo_box.addItem(str(range_value))
            try:
                if float(param.get_value()) == range_value:
                    range_index = param.get_range().index(range_value)
                    mismatch = False
            except ValueError:
                pass

        if mismatch:
            if "Value mismatch error" in str(param.get_value()):
                combo_box.addItem(str(param.get_value()))
            else:
                combo_box.addItem("* " + str(param.get_value()) + " - Value mismatch error")
            range_index = combo_box.count() - 1
            combo_box.setItemData(range_index, QColor('#f6989d'), Qt.BackgroundRole)

        combo_box.currentIndexChanged.connect(self.on_box_changed)
        combo_box.setCurrentIndex(range_index)
        form_layout.addRow(label_param, combo_box)

    def add_lineedit_widget(self, param, n_param, label_param, form_layout):
        """
        set lineedit widget with values
        :param param: parameter
        :param param: Param
        :param n_param: position in form_layout
        :param n_param: int
        :param label_param: description of the param
        :param label_param: QLabel
        :param form_layout: form layout to insert widget
        :param form_layout: QFormLayout
        """
        line_edit_param = QLineEdit(self.scrollAreaWidgetContents)
        line_edit_param.setObjectName(str(n_param))
        line_edit_param.textChanged.connect(self.on_text_changed)
        line_edit_param.setText(param.get_value())
        form_layout.addRow(label_param, line_edit_param)

    def add_spinbox_widget(self, param, n_param, label_param, form_layout):
        """
        set spinbox widget with values
        :param param: parameter
        :param param: Param
        :param n_param: position in form_layout
        :param n_param: int
        :param label_param: description of the param
        :param label_param: QLabel
        :param form_layout: form layout to insert widget
        :param form_layout: QFormLayout
        """
        if param.get_type() == "int":
            spin_box = QSpinBox(self.scrollAreaWidgetContents)
            if param.get_value() != "null":
                value = int(param.get_value())
        else:
            spin_box = QDoubleSpinBox(self.scrollAreaWidgetContents)
            if param.get_value() != "null":
                value = float(param.get_value())

        spin_box.installEventFilter(self)
        spin_box.setObjectName(str(n_param))
        spin_box.setRange(param.get_min_limit(), param.get_max_limit())
        spin_box.valueChanged.connect(self.on_spinbox_changed)
        if param.get_value() != "null":
            spin_box.setValue(value)
        else:
            spin_box.setValue(param.get_min_limit())

        form_layout.addRow(label_param, spin_box)

    def eventFilter(self, source, event):
        # event filter to ignore wheel on combobox
        if (event.type() == QEvent.Wheel
                and (isinstance(source, QComboBox)
                     or isinstance(source, QSpinBox)
                     or isinstance(source, QDoubleSpinBox))):
            event.ignore()
            return True
        else:
            return QWidget.eventFilter(self, source, event)

    def on_text_changed(self, string):
        """
        Check if 'string' is valid
        :param string: value of the field
        """
        sender = self.sender()
        params = self.current_section.get_params()
        params[int(sender.objectName())].set_value(string)
        # check lines correct

        state = self.check_param(params[int(sender.objectName())])
        color = get_color(state)
        sender.setStyleSheet('QLineEdit { background-color: %s }' % color)
        self.changes = True

    def on_box_changed(self, i):
        """
        set box value
        """
        sender = self.sender()
        params = self.current_section.get_params()
        params[int(sender.objectName())].set_value(sender.itemText(i))
        self.changes = True

    def on_spinbox_changed(self):
        """
        set spinbox value
        """
        sender = self.sender()
        params = self.current_section.get_params()
        params[int(sender.objectName())].set_value(str(sender.value()))
        self.changes = True

    def check_param(self, param):
        """
        Check if 'param' is valid
        :param param: value of the field
        """
        # int
        if not param.is_array() and param.get_type() == "int":
            validator = get_int_validator()
        # double
        if not param.is_array() and param.get_type() == "double":
            # double validator
            validator = get_custom_double_validator()

        # double array
        elif param.is_array() and param.get_type() == "double":
            string = ""
            # dinamic regular expresion
            for x in range(0, int(param.get_array_size())):
                if x == int(param.get_array_size()) - 1:
                    string += "-?[\\d\\.]*"
                else:
                    string += "-?[\\d\\.]*,\s*"
            regexp = QRegExp("\\[" + string + "\\]")
            validator = QRegExpValidator(regexp)

            # boolean array
        elif param.is_array() and param.get_type() == "boolean":
            string = ""
            # dinamic regular expresion
            for x in range(0, int(param.get_array_size())):
                if x == int(param.get_array_size()) - 1:
                    string += "(true|false)"
                else:
                    string += "(true|false),\s*"
            regexp = QRegExp("\\[" + string + "\\]")
            validator = QRegExpValidator(regexp)

        elif not param.is_array() and param.get_type() == "ip":
            validator = get_ip_validator()

        state = validator.validate(param.get_value(), 0)[0]
        return state

    def are_values_acceptable(self):
        """
        Check if the values are acceptable
        :return: return True if all the values are acceptable, otherwise False
        """
        is_acceptable = True
        for param in self.current_section.get_params():
            # only check lineedits
            if not (not param.is_array() and param.get_type() == "boolean"):
                # check param in lineEdit
                state = self.check_param(param)
                if state != QValidator.Acceptable:
                    is_acceptable = False
        return is_acceptable

    def apply_params(self):
        """ Apply params"""
        try:
            if self.are_values_acceptable():
                logger.info("Applying parameters")
                for param in self.current_section.get_params():
                    param_value = param.get_value()
                    if param.get_type() == 'ip':
                        param_value = "\"" + param_value + "\""
                    cola2_interface.set_ros_param(self.ip, self.port, self.vehicle_namespace + param.get_name(),
                                                  param_value)
                time.sleep(1.0)
                # action_id
                logger.info(
                    "sending action service:  %s " % (self.current_section.get_action_id()))
                response = cola2_interface.send_trigger_service(self.ip, self.port,
                                                                self.vehicle_namespace + self.current_section.get_action_id())
                logger.info("Result: %s" % response)
                if not response['values']['success']:
                    QMessageBox.critical(self,
                                         "Error applying params: " + self.current_section.get_action_id(),
                                         response['values']['message'],
                                         QMessageBox.Close)
                    self.changes = False
                    params = self.current_section.get_params()
                    # if apply service fail, set old params
                    for index, param in enumerate(self.old_values.get_params()):
                        param_value = param.get_value()
                        if param.get_type() == 'ip':
                            param_value = "\"" + param_value + "\""
                        cola2_interface.set_ros_param(self.ip, self.port, self.vehicle_namespace + param.get_name(),
                                                      param_value)
                        params[index].set_value(param.get_value())

                    self.update_params_form()
                self.changes = False
                self.applied_changes.emit()
                return True

            else:
                logger.warning("It has not been possible to apply changes")
                raise Exception("It has not been possible to apply changes. \
                                 Please review the values of the parameters.")

        except ConnectionRefusedError:
            logger.error("Applying parameters failed")
            QMessageBox.critical(self,
                                 "Applying parameters failed",
                                 "Connection Refused",
                                 QMessageBox.Close)
        except Exception as e:
            logger.error("Applying parameters failed")
            QMessageBox.critical(self,
                                 "Applying parameters failed",
                                 e.args[0],
                                 QMessageBox.Close)

    def save_params(self):
        """ Save Params"""

        if self.unique_section:
            try:
                # apply params
                self.apply_params()

                logger.info("Saving parameters as defaults...")
                for param in self.current_section.get_params():
                    string = "/default_param_handler/update_param_in_yaml"
                    cola2_interface.send_string_service(self.ip, self.port,
                                                        self.vehicle_namespace + string,
                                                        self.vehicle_namespace + param.get_name())
            except ConnectionRefusedError:
                logger.error("Saving parameters failed")
                QMessageBox.critical(self,
                                     "Saving parameters failed",
                                     "Connection Refused",
                                     QMessageBox.Close)

        else:
            save_msg = "Are you sure you want to save all the parameters as defaults?"
            reply = QMessageBox.question(self, 'Saving Confirmation',
                                         save_msg, QMessageBox.Yes, QMessageBox.No)

            if reply == QMessageBox.Yes:
                try:
                    # apply params
                    self.apply_params()
                    logger.info("Saving parameters as defaults...")

                    string = "/default_param_handler/update_params_in_yamls"
                    response = cola2_interface.send_trigger_service(self.ip, self.port,
                                                                    self.vehicle_namespace + string)
                    if not response['values']['success']:
                        QMessageBox.critical(self,
                                             "Error saving parameters",
                                             response['values']['message'],
                                             QMessageBox.Close)

                except ConnectionRefusedError:
                    logger.error("Saving parameters failed")
                    QMessageBox.critical(self,
                                         "Saving parameters failed",
                                         "Connection Refused",
                                         QMessageBox.Close)
            else:
                logger.info("Parameters not saved")

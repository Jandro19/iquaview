"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

"""
 Class to subscribe to topics
"""
import logging
from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from PyQt5.QtWidgets import QMessageBox
from iquaview.src.cola2api.cola2_interface import SubscribeToTopic
from iquaview.src.xmlconfighandler.vehicledatahandler import VehicleDataHandler

logger = logging.getLogger(__name__)


class VehicleData(QObject):
    state_signal = pyqtSignal()

    def __init__(self, config, vehicle_info):
        """
        Constructor
        :param config: configuration
        :param vehicle_info: information about the AUV
        """
        super(VehicleData, self).__init__()

        self.auv_config_xml = config.csettings['configs_path'] + '/' + config.csettings['last_auv_config_xml']

        self.vehicle_info = vehicle_info

        self.subscribed = False
        self.state = None

        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh_data)

        self.topics = dict()
        self.current_data = dict()

        self.topic_names = self.read_xml_topics()
        #read services from xml
        self.services = self.read_xml_services()
        #read lauch list
        self.controlstation_launchlist = self.read_xml_controlstation_launch_list()
        self.vehicle_launchlist = self.read_xml_vehicle_launch_list()

    def read_xml_topics(self):
        """
        Read a vehicle data topics from a XML
        :return: return vehicle data topics in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get vehicle data topics
        xml_vehicle_data_topics = vd_handler.read_topics()

        topic_names = dict()

        for topic in xml_vehicle_data_topics:
            topic_names[topic.get('id')] = topic.text

        topic_names['rosout'] = '/rosout_agg'

        return topic_names

    def read_xml_services(self):
        """
        Read a vehicle data services from a XML
        :return: return vehicle data services in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get vehicle data topics
        xml_vehicle_data_services = vd_handler.read_services()

        services_names = dict()

        for service in xml_vehicle_data_services:
            services_names[service.get('id')] = service.text

        logger.debug("Services loaded: ", services_names)

        return services_names

    def read_xml_controlstation_launch_list(self):
        """
        Read a launch list from a XML
        :return: return launchs in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get launch list
        xml_launch_list = vd_handler.read_controlstation_launch_list()
        if xml_launch_list is None:
            launch_list = None
        else:
            launch_list = dict()

            for launch in xml_launch_list:
                launch_list[launch.get('id')] = launch.text

            logger.debug("Launch commands loaded: ", launch_list)

        return launch_list

    def read_xml_vehicle_launch_list(self):
        """
        Read a launch list from a XML
        :return: return launchs in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get launch list
        xml_launch_list = vd_handler.read_vehicle_launch_list()
        if xml_launch_list is None:
            launch_list = None
        else:
            launch_list = dict()

            for launch in xml_launch_list:
                launch_list[launch.get('id')] = launch.text

            logger.debug("Launch commands loaded: ", launch_list)

        return launch_list

    def subscribe_topics(self):

        """
        Subscribe topics
        """

        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        try:
            for key, value in self.topic_names.items():
                if key == 'rosout':
                    self.topics[key] = SubscribeToTopic(ip, port, value, 30, True)
                    self.topics[key].subscribe()
                elif key == 'thruster setpoints' or key == 'state feedback':
                    self.topics[key] = None
                elif 'usage' in key:
                    self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace+value, 10)
                    self.topics[key].subscribe()
                else:
                    self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace+value)
                    self.topics[key].subscribe()


            self.subscribed = True

            self.timer.start(1000)

        except:
            logger.error("Connection with COLA2 could not be established")
            QMessageBox.critical(self.parent(),
                                 "Connection with AUV Failed",
                                 "Connection with COLA2 could not be established",
                                 QMessageBox.Close)
            self.state_signal.emit()
            self.subscribed = False

    def unsubscribe_topic(self, key):
        """
        Unsubscribe topic with key key
        :param key: the key of the topic in the xml
        :type key: str
        """
        if self.topics.get(key) is not None:
            self.topics[key].close()

    def subscribe_topic(self, key):
        """
        Subscribe topic with key 'key'
        :param key: the key of the topic in the xml
        :type key: str
        """
        ip = self.vehicle_info.get_vehicle_ip()
        port = 9091
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        value = self.topic_names[key]
        if self.topics.get(key) is not None:
            self.topics[key].close()
        self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace+value)
        self.topics[key].subscribe()

    def is_subscribed_to_topic(self, key):
        """
        Get True if is subscribed to topic with key 'key', otherwise False
        :param key: key of the topic
        :type key: str
        :return: Return True if is subscribed to topic with key 'key', otherwise False
        :rtype: bool
        """
        if self.topics.get(key) is not None and self.topics[key].get_keepgoing():
            return True
        else:
            return False

    def refresh_data(self):
        """
        Start timer and refresh data
        """
        if self.subscribed:
            for key, value in self.topics.items():
                if key == 'rosout':
                    buffer = value.get_buffer()
                    self.set_buffer(key, buffer)
                    value.clear_buffer()

                else:
                    if value is not None:
                        data = value.get_data()
                        self.set_data(key, data)

    def get_vehicle_status(self):
        """
        Get vehicle status.
        :return: return vehicle status in a dict
        :rtype: dict
        """
        return self.current_data['vehicle status']

    def get_safety_supervisor_status(self):
        """
        Get safety supervisor status.
        :return: return safety supervisor status in a dict
        :rtype: dict
        """
        return self.current_data['safety supervisor status']

    def get_nav_sts(self):
        """
        Get navigation status.
        :return: return navigation status in a dict
        :rtype: dict
        """
        return self.current_data.get('navigation status')

    def get_desired_pose(self):
        """
        Get merged world waypoint req. (Desires pose)
        :return: return merged world waypoint req in a dict
        :rtype: dict
        """
        return self.current_data['merged world waypoint req']

    def get_desired_twist(self):
        """
        Get merged body velocity req. (Desired twist)
        :return: return merged body velocity req
        :rtype: dict
        """
        return self.current_data['merged body velocity req']

    def get_total_time(self):
        """
        Get total time.
        :return: return total time
        :rtype: dict
        """
        if self.current_data.get('total time') is None:
            return None

        return self.current_data['total time']

    def get_watchdog(self):
        """
        Get watchdog.
        :return: return watchdog
        :rtype: dict
        """
        if self.current_data.get('watchdog') is None:
            return None

        return self.current_data['watchdog']

    def get_goto_status(self):
        """
         Get goto status
        :return: return goto status
        :rtype: dict
        """
        if self.current_data is None:
            return None
        if self.current_data.get('goto status') is None:
            return None

        return self.current_data['goto status']

    def get_thrusters_status(self):
        """
         Get thrusters status
        :return: return thrusters status
        :rtype: bool
        """
        if self.current_data is None:
            return None
        if self.current_data.get('vehicle status') is None:
            return None
        if self.current_data.get('vehicle status').get('thrusters_enabled') is None:
            return None
        return self.current_data['vehicle status']['thrusters_enabled']

    def get_active_controller(self):
        """
        Get active controller
        :return: returns active controller
        :rtype: int
        """
        if self.current_data is None:
            return None
        if self.current_data.get('vehicle status') is None:
            return None
        if self.current_data.get('vehicle status').get('active_controller') is None:
            return None
        return self.current_data['vehicle status']['active_controller']

    def get_battery_charge(self):
        """
         Get battery charge
        :return: return battery charge
        :rtype: float
        """
        if self.current_data is None:
            return None
        if self.current_data.get('vehicle status') is None:
            return None
        if self.current_data.get('vehicle status').get('battery_charge') is None:
            return None
        return self.current_data['vehicle status']['battery_charge']

    def get_captain_state(self):
        """
         Get captain state
        :return:  return captain state
        :rtype: int
        """
        if self.current_data is None:
            return None
        if self.current_data.get('vehicle status') is None:
            return None
        if self.current_data.get('vehicle status').get('captain_state') is None:
            return None
        return self.current_data['vehicle status']['captain_state']

    def get_captain_status(self):
        """
        Get captain status
        :return: return captain status
        :rtype: dict
        """
        if self.current_data is None:
            return None
        if self.current_data.get('captain status') is None:
            return None
        return self.current_data['captain status']


    def get_cpu_usage(self):
        """
        Get cpu usage
        :return: return vehicle cpu usage
        :rtype: float
        """
        if self.current_data is None:
            return None
        if self.current_data.get('cpu usage') is None:
            return None
        if self.current_data.get('cpu usage').get('data') is None:
            return None
        return self.current_data['cpu usage']['data']

    def get_ram_usage(self):
        """
        Get ram usage
        :return: return vehicle ram usage
        :rtype: float
        """
        if self.current_data is None:
            return None
        if self.current_data.get('ram usage') is None:
            return None
        if self.current_data.get('ram usage').get('data') is None:
            return None
        return self.current_data['ram usage']['data']

    def get_thruster_setpoints(self):
        """
         Get thruster setpoints topic
        :return: return thruster setpoints
        :rtype: dict
        """
        if self.current_data is None:
            return None
        if self.current_data.get('thruster setpoints') is None:
            return None
        return self.current_data['thruster setpoints']

    def get_captain_state_feedback(self):
        """
         Get captain state feedback
        :return: return state feedback
        :rtype: dict
        """
        if self.current_data is None:
            return None
        if self.current_data.get('state feedback') is None:
            return None
        return self.current_data['state feedback']

    def get_mission_active(self):
        """
        Get mission active
        :return: return mission active state
        :rtype: bool
        """
        if self.current_data is None:
            return None
        if self.current_data.get('vehicle status') is None:
            return None
        if self.current_data.get('vehicle status').get('mission_active') is None:
            return None
        return self.current_data['vehicle status']['mission_active']

    def get_total_steps(self):
        """
        Get total steps
        :return: return number of steps in the mission
        :rtype: int
        """
        if self.current_data is None:
            return None
        if self.current_data.get('vehicle status') is None:
            return None
        if self.current_data.get('vehicle status').get('total_steps') is None:
            return None
        return self.current_data['vehicle status']['total_steps']

    def get_status_code(self):
        """
        Get the status code
        :return: return status code codified in a byte
        :rtype: int
        """
        if self.current_data is None:
            return None
        if self.current_data.get('safety supervisor status') is None:
            return None
        if self.current_data.get('safety supervisor status').get('status_code') is None:
            return None

        return self.current_data['safety supervisor status']['status_code']

    # back compatibility
    def get_error_code(self):
        """
        Get the error code
        :return: return error code codified in a byte
        :rtype: int
        """
        if self.current_data is None:
            return None
        if self.current_data.get('safety supervisor status') is None:
            return None
        if self.current_data.get('safety supervisor status').get('error_code') is None:
            return None

        return self.current_data['safety supervisor status']['error_code']

    def get_recovery_action(self):
        """ Get recovery action
        :return: return recovery action codified in a byte
        :rtype: int
        """
        if self.current_data is None:
            return None

        if self.current_data.get('safety supervisor status') is None:
            return None

        recovery_action = self.current_data['safety supervisor status']['recovery_action']
        error_level = recovery_action['error_level']
        error_string = recovery_action['error_string']
        return error_level, error_string

    def get_rosout(self):
        """
         Get rosout
        :return: returns rosout
        :rtype: list
        """
        if self.current_data.get('rosout') is None:
            return None

        if not self.current_data['rosout']:
            return None
        return self.current_data['rosout']

    def get_calibrate_magnetometer_service(self):
        """
        Get calibrate magnetometer service
        :return: returns calibrate magnetometer service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('calibrate magnetometer') is None:
            return None
        return self.services['calibrate magnetometer']

    def get_stop_magnetometer_calibration_service(self):
        """
        Get stop magnetometer calibration service
        :return: return stop magnetometer calibration service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('stop magnetometer calibration') is None:
            return None
        return self.services['stop magnetometer calibration']

    def get_keep_position_service(self):
        """
        Get keep position service
        :return: return keep position service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('keep position') is None:
            return None

        return self.services['keep position']

    def get_disable_keep_position_service(self):
        """
        Get disable keep position service
        :return: return disable keep position service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('disable keep position') is None:
            return None

        return self.services['disable keep position']

    def get_disable_all_keep_positions_service(self):
        """
        Get disable all keep position service
        :return: return disable all keep position service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('disable all keep positions') is None:
            return None

        return self.services['disable all keep positions']

    def get_reset_timeout_service(self):
        """ Get reset timeout service
        :return: return reset timeout service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('reset timeout') is None:
            return None

        return self.services['reset timeout']

    def get_goto_service(self):
        """
        Get goto service
        :return: return enable goto service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('enable goto') is None:
            return None

        return self.services['enable goto']

    def get_disable_goto_service(self):
        """
        Get disable goto service
        :return: return disable goto service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('disable goto') is None:
            return None

        return self.services['disable goto']

    def get_enable_thrusters_service(self):
        """
        Get enable thrusters service
        :return: return enable thrusters service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('enable thrusters') is None:
            return None

        return self.services['enable thrusters']

    def get_disable_thrusters_service(self):
        """
         Get disable thrusters service
        :return: return disable thrusters service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('disable thrusters') is None:
            return None

        return self.services['disable thrusters']

    def get_enable_mission_service(self):
        """
        Get enable mission service
        :return: return enable mission service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('enable mission') is None:
            return None

        return self.services['enable mission']

    def get_disable_mission_service(self):
        """
        Get disable mission service
        :return: return disable mission service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('disable mission') is None:
            return None

        return self.services['disable mission']

    def get_pause_mission_service(self):
        """
        Get pause mission service
        :return: return pause mission service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('pause mission') is None:
            return None

        return self.services['pause mission']

    def get_resume_mission_service(self):
        """
        Get pause mission service
        :return: return pause mission service
        :rtype: str
        """
        if self.services is None:
            return None
        if self.services.get('resume mission') is None:
            return None

        return self.services['resume mission']

    def get_teleoperation_launch(self):
        """
        Get teleoperation launch
        :return: return teleoperation launch
        :rtype: str
        """
        if self.controlstation_launchlist is None:
            return None
        if self.controlstation_launchlist.get('teleoperation') is None:
            return None

        return self.controlstation_launchlist['teleoperation']

    def get_vehicle_launch_dict(self):
        """
        Get vehicle launch list
        :return: return vehicle launch list
        :rtype: dict
        """
        if self.vehicle_launchlist is None:
            return None

        return self.vehicle_launchlist

    def set_data(self, identifier, data):
        """
        set data 'data' in  self.current_data list with key 'identifier'

        :param identifier: key to identify the data
        :param data: new data to update
        """

        if data:
            self.current_data[identifier] = data
        else:
            self.state_signal.emit()

    def set_buffer(self, identifier, buffer):
        """
        set data 'buffer' in  self.current_data list with key 'identifier'

        :param identifier: key to identify the data
        :param buffer: new data to update
        """

        self.current_data[identifier] = buffer

    def is_subscribed(self):
        """ get subscribed state
        :return: return subscribe state
        :rtype: bool
        """
        return self.subscribed

    def disconnect(self):
        """ Disconnect subscribers and stop timer"""
        self.subscribed = False

        for key, subscriber in self.topics.items():
            if subscriber is not None:
                subscriber.close()

        self.timer.stop()

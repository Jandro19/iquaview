# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_lm_coords_dlg.ui'
#
# Created by: PyQt5 UI code generator 5.5.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_LandmarkCoordinatesDialog(object):
    def setupUi(self, LandmarkCoordinatesDialog):
        LandmarkCoordinatesDialog.setObjectName("LandmarkCoordinatesDialog")
        LandmarkCoordinatesDialog.resize(285, 117)
        self.verticalLayout = QtWidgets.QVBoxLayout(LandmarkCoordinatesDialog)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label = QtWidgets.QLabel(LandmarkCoordinatesDialog)
        self.label.setMinimumSize(QtCore.QSize(75, 0))
        self.label.setMaximumSize(QtCore.QSize(75, 16777215))
        self.label.setObjectName("label")
        self.horizontalLayout.addWidget(self.label)
        self.latitude_lineEdit = QtWidgets.QLineEdit(LandmarkCoordinatesDialog)
        self.latitude_lineEdit.setFocusPolicy(QtCore.Qt.NoFocus)
        self.latitude_lineEdit.setReadOnly(True)
        self.latitude_lineEdit.setObjectName("latitude_lineEdit")
        self.horizontalLayout.addWidget(self.latitude_lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_2 = QtWidgets.QLabel(LandmarkCoordinatesDialog)
        self.label_2.setMinimumSize(QtCore.QSize(75, 0))
        self.label_2.setMaximumSize(QtCore.QSize(75, 16777215))
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        self.longitude_lineEdit = QtWidgets.QLineEdit(LandmarkCoordinatesDialog)
        self.longitude_lineEdit.setFocusPolicy(QtCore.Qt.NoFocus)
        self.longitude_lineEdit.setReadOnly(True)
        self.longitude_lineEdit.setObjectName("longitude_lineEdit")
        self.horizontalLayout_2.addWidget(self.longitude_lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem)
        self.copy_button = QtWidgets.QPushButton(LandmarkCoordinatesDialog)
        self.copy_button.setMinimumSize(QtCore.QSize(27, 0))
        self.copy_button.setMaximumSize(QtCore.QSize(27, 16777215))
        self.copy_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.copy_button.setText("")
        self.copy_button.setObjectName("copy_button")
        self.horizontalLayout_3.addWidget(self.copy_button)
        self.close_button = QtWidgets.QPushButton(LandmarkCoordinatesDialog)
        self.close_button.setObjectName("close_button")
        self.horizontalLayout_3.addWidget(self.close_button)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(LandmarkCoordinatesDialog)
        self.close_button.pressed.connect(LandmarkCoordinatesDialog.close)
        QtCore.QMetaObject.connectSlotsByName(LandmarkCoordinatesDialog)

    def retranslateUi(self, LandmarkCoordinatesDialog):
        _translate = QtCore.QCoreApplication.translate
        LandmarkCoordinatesDialog.setWindowTitle(_translate("LandmarkCoordinatesDialog", "Landmark Coordinates"))
        self.label.setText(_translate("LandmarkCoordinatesDialog", "Latitude:"))
        self.label_2.setText(_translate("LandmarkCoordinatesDialog", "Longitude:"))
        self.close_button.setText(_translate("LandmarkCoordinatesDialog", "Close"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    LandmarkCoordinatesDialog = QtWidgets.QDialog()
    ui = Ui_LandmarkCoordinatesDialog()
    ui.setupUi(LandmarkCoordinatesDialog)
    LandmarkCoordinatesDialog.show()
    sys.exit(app.exec_())


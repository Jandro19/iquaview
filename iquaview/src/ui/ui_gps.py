# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_gps.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_GPSwidget(object):
    def setupUi(self, GPSwidget):
        GPSwidget.setObjectName("GPSwidget")
        GPSwidget.resize(313, 165)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(GPSwidget.sizePolicy().hasHeightForWidth())
        GPSwidget.setSizePolicy(sizePolicy)
        GPSwidget.setMinimumSize(QtCore.QSize(0, 0))
        GPSwidget.setMaximumSize(QtCore.QSize(16777215, 16777215))
        self.verticalLayout = QtWidgets.QVBoxLayout(GPSwidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.connectButton = QtWidgets.QPushButton(GPSwidget)
        self.connectButton.setObjectName("connectButton")
        self.verticalLayout.addWidget(self.connectButton)
        self.trackwidget = TrackWidget(GPSwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.trackwidget.sizePolicy().hasHeightForWidth())
        self.trackwidget.setSizePolicy(sizePolicy)
        self.trackwidget.setMinimumSize(QtCore.QSize(0, 0))
        self.trackwidget.setObjectName("trackwidget")
        self.verticalLayout.addWidget(self.trackwidget)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.status_label = QtWidgets.QLabel(GPSwidget)
        font = QtGui.QFont()
        font.setItalic(True)
        self.status_label.setFont(font)
        self.status_label.setObjectName("status_label")
        self.horizontalLayout.addWidget(self.status_label)
        self.gps_status_label = QtWidgets.QLabel(GPSwidget)
        self.gps_status_label.setMinimumSize(QtCore.QSize(240, 0))
        self.gps_status_label.setText("")
        self.gps_status_label.setObjectName("gps_status_label")
        self.horizontalLayout.addWidget(self.gps_status_label)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.status_label_2 = QtWidgets.QLabel(GPSwidget)
        font = QtGui.QFont()
        font.setItalic(True)
        self.status_label_2.setFont(font)
        self.status_label_2.setObjectName("status_label_2")
        self.horizontalLayout_7.addWidget(self.status_label_2)
        self.gps_fix_quality_label = QtWidgets.QLabel(GPSwidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gps_fix_quality_label.sizePolicy().hasHeightForWidth())
        self.gps_fix_quality_label.setSizePolicy(sizePolicy)
        self.gps_fix_quality_label.setMinimumSize(QtCore.QSize(0, 0))
        self.gps_fix_quality_label.setText("")
        self.gps_fix_quality_label.setObjectName("gps_fix_quality_label")
        self.horizontalLayout_7.addWidget(self.gps_fix_quality_label)
        self.verticalLayout.addLayout(self.horizontalLayout_7)

        self.retranslateUi(GPSwidget)
        QtCore.QMetaObject.connectSlotsByName(GPSwidget)

    def retranslateUi(self, GPSwidget):
        _translate = QtCore.QCoreApplication.translate
        GPSwidget.setWindowTitle(_translate("GPSwidget", "GPS"))
        self.connectButton.setText(_translate("GPSwidget", "Connect"))
        self.status_label.setText(_translate("GPSwidget", "Status:"))
        self.status_label_2.setText(_translate("GPSwidget", "GPS fix quality:"))

from canvastracks.trackwidget import TrackWidget
import resources_rc

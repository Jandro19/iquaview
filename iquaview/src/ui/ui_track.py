# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_track.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Track(object):
    def setupUi(self, Track):
        Track.setObjectName("Track")
        Track.resize(299, 67)
        Track.setMinimumSize(QtCore.QSize(0, 0))
        Track.setMaximumSize(QtCore.QSize(299, 98))
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(Track)
        self.horizontalLayout_2.setContentsMargins(0, 9, 0, 9)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.track_label = QtWidgets.QLabel(Track)
        self.track_label.setMinimumSize(QtCore.QSize(90, 0))
        self.track_label.setMaximumSize(QtCore.QSize(100, 16777215))
        self.track_label.setObjectName("track_label")
        self.horizontalLayout_2.addWidget(self.track_label)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.centerButton = QtWidgets.QPushButton(Track)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centerButton.sizePolicy().hasHeightForWidth())
        self.centerButton.setSizePolicy(sizePolicy)
        self.centerButton.setText("")
        self.centerButton.setObjectName("centerButton")
        self.horizontalLayout_2.addWidget(self.centerButton)
        self.color_btn = gui.QgsColorButton(Track)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.color_btn.sizePolicy().hasHeightForWidth())
        self.color_btn.setSizePolicy(sizePolicy)
        self.color_btn.setMinimumSize(QtCore.QSize(0, 0))
        self.color_btn.setObjectName("color_btn")
        self.horizontalLayout_2.addWidget(self.color_btn)
        self.save_track_pushButton = QtWidgets.QPushButton(Track)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.save_track_pushButton.sizePolicy().hasHeightForWidth())
        self.save_track_pushButton.setSizePolicy(sizePolicy)
        self.save_track_pushButton.setText("")
        self.save_track_pushButton.setObjectName("save_track_pushButton")
        self.horizontalLayout_2.addWidget(self.save_track_pushButton)
        self.clearTrackButton = QtWidgets.QPushButton(Track)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.clearTrackButton.sizePolicy().hasHeightForWidth())
        self.clearTrackButton.setSizePolicy(sizePolicy)
        self.clearTrackButton.setText("")
        self.clearTrackButton.setObjectName("clearTrackButton")
        self.horizontalLayout_2.addWidget(self.clearTrackButton)

        self.retranslateUi(Track)
        QtCore.QMetaObject.connectSlotsByName(Track)

    def retranslateUi(self, Track):
        _translate = QtCore.QCoreApplication.translate
        Track.setWindowTitle(_translate("Track", "Track"))
        self.track_label.setText(_translate("Track", "Display Track"))

from qgis import gui

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_connection_settings.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_connectionSettingsWidget(object):
    def setupUi(self, connectionSettingsWidget):
        connectionSettingsWidget.setObjectName("connectionSettingsWidget")
        connectionSettingsWidget.resize(333, 497)
        self.verticalLayout = QtWidgets.QVBoxLayout(connectionSettingsWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.auv_connection_widget = AUVConnectionWidget(connectionSettingsWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.auv_connection_widget.sizePolicy().hasHeightForWidth())
        self.auv_connection_widget.setSizePolicy(sizePolicy)
        self.auv_connection_widget.setMinimumSize(QtCore.QSize(303, 80))
        self.auv_connection_widget.setObjectName("auv_connection_widget")
        self.verticalLayout.addWidget(self.auv_connection_widget)
        self.gps_connection_widget = GPSConnectionWidget(connectionSettingsWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.gps_connection_widget.sizePolicy().hasHeightForWidth())
        self.gps_connection_widget.setSizePolicy(sizePolicy)
        self.gps_connection_widget.setMinimumSize(QtCore.QSize(303, 255))
        self.gps_connection_widget.setObjectName("gps_connection_widget")
        self.verticalLayout.addWidget(self.gps_connection_widget)
        self.usbl_connection_widget = USBLConnectionWidget(connectionSettingsWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.usbl_connection_widget.sizePolicy().hasHeightForWidth())
        self.usbl_connection_widget.setSizePolicy(sizePolicy)
        self.usbl_connection_widget.setMinimumSize(QtCore.QSize(303, 110))
        self.usbl_connection_widget.setObjectName("usbl_connection_widget")
        self.verticalLayout.addWidget(self.usbl_connection_widget)
        self.teleoperation_connection_widget = TeleoperationConnectionWidget(connectionSettingsWidget)
        self.teleoperation_connection_widget.setObjectName("teleoperation_connection_widget")
        self.verticalLayout.addWidget(self.teleoperation_connection_widget)
        spacerItem = QtWidgets.QSpacerItem(20, 30, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)

        self.retranslateUi(connectionSettingsWidget)
        QtCore.QMetaObject.connectSlotsByName(connectionSettingsWidget)

    def retranslateUi(self, connectionSettingsWidget):
        _translate = QtCore.QCoreApplication.translate
        connectionSettingsWidget.setWindowTitle(_translate("connectionSettingsWidget", "Form"))

from connection.settings.auvconnectionwidget import AUVConnectionWidget
from connection.settings.gpsconnectionwidget import GPSConnectionWidget
from connection.settings.teleoperationconnectionwidget import TeleoperationConnectionWidget
from connection.settings.usblconnectionwidget import USBLConnectionWidget

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    connectionSettingsWidget = QtWidgets.QWidget()
    ui = Ui_connectionSettingsWidget()
    ui.setupUi(connectionSettingsWidget)
    connectionSettingsWidget.show()
    sys.exit(app.exec_())


# Changelog
All notable changes to IQUAview are documented in this file.

## [1.2.0] - 2019-10-26
### Added

* Method to check if all actions in a mission are present in the config XML.
* Multiple missions can be loaded at once.
* Vehicle roslaunch list added to XML configuration.
* Support to import CSV layers.
* Added selector to change Project Coordinate Reference System (CRS).
* World map layer as default layer.
* Buttons to pause and resume a mission.
* Drag&Drop functionallity to import missions and layers (vector and raster) to the project.
* SFTP client to get and transfer files from/to the AUV.
* Undo (ctrl+z) functionallity when moving and rotating missions.
* Show coordinates of single landmark points by right clicking them in the layer legend.
* Now hovering the mouse over a mission or a vector layer displays the name of that layer in a tooltip.
* Button to move NED Origin.
### Changed
* Tracks remain on the canvas even if the connection is lost.
* Rotation key changed from Ctrl to Shift.
* Service calls are handled as Trigger services instead of Empty services.
* Re-styling of battery and thrusters widgets.
### Fixed
* Minor bugs

## [1.1.0] - 2019-02-26

* First Release version.

## [1.0.0] - 2017-07-30

* Initial version.

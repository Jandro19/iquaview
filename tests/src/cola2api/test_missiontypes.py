"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from iquaview.src.cola2api import  mission_types

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestMissionTypes(unittest.TestCase):

    def test_types(self):
        position = mission_types.MissionPosition(41.3, 3.3, 0.0, False)
        position_2 = mission_types.MissionPosition(0.0, 0.0, 5.0, True)
        self.assertEqual(type(position.get_latitude()), type(41.3))
        self.assertEqual(type(position.get_longitude()), type(3.3))
        self.assertEqual(type(position.get_z()), type(0.0))
        self.assertEqual(type(position.get_altitude_mode()), type(False))

        position.copy(position_2)

        self.assertEqual(position.get_latitude(), 0.0)
        self.assertEqual(position.get_longitude(), 0.0)
        self.assertEqual(position.get_z(), 5.0)
        self.assertEqual(position.get_altitude_mode(), True)

        position.set(1.0, 1.0, 1.0, False)

        self.assertEqual(type(position.get_latitude()), type(1.0))
        self.assertEqual(type(position.get_longitude()), type(1.0))
        self.assertEqual(type(position.get_z()), type(1.0))
        self.assertEqual(type(position.get_altitude_mode()), type(False))

        position.set_lat_lon(30.1, 10.1)

        self.assertEqual(type(position.get_latitude()), type(30.1))
        self.assertEqual(type(position.get_longitude()), type(10.1))

        tolerance = mission_types.MissionTolerance(0.0, 0.0, 0.0)
        tolerance_2 = mission_types.MissionTolerance(2.0, 2.0, 2.0)
        self.assertEqual(type(tolerance.x), type(0.0))
        self.assertEqual(type(tolerance.y), type(0.0))
        self.assertEqual(type(tolerance.z), type(0.0))

        tolerance.copy(tolerance_2)

        self.assertEqual(type(tolerance.x), type(2.0))
        self.assertEqual(type(tolerance.y), type(2.0))
        self.assertEqual(type(tolerance.z), type(2.0))

        tolerance.set(4.0, 4.0, 4.0)

        self.assertEqual(type(tolerance.x), type(4.0))
        self.assertEqual(type(tolerance.y), type(4.0))
        self.assertEqual(type(tolerance.z), type(4.0))

        self.assertTrue(issubclass(mission_types.MissionWaypoint, mission_types.MissionManeuver))
        self.assertTrue(issubclass(mission_types.MissionSection, mission_types.MissionManeuver))
        self.assertTrue(issubclass(mission_types.MissionPark, mission_types.MissionManeuver))

        mission = mission_types.Mission()
        mw = mission_types.MissionWaypoint(position, 1.0, tolerance)
        ms = mission_types.MissionSection(mission_types.MissionPosition(41.777, 3.030, 15.0, False),
                                          mission_types.MissionPosition(41.787, 3.034, 15.0, False),
                                          0.5,
                                          mission_types.MissionTolerance(2.0, 2.0, 1.0))
        mp = mission_types.MissionPark(mission_types.MissionPosition(41.777, 3.030, 15.0, False),
                                       0.5,
                                       120,
                                       mission_types.MissionTolerance(2.0, 2.0, 1.0))

        self.assertEqual(mission.get_length(), 0)
        self.assertEqual(mission.size(), 0)
        mission_step = mission_types.MissionStep()
        mission_step.add_maneuver(mw)
        mission.add_step(mission_step)
        self.assertEqual(mission.get_length(), 1)
        self.assertEqual(mission.size(), 1)
        mission_step = mission_types.MissionStep()
        mission_step.add_maneuver(mw)
        mission.add_step(mission_step)
        self.assertEqual(mission.get_length(), 2)
        self.assertEqual(mission.size(), 2)
        mission_step = mission_types.MissionStep()
        mission_step.add_maneuver(mw)
        mission.add_step(mission_step)
        self.assertEqual(mission.get_length(), 3)
        self.assertEqual(mission.size(), 3)
        mission.remove_step(2)
        self.assertEqual(mission.get_length(), 2)
        self.assertEqual(mission.size(), 2)
        mission.remove_step(1)
        self.assertEqual(mission.get_length(), 1)
        self.assertEqual(mission.size(), 1)
        mission.remove_step(0)
        self.assertEqual(mission.get_length(), 0)
        self.assertEqual(mission.size(), 0)

if __name__ == "__main__":
    unittest.main()

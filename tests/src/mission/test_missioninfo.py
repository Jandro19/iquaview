"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from qgis.gui import QgsMapCanvas
from qgis.core import QgsProject


from PyQt5.QtWidgets import QApplication

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.mission.missioninfo import MissionInfo
from iquaview.src.mission.missiontrack import MissionTrack
from iquaview.src.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 Parameter,
                                                 MissionAction,
                                                 MissionPosition,
                                                 MissionTolerance,
                                                 MissionSection,
                                                 MissionPark,
                                                 MissionWaypoint)


class TestMissionInfo(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.mission_name = "temp_mission"

        self.canvas = QgsMapCanvas()
        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.missioninfo = None

    def write_temp_mission_xml_surface(self):

        mission = Mission()
        mission_step = MissionStep()
        param = Parameter("abcd")
        param_2 = Parameter("2")
        parameters = list()
        parameters.append(param)
        parameters.append(param_2)
        action = MissionAction("action1", parameters)
        mission_step.add_action(action)
        wp = MissionWaypoint(MissionPosition(41.777, 3.030, 0.0, False),
                             0.5,
                             MissionTolerance(2.0, 2.0, 1.0))
        mission_step.add_maneuver(wp)
        mission.add_step(mission_step)
        mission_step2 = MissionStep()
        sec = MissionSection(MissionPosition(41.777, 3.030, 15.0, False),
                             MissionPosition(41.787, 3.034, 15.0, False),
                             0.5,
                             MissionTolerance(2.0, 2.0, 1.0))
        mission_step2.add_maneuver(sec)
        mission.add_step(mission_step2)
        mission_step3 = MissionStep()
        park = MissionPark(MissionPosition(41.777, 3.030, 0.0, False),
                           0.5,
                           120,
                           MissionTolerance(2.0, 2.0, 1.0))
        mission_step3.add_maneuver(park)
        mission.add_step(mission_step3)
        mission.write_mission(self.mission_name+'.xml')


        return mission

    def write_temp_mission_xml_dive(self):

        mission = Mission()
        mission_step = MissionStep()
        param = Parameter("abcd")
        param_2 = Parameter("2")
        parameters = list()
        parameters.append(param)
        parameters.append(param_2)
        action = MissionAction("action1", parameters)
        mission_step.add_action(action)
        wp = MissionWaypoint(MissionPosition(41.777, 3.030, 15.0, False),
                             0.5,
                             MissionTolerance(2.0, 2.0, 1.0))
        mission_step.add_maneuver(wp)
        mission.add_step(mission_step)
        mission_step2 = MissionStep()
        sec = MissionSection(MissionPosition(41.777, 3.030, 15.0, False),
                             MissionPosition(41.787, 3.034, 15.0, False),
                             0.5,
                             MissionTolerance(2.0, 2.0, 1.0))
        mission_step2.add_maneuver(sec)
        mission.add_step(mission_step2)
        mission_step3 = MissionStep()
        park = MissionPark(MissionPosition(41.777, 3.030, 15.0, False),
                           0.5,
                           120,
                           MissionTolerance(2.0, 2.0, 1.0))
        mission_step3.add_maneuver(park)
        mission.add_step(mission_step3)
        mission.write_mission(self.mission_name+'.xml')


        return mission


    def write_temp_mission_xml_empty(self):
        mission = Mission()
        mission.write_mission(self.mission_name + '.xml')
        return mission

    def test_set_current_mission(self):
        old_name = self.mission_name
        new_name = "new_temp_mission"

        mission = self.write_temp_mission_xml_empty()
        mt = MissionTrack(self.mission_name,
                          canvas=self.canvas, proj=self.proj)
        mt.set_mission(mission)
        mt.render_mission()
        self.missioninfo = MissionInfo(self.canvas, mt)

        self.missioninfo.set_current_mission(new_name)
        self.assertEqual(self.missioninfo.current_mission, new_name)

        #reset name
        self.missioninfo.set_current_mission(old_name)
        self.assertEqual(self.missioninfo.current_mission, old_name)

    def test_emtpy_values(self):

        mission = self.write_temp_mission_xml_empty()
        mt = MissionTrack(self.mission_name,
                          canvas=self.canvas, proj=self.proj)
        mt.set_mission(mission)
        mt.render_mission()
        self.missioninfo = MissionInfo(self.canvas, mt)

        self.missioninfo.update_values()

        self.assertEqual(self.missioninfo.first_waypoint_onsurface.text(), "-")
        self.assertEqual(self.missioninfo.last_waypoint_onsurface.text(), "-")
        self.assertEqual(self.missioninfo.total_distance.text(), "-")
        self.assertEqual(self.missioninfo.estimated_time.text(), "-")

    def test_values(self):

        mission = self.write_temp_mission_xml_surface()
        mt = MissionTrack(self.mission_name,
                          canvas=self.canvas, proj=self.proj)
        mt.set_mission(mission)
        mt.render_mission()
        self.missioninfo = MissionInfo(self.canvas, mt)

        self.missioninfo.update_values()

        self.assertEqual(self.missioninfo.first_waypoint_onsurface.text(), "True")
        self.assertEqual(self.missioninfo.last_waypoint_onsurface.text(), "True")

        self.assertNotEqual(self.missioninfo.total_distance.text(), "-")
        self.assertNotEqual(self.missioninfo.estimated_time.text(), "-")

        mission = self.write_temp_mission_xml_dive()
        mt = MissionTrack(self.mission_name,
                          canvas=self.canvas, proj=self.proj)
        mt.set_mission(mission)
        mt.render_mission()
        self.missioninfo = MissionInfo(self.canvas, mt)

        self.missioninfo.update_values()

        self.assertEqual(self.missioninfo.first_waypoint_onsurface.text(), "False")
        self.assertEqual(self.missioninfo.last_waypoint_onsurface.text(), "False")

        self.assertNotEqual(self.missioninfo.total_distance.text(), "-")
        self.assertNotEqual(self.missioninfo.estimated_time.text(), "-")

    def tearDown(self):
        os.remove("temp_mission.xml")

if __name__ == "__main__":
    unittest.main()

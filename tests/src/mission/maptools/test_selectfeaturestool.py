"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtCore import QPoint, Qt, QEvent
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QApplication
from qgis.core import QgsProject, QgsApplication, QgsPointXY
from qgis.gui import QgsMapCanvas, QgsMapMouseEvent


from iquaview.src.config import Config
from iquaview.src.mission.maptools.selectfeaturestool import SelectFeaturesTool
from iquaview.src.vehicle.vehicleinfo import VehicleInfo
from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionPosition,
                                                 MissionTolerance,
                                                 MissionWaypoint)

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestEditTool(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)

        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.canvas = QgsMapCanvas()
        self.view = None
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()

        self.mission_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)

        self.mission_name = "temp_selectfeaturestool_mission"
        self.write_temp_mission_xml()
        self.mission_ctrl.load_mission(os.getcwd()+"/"+self.mission_name + ".xml")

        self.mission_track = self.mission_ctrl.mission_list[0]

        self.select_tool = SelectFeaturesTool(self.mission_track, self.canvas)

    def tearDown(self):
        os.remove(self.mission_name + ".xml")

    def test_individual_selection(self):
        x = self.mission_track.get_step(0).get_maneuver().get_position().get_longitude()
        y = self.mission_track.get_step(0).get_maneuver().get_position().get_latitude()
        first_wp_canvas = self.select_tool.toCanvasCoordinates(QgsPointXY(float(x), float(y)))

        x = self.mission_track.get_step(1).get_maneuver().get_position().get_longitude()
        y = self.mission_track.get_step(1).get_maneuver().get_position().get_latitude()
        second_wp_canvas = self.select_tool.toCanvasCoordinates(QgsPointXY(float(x), float(y)))

        x = self.mission_track.get_step(2).get_maneuver().get_position().get_longitude()
        y = self.mission_track.get_step(2).get_maneuver().get_position().get_latitude()
        third_wp_canvas = self.select_tool.toCanvasCoordinates(QgsPointXY(float(x), float(y)))

        key_press_event = QKeyEvent(QEvent.KeyPress, Qt.Key_Control, Qt.NoModifier)
        self.select_tool.keyPressEvent(key_press_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, first_wp_canvas, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, first_wp_canvas, Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, second_wp_canvas, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, second_wp_canvas, Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, third_wp_canvas, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, third_wp_canvas, Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        key_release_event = QKeyEvent(QEvent.KeyRelease, Qt.Key_Control, Qt.NoModifier)
        self.select_tool.keyReleaseEvent(key_release_event)

        self.assertEqual(3, len(self.select_tool.get_indexes_within_list()))

        # delete wp
        for step in reversed(range(0,3)):
            self.mission_track.remove_step(step)

        self.assertEqual(0, len(self.select_tool.get_indexes_within_list()))

    def test_multiple_selection(self):
        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, QPoint(0, 0), Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, QPoint(0, 0), Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, QPoint(10, -60))
        self.select_tool.canvasMoveEvent(move_event)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, QPoint(10, -60), Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, QPoint(10, -60), Qt.LeftButton)
        self.select_tool.canvasPressEvent(press_event)
        self.select_tool.canvasReleaseEvent(release_event)

        self.assertEqual(3, len(self.select_tool.get_indexes_within_list()))

        # delete wp
        for step in reversed(range(0, 3)):
            self.mission_track.remove_step(step)

        self.assertEqual(0, len(self.select_tool.get_indexes_within_list()))

    def write_temp_mission_xml(self):
        mission = Mission()

        wp1 = MissionWaypoint(MissionPosition(42, 4, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))
        wp2 = MissionWaypoint(MissionPosition(45, 7, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))
        wp3 = MissionWaypoint(MissionPosition(39, 2, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))

        mission_step1 = MissionStep()
        mission_step2 = MissionStep()
        mission_step3 = MissionStep()

        mission_step1.add_maneuver(wp1)
        mission_step2.add_maneuver(wp2)
        mission_step3.add_maneuver(wp3)

        mission.add_step(mission_step1)
        mission.add_step(mission_step2)
        mission.add_step(mission_step3)

        mission.write_mission(self.mission_name + '.xml')

if __name__ == "__main__":
    unittest.main()
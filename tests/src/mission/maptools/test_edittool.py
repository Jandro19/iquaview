"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtCore import QEvent, QPoint, Qt
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QApplication
from qgis.core import QgsProject, QgsApplication, QgsCoordinateReferenceSystem
from qgis.gui import QgsMapCanvas, QgsMapMouseEvent


from iquaview.src.config import Config
from iquaview.src.mission.maptools.edittool import EditTool
from iquaview.src.vehicle.vehicleinfo import VehicleInfo
from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.cola2api.mission_types import Mission

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestEditTool(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)

        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.canvas = QgsMapCanvas()
        self.view = None
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()

        self.mission_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)

        self.mission_name = "temp_edittool_mission"
        self.write_temp_mission_xml()
        self.mission_ctrl.load_mission(os.getcwd()+"/"+self.mission_name + ".xml")

        self.mission_track = self.mission_ctrl.mission_list[0]

        self.edit_tool = EditTool(self.mission_track, self.canvas, self.msg_log)

    def tearDown(self):
        self.edit_tool.close_band()
        os.remove(self.mission_name + ".xml")

    def test_create_delete_wp(self):
        self.assertEqual(0, self.mission_track.get_mission_length())

        width = self.canvas.width()
        height = self.canvas.height()
        h_dist = width / 20
        v_dist = height / 20

        # Create 20 points clicking in 20 different positions
        for i in range(0, 20):
            point = QPoint(i * h_dist, i * v_dist)
            press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point, Qt.LeftButton)
            release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, point, Qt.LeftButton)
            self.edit_tool.canvasPressEvent(press_event)
            self.edit_tool.canvasReleaseEvent(release_event)

        self.assertEqual(20, self.mission_track.get_mission_length())

        # Delete the 20 points created by right clicking
        for i in range(0, 20):
            point = QPoint(i * h_dist, i * v_dist)
            press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point, Qt.RightButton)
            release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, point, Qt.RightButton)
            self.edit_tool.canvasPressEvent(press_event)
            self.edit_tool.canvasReleaseEvent(release_event)

        self.assertEqual(0, self.mission_track.get_mission_length())

    def test_in_between_and_drag_wp(self):
        width = self.canvas.width()
        height = self.canvas.height()

        # Create 3 points where p2 is in between points p1 and p3
        p1 = QPoint(width / 4, height / 2)
        p2 = QPoint(width / 2, height / 2)
        p3 = QPoint(3 * width / 4, height / 2)

        # create p1 and p3
        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, p1, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, p1, Qt.LeftButton)
        self.edit_tool.canvasPressEvent(press_event)
        self.edit_tool.canvasReleaseEvent(release_event)
        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, p3, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, p3, Qt.LeftButton)
        self.edit_tool.canvasPressEvent(press_event)
        self.edit_tool.canvasReleaseEvent(release_event)

        # create in between point p2
        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, p2, Qt.LeftButton)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, p2, Qt.LeftButton)
        self.edit_tool.canvasPressEvent(press_event)
        self.edit_tool.canvasReleaseEvent(release_event)

        # second point should be p2 and not p3
        x = self.mission_track.get_step(1).get_maneuver().get_position().get_longitude()
        y = self.mission_track.get_step(1).get_maneuver().get_position().get_latitude()
        # detail: going vertically down increases y value from canvas window perspective, while decreases
        # the value in the map perspective (latitude) so the y coordinate must be converted first to negative and +1
        second = QPoint(x, y*(-1)+1)
        self.assertEqual(p2, second)

        # now drag this in between point up and left
        final_p2 = QPoint(width / 3, height / 3)

        press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, p2, Qt.LeftButton)
        move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, final_p2)
        release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, final_p2, Qt.LeftButton)

        self.edit_tool.canvasPressEvent(press_event)
        self.edit_tool.canvasMoveEvent(move_event)
        self.edit_tool.canvasReleaseEvent(release_event)

        x = self.mission_track.get_step(1).get_maneuver().get_position().get_longitude()
        y = self.mission_track.get_step(1).get_maneuver().get_position().get_latitude()

        final_pos = QPoint(x, y*(-1)+1)
        self.assertEqual(final_p2, final_pos)

    def test_crs_change(self):
        h_dist = self.canvas.width() / 20
        v_dist = self.canvas.height() / 20

        # Create 3 points
        for i in range(0, 3):
            point = QPoint(i * h_dist, i * v_dist)
            press_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonPress, point, Qt.LeftButton)
            release_event = QgsMapMouseEvent(self.canvas, QEvent.MouseButtonRelease, point, Qt.LeftButton)
            self.edit_tool.canvasPressEvent(press_event)
            self.edit_tool.canvasReleaseEvent(release_event)

        # Change canvas crs, this will change rubber band point locations
        prev_crs = (self.mission_track.get_mission_layer().crs())
        prev_geom = self.edit_tool.rubber_band_points.asGeometry()
        crs = QgsCoordinateReferenceSystem("EPSG:3857")  # WSG 84 / Pseudo-Mercator
        self.canvas.setDestinationCrs(crs)
        curr_geom = self.edit_tool.rubber_band_points.asGeometry()

        self.assertNotEqual(prev_geom.asMultiPoint(), curr_geom.asMultiPoint())

        # Transform corrds again to the initial crs
        self.canvas.setDestinationCrs(prev_crs)
        curr_geom = self.edit_tool.rubber_band_points.asGeometry()
        self.assertEqual(prev_geom.asMultiPoint(), curr_geom.asMultiPoint())

    def test_key_events(self):
        event_key_press_control = QKeyEvent(QEvent.KeyPress, Qt.Key_Control, Qt.NoModifier)
        event_key_release_control = QKeyEvent(QEvent.KeyRelease, Qt.Key_Control, Qt.NoModifier)
        event_move_mouse = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, QPoint(1, 1))

        self.edit_tool.keyPressEvent(event_key_press_control)
        self.edit_tool.keyReleaseEvent(event_key_release_control)
        self.edit_tool.canvasMoveEvent(event_move_mouse)

    def write_temp_mission_xml(self):
        mission = Mission()
        mission.write_mission(self.mission_name+'.xml')


if __name__ == "__main__":
    unittest.main()

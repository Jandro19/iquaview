"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import math
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtCore import QPoint

from qgis.core import QgsPointXY, QgsPoint

from iquaview.src.utils.calcutils import (wrap_angle,
                                          get_angle_of_line_between_two_points,
                                          calc_slope,
                                          calc_is_collinear, calc_middle_point, calc_parallel_segment,
                                          distance_ellipsoid,
                                          bearing, endpoint_sphere, endpoint_ellipsoid, distance_plane,
                                          project_point_to_line, distance_to_segment, is_between)


class TestCalcUtils(unittest.TestCase):

    def setUp(self):
        pass

    def test(self):
        # wrap angle
        self.assertEqual(wrap_angle(0), 0)
        self.assertEqual(wrap_angle(2*math.pi), 0)
        self.assertEqual(wrap_angle(3*math.pi), math.pi)

        # get_angle_of_line_between_two_points
        self.assertEqual(get_angle_of_line_between_two_points(QgsPointXY(0, 0), QgsPointXY(1, 0)), 0.0)
        self.assertEqual(get_angle_of_line_between_two_points(QgsPointXY(0, 0), QgsPointXY(0, 1)), 90.0)
        self.assertEqual(get_angle_of_line_between_two_points(QgsPointXY(0, 0), QgsPointXY(-1, 0)), 180.0)
        self.assertEqual(get_angle_of_line_between_two_points(QgsPointXY(0, 0), QgsPointXY(0, -1)), -90.0)
        self.assertEqual(get_angle_of_line_between_two_points(QgsPointXY(0, 0), QgsPointXY(0, 0)), 0.0)

        # calc_slope
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(0, 0)), 0.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(1, 0)), 0.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(-1, 0)), 0.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(1, 1)), 1.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(1, 2)), 2.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(2, 1)), 0.5)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(1, -1)), -1.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(-1, -1)), 1.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(-1, 1)), -1.0)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(0, 1)), math.inf)
        self.assertEqual(calc_slope(QgsPointXY(0, 0), QgsPointXY(0, -1)), -math.inf)

        # calc_is_collinear
        self.assertEqual(calc_is_collinear(QgsPointXY(0, 0), QgsPointXY(0, 0), QgsPointXY(0, 0)), 0)
        self.assertEqual(calc_is_collinear(QgsPointXY(0, 0), QgsPointXY(0, 1), QgsPointXY(1, 0)), 1)
        self.assertEqual(calc_is_collinear(QgsPointXY(0, 0), QgsPointXY(0, 1), QgsPointXY(-1, 0)), -1)
        self.assertEqual(calc_is_collinear(QgsPointXY(0, -1), QgsPointXY(0, 1), QgsPointXY(0, 0)), 0)
        self.assertEqual(calc_is_collinear(QgsPointXY(1, 0), QgsPointXY(-1, 0), QgsPointXY(0, -1)), -1)
        self.assertEqual(calc_is_collinear(QgsPointXY(1, 0), QgsPointXY(-1, 0), QgsPointXY(0, 1)), 1)
        self.assertEqual(calc_is_collinear(QgsPointXY(0, -1), QgsPointXY(0, 1), QgsPointXY(0, 0)), 0)
        self.assertEqual(calc_is_collinear(QgsPointXY(0, -1), QgsPointXY(0, 1), QgsPointXY(0, 0)), 0)
        self.assertEqual(calc_is_collinear(QgsPointXY(0, -1), QgsPointXY(0, 1), QgsPointXY(0, 0)), 0)

        # calc_middle_point
        self.assertEqual(calc_middle_point(QgsPointXY(0, 0), QgsPointXY(0, 0)), QgsPointXY(0, 0))
        self.assertEqual(calc_middle_point(QgsPointXY(1, 0), QgsPointXY(1, 0)), QgsPointXY(1, 0))
        self.assertEqual(calc_middle_point(QgsPointXY(-1, 0), QgsPointXY(1, 0)), QgsPointXY(0, 0))
        self.assertEqual(calc_middle_point(QgsPointXY(1, 1), QgsPointXY(-3, -3)), QgsPointXY(-1, -1))

        # calc_parallel_segment
        p1, p2 = QgsPointXY(-1, -1), QgsPointXY(1, 1)
        p3, p4 = calc_parallel_segment(p1, p2, 0)  # dist = 0, return should be same points
        self.assertEqual(p1, p3)
        self.assertEqual(p2, p4)

        p1, p2 = QgsPointXY(0, -1), QgsPointXY(0, 1)
        p3, p4 = calc_parallel_segment(p1, p2, 2)
        self.assertEqual(QgsPointXY(-2, -1), p3)
        self.assertEqual(QgsPointXY(-2, 1), p4)

        p1, p2 = QgsPointXY(0, -1), QgsPointXY(0, 1)
        p3, p4 = calc_parallel_segment(p1, p2, -2)
        self.assertEqual(QgsPointXY(2, -1), p3)
        self.assertEqual(QgsPointXY(2, 1), p4)

        # distance_ellipsoid
        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(0, 0)
        self.assertEqual(0, distance_ellipsoid(p1, p2))

        # 1º longitude difference in the equator equals to 111 km
        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(1, 0)
        d = round(distance_ellipsoid(p1, p2)/1000)  # to pass from m to km
        self.assertEqual(111, d)

        # 1º longitude difference at 40 degrees north or south equals 85 km
        p1 = QgsPointXY(0, 40)
        p2 = QgsPointXY(1, 40)
        d = round(distance_ellipsoid(p1, p2) / 1000)
        self.assertEqual(85, d)

        # 1ª latitude difference anywhere equals to 111 km
        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(0, 1)
        d = round(distance_ellipsoid(p1, p2)/1000)
        self.assertEqual(111, d)

        # bearing
        p1 = QgsPointXY(0, 0)
        self.assertEqual(0, bearing(p1, p1))

        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(1, 0)
        self.assertEqual(90, bearing(p1, p2))
        self.assertEqual(-90, bearing(p2, p1))

        p1 = QgsPointXY(0, 0)
        p2 = QgsPointXY(0, 1)
        self.assertEqual(0, bearing(p1, p2))
        self.assertEqual(180, bearing(p2, p1))

        # endpoint_sphere
        p1 = QgsPointXY(0, 0)
        p2 = endpoint_sphere(p1, 111300, 90)  # 1ª latitude difference anywhere is 111 km 300m aprox
        self.assertAlmostEqual(1, p2.x(), 3)

        p2 = endpoint_sphere(p1, 111300, 180)
        self.assertAlmostEqual(-1, p2.y(), 3)

        # endpoint ellipsoid
        p1, p2 = QgsPointXY(40, 3), QgsPointXY(40, 4)
        dist = distance_ellipsoid(p1, p2)
        bear = bearing(p1, p2)  # sphere approximation, will bring precision loss
        p = endpoint_ellipsoid(p1, dist, bear)
        self.assertAlmostEqual(p2.x(), p.x(), 2)    # significant precision loss
        self.assertAlmostEqual(p2.y(), p.y(), 2)

        # distance_plane
        p1, p2 = QgsPointXY(1, 0), QgsPointXY(2, 0)
        self.assertEqual(1, distance_plane(p1, p2))

        p1, p2 = QgsPointXY(0, 1), QgsPointXY(0, 2)
        self.assertEqual(1, distance_plane(p1, p2))

        p1, p2 = QgsPointXY(-2, 0), QgsPointXY(2, 0)
        self.assertEqual(4, distance_plane(p1, p2))

        p1, p2 = QgsPointXY(0, 0), QgsPointXY(3, 4)
        self.assertEqual(5, distance_plane(p1, p2))

        # project_point_to_line
        p1, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(QgsPointXY(3, 0), project_point_to_line(p1, p_start, p_end))

        p1, p_start, p_end = QgsPointXY(3, -5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(QgsPointXY(3, 0), project_point_to_line(p1, p_start, p_end))

        p1, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(-15, 0), QgsPointXY(5, 0)
        self.assertEqual(QgsPointXY(3, 0), project_point_to_line(p1, p_start, p_end))

        p1, p_start, p_end = QgsPointXY(5, 0), QgsPointXY(0, 0), QgsPointXY(4, 0)
        self.assertEqual(QgsPointXY(5, 0), project_point_to_line(p1, p_start, p_end))

        # distance_to_segment
        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(5, distance_to_segment(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(3, -5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(5, distance_to_segment(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(3, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(0, distance_to_segment(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(3, 5)
        self.assertEqual(0, distance_to_segment(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(3, 5), QgsPointXY(5, 0)
        self.assertEqual(0, distance_to_segment(point, p_start, p_end))

        # is_between
        point, p_start, p_end = QgsPointXY(3, 5), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(0, 3), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(0, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(5, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(True, is_between(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(6, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(False, is_between(point, p_start, p_end))

        point, p_start, p_end = QgsPointXY(-1, 0), QgsPointXY(0, 0), QgsPointXY(5, 0)
        self.assertEqual(False, is_between(point, p_start, p_end))


if __name__ == "__main__":
    unittest.main()
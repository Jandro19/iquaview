"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import Qt, QPointF

from qgis.gui import QgsMapCanvas
from qgis.core import QgsPointXY

from iquaview.src.canvastracks.canvasmarker import CanvasMarker
from iquaview.src.config import Config


class TestCanvasMarker(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.canvas = QgsMapCanvas()
        self.canvas.setObjectName("canvas")
        self.default_color_auv = Qt.darkGray
        self.marker_mode = True
        self.marker = CanvasMarker(self.canvas, self.default_color_auv, None,
                                   marker_mode=self.marker_mode, config=self.config)

    def test_types(self):
        self.assertEqual(type(self.marker.get_color()), Qt.GlobalColor)
        self.assertEqual(self.marker.get_color(), Qt.darkGray)
        self.assertEqual(type(self.marker.get_marker_mode()), bool)
        self.assertEqual(self.marker.get_marker_mode(), True)

        self.assertEqual(type(self.marker.get_length()), float)
        self.assertEqual(type(self.marker.get_width()), float)
        self.assertEqual(type(self.marker.get_size()), int)
        self.assertEqual(type(self.marker.get_center()), QPointF)

        size = 55
        length = 17.0
        width = 29.0
        marker_mode = False
        color = Qt.cyan
        center = QgsPointXY(13.22, 2)

        self.marker.set_size(size)
        self.marker.set_length(length)
        self.marker.set_width(width)
        self.marker.set_marker_mode(marker_mode)
        self.marker.set_color(color)
        self.marker.set_center(center, heading=0.0)

        self.assertEqual(self.marker.get_size(), size)
        self.assertEqual(self.marker.get_length(), length)
        self.assertEqual(self.marker.get_width(), width)
        self.assertEqual(self.marker.get_marker_mode(), marker_mode)
        self.assertEqual(self.marker.get_color(), color)
        self.assertEqual(self.marker.get_center(), self.marker.toCanvasCoordinates(center))

        self.assertEqual(type(self.marker.get_color()), Qt.GlobalColor)
        self.assertEqual(type(self.marker.get_marker_mode()), bool)
        self.assertEqual(type(self.marker.get_length()), float)
        self.assertEqual(type(self.marker.get_width()), float)
        self.assertEqual(type(self.marker.get_size()), int)
        self.assertEqual(type(self.marker.get_center()), QPointF)


if __name__ == "__main__":
    unittest.main()
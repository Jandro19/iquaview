"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest


srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication

from qgis.gui import QgsMapCanvas
from qgis.core import QgsApplication

from iquaview.src import resources_rc, resources_qgis
from iquaview.src.config import Config
from iquaview.src.vehicle.vehicleinfo import VehicleInfo
from iquaview.src.vehicle.vehicledata import VehicleData
from iquaview.src.mission.missionstatus import MissionStatus
from iquaview.src.canvastracks.auvposewidget import AUVPoseWidget


class TestAuvPoseWidget(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)
        self.vehicle_data = VehicleData(self.config, self.vehicle_info)

        self.msg_log = QgsApplication.messageLog()
        self.mission_sts = MissionStatus(self.vehicle_data, self.msg_log)

        self.canvas = QgsMapCanvas()
        self.canvas.setObjectName("canvas")

        self.auv_pose = AUVPoseWidget(self.canvas, self.vehicle_info, self.vehicle_data, self.mission_sts)

    def test_connect(self):
        self.assertEqual(False, self.auv_pose.is_connected())
        self.auv_pose.connect()
        self.assertEqual(True, self.auv_pose.is_connected())
        self.auv_pose.disconnect()
        self.assertEqual(False, self.auv_pose.is_connected())

    def test_update_pose(self):
        self.auv_pose.connect()

        # mock vehicle data
        data = get_mock_data()
        self.vehicle_data.set_data('navigation status', data)

        self.auv_pose.auv_pose_update_canvas()


def get_mock_data():
    data = dict()
    data['global_position'] = {'longitude': 3.0332993934595223, 'latitude': 41.77770082113855}
    data['header'] = {'seq': 143140, 'frame_id': 'sparus2/base_link', 'stamp': {'secs': 1571909984, 'nsecs': 649338006}}
    data['origin'] = {'longitude': 3.0333, 'latitude': 41.7777}
    data['orientation_variance'] = {'yaw': 0.009999999776482582, 'roll': 0.00999999977648, 'pitch': 0.0099999997764}
    data['altitude'] = 15.114990234375
    data['position'] = {'north': 0.09120329659100541, 'depth': 0.04088652010212501, 'east': -0.05042686178335665}
    data['position_variance'] = {'north': 0.3594574663949893, 'depth': 0.016336442720768854, 'east': 0.3613904697356909}
    data['body_velocity'] = {'x': -5.624256548296993e-05, 'y': 0.0006189048094644342, 'z': 0.0007696325145229823}
    data['orientation'] = {'yaw': -0.008737131953239441, 'roll': -0.0001826024235924, 'pitch': 0.0001762669126037}
    data['orientation_rate'] = {'yaw': 0.0, 'roll': 0.0, 'pitch': -5.192636849274467e-26}
    data['valid_data'] = 'new_data'

    return data


if __name__ == "__main__":
    unittest.main()
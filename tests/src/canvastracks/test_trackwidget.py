"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication
from PyQt5.QtTest import QTest
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor

from qgis.gui import QgsMapCanvas
from qgis.core import QgsWkbTypes, QgsPointXY, QgsCoordinateReferenceSystem

from iquaview.src import resources_rc, resources_qgis
from iquaview.src.canvastracks.canvasmarker import CanvasMarker
from iquaview.src.canvastracks.trackwidget import TrackWidget
from iquaview.src.config import Config


class TestTrackWidget(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.canvas = QgsMapCanvas()
        self.canvas.setObjectName("canvas")
        self.geom_type = QgsWkbTypes.LineGeometry
        self.default_color_auv = Qt.darkGreen

        self.marker = CanvasMarker(self.canvas, self.default_color_auv,
                                   None,
                                   marker_mode=True, config=self.config)

    def test_buttons(self):
        self.trackwidget_auv = TrackWidget()
        self.trackwidget_auv.init("AUV track",
                                  self.canvas,
                                  self.default_color_auv,
                                  self.geom_type,
                                  self.marker)
        band = self.trackwidget_auv.get_band()

        # Write some points into the track
        number_of_points = 10
        for i in range(0, number_of_points):
            new_point = QgsPointXY(i*10, i*-10)
            heading = i*0.1
            self.trackwidget_auv.track_update_canvas(new_point, heading)
            self.assertEqual(band.getPoint(QgsWkbTypes.PointGeometry, i), new_point)

        # band should have as many vertices as points were added
        self.assertEqual(band.numberOfVertices(), number_of_points)

        # change crs
        crs = QgsCoordinateReferenceSystem("EPSG:3857")  # WSG 84 / Pseudo-Mercator
        self.canvas.setDestinationCrs(crs)

        QTest.mouseClick(self.trackwidget_auv.centerButton, Qt.LeftButton)
        QTest.mouseClick(self.trackwidget_auv.clearTrackButton, Qt.LeftButton)
        # Test that clearTrackButton really cleared the track
        self.assertEqual(band.numberOfVertices(), 0)

        # Test changing color_btn
        new_color = QColor(Qt.darkBlue)
        color_btn = self.trackwidget_auv.get_color_btn()
        color_btn.setColor(new_color)
        self.assertEqual(self.marker.get_color(), new_color)

        self.trackwidget_auv.close()


if __name__ == "__main__":
    unittest.main()

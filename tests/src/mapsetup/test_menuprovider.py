"""
Copyright (c) 2018 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest


srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication

from qgis.gui import QgsMapCanvas, QgsLayerTreeView
from qgis.core import QgsApplication, QgsProject, QgsLayerTreeModel, QgsPointXY, QgsMapLayer

from iquaview.src import resources_rc, resources_qgis
from iquaview.src.cola2api.mission_types import Mission
from iquaview.src.config import Config
from iquaview.src.mapsetup.menuprovider import MenuProvider
from iquaview.src.mapsetup.pointfeaturedlg import PointFeatureDlg
from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.vehicle.vehicleinfo import VehicleInfo


class TestMenuProvider(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.app = QApplication(sys.argv)

    @classmethod
    def tearDownClass(cls) -> None:
        cls.app.exit()
        del cls.app

    def setUp(self) -> None:
        self.proj = QgsProject.instance()
        self.proj.setFileName("test project")
        self.canvas = QgsMapCanvas()

        # Init view
        self.root = self.proj.layerTreeRoot()
        self.model = QgsLayerTreeModel(self.root)
        self.model.setFlag(QgsLayerTreeModel.AllowNodeReorder)
        self.model.setFlag(QgsLayerTreeModel.AllowNodeRename)
        self.model.setFlag(QgsLayerTreeModel.AllowNodeChangeVisibility)
        self.model.setFlag(QgsLayerTreeModel.ShowLegend)
        self.view = QgsLayerTreeView()
        self.view.setModel(self.model)


        # Init Mission Controller
        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings
        self.vehicle_info = VehicleInfo(self.config)
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()
        self.mission_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)
        self.mission_ctrl.mission_added.connect(self.add_map_layer)

        self.menu_provider = MenuProvider(self.view, self.canvas, self.proj, self.mission_ctrl)
        self.view.setMenuProvider(self.menu_provider)

        # Add a mission layer
        self.mission_name = "temp_mission"
        write_temp_mission_xml(self.mission_name)
        self.mission_ctrl.load_mission(os.getcwd()+"/"+self.mission_name + ".xml")

        # Init point feature dlg (to add landmarks)
        self.point_feature_dlg = PointFeatureDlg(self.canvas, self.proj)
        self.point_feature_dlg.reset()
        self.point_feature_dlg.landmark_added.connect(self.add_map_layer)

        # Add a landmark layer
        point = QgsPointXY(5.155, 7.753)
        self.point_feature_dlg.add_new_landmark(point)

    def tearDown(self) -> None:
        os.remove(self.mission_name + ".xml")
        self.proj.removeAllMapLayers()
        del self.model  # To avoid QBasicTimer crashes caused by python garbage collector random delete order

    def test(self):
        for layer in self.proj.mapLayers().values():
            self.view.setCurrentLayer(layer)
            self.menu_provider.createContextMenu()
            self.menu_provider.remove_layer()

        self.app.processEvents()
        self.assertEqual(0, len(self.proj.mapLayers()))

    def add_map_layer(self, layer):
        """
        Add layer to  the current project, view and canvas
        :param layer: layer
        :type layer: QgsMapLayer
        """
        self.proj.addMapLayer(layer, True)
        self.view.setCurrentLayer(layer)
        self.canvas.refresh()


def write_temp_mission_xml(name):
    mission = Mission()
    mission.write_mission(name+'.xml')


if __name__ == "__main__":
    unittest.main()
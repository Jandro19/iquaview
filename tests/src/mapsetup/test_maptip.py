"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtCore import QEvent, Qt
from PyQt5.QtWidgets import QToolTip
from qgis.core import QgsApplication, QgsPointXY
from qgis.gui import QgsMapMouseEvent


from iquaview.src.mainwindow import MainWindow
from iquaview.src.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionPosition,
                                                 MissionTolerance,
                                                 MissionWaypoint)

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestMapTip(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.qgs = QgsApplication([], True)
        cls.mainwindow = MainWindow()
    @classmethod
    def tearDownClass(cls) -> None:
        cls.mainwindow.close()
        cls.mainwindow.deleteLater()
        cls.qgs.exitQgis()
        del cls.qgs

    def setUp(self):

        self.mission_name = "temp_mission"
        self.write_temp_mission_xml()
        self.mainwindow.mission_ctrl.load_mission(os.getcwd()+"/"+self.mission_name + ".xml")

        QgsApplication.instance().processEvents()

        self.mission_track = self.mainwindow.mission_ctrl.mission_list[0]
        self.canvas = self.mainwindow.canvas
        self.pan_tool = self.canvas.mapTool()
        self.map_tip = self.mainwindow.map_tip

    def test_tooltip(self):
        # Get the position of a mission point
        m_position = self.mission_track.get_step(0).get_maneuver().get_position()
        m_point = QgsPointXY(float(m_position.get_longitude()), float(m_position.get_latitude()))
        canvas_point = self.pan_tool.toCanvasCoordinates(m_point)

        # Simulate mouse event and canvas under mouse
        move_event = QgsMapMouseEvent(self.canvas, QEvent.MouseMove, canvas_point)
        self.canvas.setAttribute(Qt.WA_UnderMouse)
        self.canvas.mouseMoveEvent(move_event)

        # QTimer doesn't work with unittests, so calling the function manually is needed
        self.map_tip.show_map_tip()
        # self.assertEqual(True, QToolTip.isVisible())

    def tearDown(self):
        os.remove(self.mission_name + ".xml")

    def write_temp_mission_xml(self):
        mission = Mission()

        wp1 = MissionWaypoint(MissionPosition(-41.777, 3.030, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))
        wp2 = MissionWaypoint(MissionPosition(-41.787, 3.034, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))
        wp3 = MissionWaypoint(MissionPosition(-41.777, 3.030, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))

        mission_step1 = MissionStep()
        mission_step2 = MissionStep()
        mission_step3 = MissionStep()

        mission_step1.add_maneuver(wp1)
        mission_step2.add_maneuver(wp2)
        mission_step3.add_maneuver(wp3)

        mission.add_step(mission_step1)
        mission.add_step(mission_step2)
        mission.add_step(mission_step3)

        mission.write_mission(self.mission_name + '.xml')


if __name__ == "__main__":
    unittest.main()

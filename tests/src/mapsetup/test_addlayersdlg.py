"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""
import sys
import os
import unittest
import atexit

from PyQt5.QtCore import QByteArray, QBuffer, QIODevice, QFile
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QDialogButtonBox
from qgis.gui import QgsMapCanvas
from qgis.core import QgsProject, QgsApplication

from iquaview.src.mapsetup.addlayers import AddLayersDlg

from iquaview.src import resources_rc

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestAddLayersDlg(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        cls.qgs = QgsApplication([], True)
        cls.qgs.setPrefixPath("/usr", True)
        cls.qgs.initQgis()

        cls.vector_file_name = "vectorlayer.gpx"
        create_vector_file(cls.vector_file_name)

        cls.csv_file_name = "csvlayer.csv"
        create_csv_file(cls.csv_file_name)

        cls.raster_file_name = "rasterlayer.tif"
        create_raster_file(cls.raster_file_name)

    @classmethod
    def tearDownClass(cls) -> None:
        os.remove(cls.vector_file_name)
        os.remove(cls.csv_file_name)
        os.remove(cls.raster_file_name)

        # A aux.xml file is created after the test terminates from raster layer, this is the only way to get rid of it
        @atexit.register
        def remove_aux_raster():
            os.remove(cls.raster_file_name+".aux.xml")

        cls.qgs.exitQgis()
        del cls.qgs

    def setUp(self) -> None:
        self.canvas = QgsMapCanvas()
        self.proj = QgsProject.instance()
        self.proj.setFileName("test project")

        self.msg_log = QgsApplication.messageLog()

        self.dialog = AddLayersDlg(self.proj, self.canvas, self.msg_log)

    def test_vector_layer(self):
        self.dialog.mOptionsListWidget.setCurrentRow(0)  # Vector layer
        self.dialog.vector_mQgsFileWidget.setFilePath(os.path.abspath(self.vector_file_name))
        accept_button = self.dialog.buttonBox.button(QDialogButtonBox.Ok)
        accept_button.click()

    def test_csv_layer_points(self):
        self.dialog.mOptionsListWidget.setCurrentRow(1)  # CSV layer
        self.dialog.delimited_text_mFileWidget.setFilePath(os.path.abspath(self.csv_file_name))
        accept_button = self.dialog.buttonBox.button(QDialogButtonBox.Ok)
        accept_button.click()

    def test_csv_layer_path(self):
        self.dialog.mOptionsListWidget.setCurrentRow(1)  # CSV layer
        self.dialog.delimited_text_mFileWidget.setFilePath(os.path.abspath(self.csv_file_name))
        self.dialog.path_radioButton.setChecked(True)
        self.dialog.points_radioButton.setChecked(False)
        accept_button = self.dialog.buttonBox.button(QDialogButtonBox.Ok)
        accept_button.click()

    def test_raster_layer(self):
        self.dialog.mOptionsListWidget.setCurrentRow(2)  # Raster layer
        self.dialog.raster_mQgsFileWidget.setFilePath(os.path.abspath(self.raster_file_name))
        accept_button = self.dialog.buttonBox.button(QDialogButtonBox.Ok)
        accept_button.click()

    def test_web_map_layers(self):
        n_maps = self.dialog.url_comboBox.count()

        for i in range(0, n_maps):
            self.dialog.mOptionsListWidget.setCurrentRow(3)  # Tiled Web Map layer
            self.dialog.url_comboBox.setCurrentIndex(i)
            accept_button = self.dialog.buttonBox.button(QDialogButtonBox.Ok)
            accept_button.click()
            if i < n_maps - 1:
                self.dialog = AddLayersDlg(self.proj, self.canvas, self.msg_log)


def create_vector_file(file_name):
    vector_gpx = u"""<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
                 <gpx xmlns="http://www.topografix.com/GPX/1/1" creator="byHand" version="1.1" 
                 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
        xsi:schemaLocation="http://www.topografix.com/GPX/1/1 http://www.topografix.com/GPX/1/1/gpx.xsd">
      <wpt lat="39.921055008" lon="3.054223107">
        <ele>12.863281</ele>
        <time>2005-05-16T11:49:06Z</time>
        <name>Cala Sant Vicenç - Mallorca</name>
        <sym>City</sym>
      </wpt>
    </gpx>""".encode("utf-8")
    vector_file = open(file_name, "wb")
    vector_file.write(vector_gpx)
    vector_file.close()


def create_raster_file(file_name):
    pixmap = QPixmap(["16 16 3 1",
                      "      c None",
                      ".     c #FF0000",
                      "+     c #1210f3",
                      "                ",
                      "       +.+      ",
                      "      ++.++     ",
                      "     +.....+    ",
                      "    +.     .+   ",
                      "   +.   .   .+  ",
                      "  +.    .    .+ ",
                      " ++.    .    .++",
                      " ... ...+... ...",
                      " ++.    .    .++",
                      "  +.    .    .+ ",
                      "   +.   .   .+  ",
                      "   ++.     .+   ",
                      "    ++.....+    ",
                      "      ++.++     ",
                      "       +.+      "])
    arr = QByteArray()
    buffer = QBuffer(arr)
    buffer.open(QIODevice.WriteOnly)
    pixmap.save(buffer, "BMP", )

    raster_file = QFile(file_name)
    raster_file.open(QIODevice.WriteOnly)
    raster_file.write(arr)
    raster_file.close()


def create_csv_file(file_name):
    csv_file = open(file_name, "w+")
    csv_file.write("lat,lon\n41.778019880608397,3.032957321049464")
    csv_file.close()


if __name__ == "__main__":
    unittest.main()
"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtWidgets import QApplication

from iquaview.src.mapsetup.landmarkcoordinatesdlg import LandmarkCoordinatesDlg

from iquaview.src import resources_rc

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestLandmarkCoordsDlg(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.lat = 1.22334455667
        self.lon = 9.88776655443
        self.dialog = LandmarkCoordinatesDlg(self.lat, self.lon)

    def test(self):
        self.assertEqual(self.lat, float(self.dialog.latitude_lineEdit.text()))
        self.assertEqual(self.lon, float(self.dialog.longitude_lineEdit.text()))

        self.dialog.copy_to_clipboard()
        cb = QApplication.clipboard()
        self.assertEqual("{}, {}".format(self.lat, self.lon), cb.text())


if __name__ == "__main__":
    unittest.main()
"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from PyQt5.QtWidgets import QApplication
from PyQt5.QtGui import QValidator

from qgis.gui import QgsMapCanvas
from qgis.core import QgsProject, QgsPointXY, QgsWkbTypes

from iquaview.src import resources_rc, resources_qgis
from iquaview.src.utils.textvalidator import get_color
from iquaview.src.mapsetup.pointfeaturedlg import PointFeatureDlg



class TestPointFeatureDlg(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        self.canvas = QgsMapCanvas()
        self.proj = QgsProject.instance()

        self.point_feature_dlg = PointFeatureDlg(self.canvas, self.proj)
        self.point_feature_dlg.landmark_added.connect(self.add_map_layer)
        self.point_feature_dlg.reset()

    def test_point_conversion(self):

        point = QgsPointXY(5.155, 7.753)
        self.point_feature_dlg.add_new_landmark(point)
        coord = self.point_feature_dlg.get_coordinates()
        self.assertAlmostEqual(point.x(), coord[1], 12)  # 12 decimal precision (0.1mm)
        self.assertAlmostEqual(point.y(), coord[0], 12)
        self.point_feature_dlg.copy_to_clipboardButton.click()

        comboBox = self.point_feature_dlg.get_comboBox()
        comboBox.setCurrentIndex(1)
        coord = self.point_feature_dlg.get_coordinates()
        self.assertAlmostEqual(point.x(), coord[1], 12)
        self.assertAlmostEqual(point.y(), coord[0], 12)
        self.point_feature_dlg.copy_to_clipboardButton.click()

        comboBox.setCurrentIndex(2)
        coord = self.point_feature_dlg.get_coordinates()
        self.assertAlmostEqual(point.x(), coord[1], 12)
        self.assertAlmostEqual(point.y(), coord[0], 12)
        self.point_feature_dlg.copy_to_clipboardButton.click()

    def test_validators(self):
        combobox = self.point_feature_dlg.get_comboBox()
        yellow = get_color(QValidator.Intermediate)
        red = get_color(QValidator.Invalid)
        for i in range(0, 3):
            combobox.setCurrentIndex(i)
            lineedits = self.point_feature_dlg.get_lineedits()
            for le in lineedits:
                # Reading stylesheet to know the color of the lineedit
                # all lineedits start blank, so they must be white
                self.assertTrue(len(le.styleSheet()) == 0 or '#' not in le.styleSheet())  # color is white

                # with ints all of the lineedits must be white
                le.setText("1")
                self.assertTrue(len(le.styleSheet()) == 0 or '#' not in le.styleSheet())

                # when changed to something that's not a float, validator will change lineedit color to red
                le.setText("hello")
                self.assertTrue(red in le.styleSheet())

                le.setText("1")
                self.assertTrue(len(le.styleSheet()) == 0 or '#' not in le.styleSheet())

                # to diferentiate lineedits validated with int and with double,
                # there is one key difference that can be observer. Setting text to blank again will make
                # int validated lineedits white, while float validated lineedits will be yellow
                le.setText("")
                if le.styleSheet() == 0 or '#' not in le.styleSheet():  # white lineedit -> int validation
                    le.setText("1.000")
                    self.assertTrue(red in le.styleSheet())
                    le.setText("1,000")
                    self.assertTrue(red in le.styleSheet())
                elif red in le.styleSheet():  # yellow lineedit -> float validation
                    le.setText("1.2")
                    self.assertTrue(len(le.styleSheet()) == 0 or '#' not in le.styleSheet())
                    le.setText("1,2")
                    self.assertTrue(red in le.styleSheet())
                    le.setText("1.")
                    self.assertTrue(yellow in le.styleSheet())
                    le.setText("1,")
                    self.assertTrue(red in le.styleSheet())

    def test_add_point(self):
        lat = 41.1566
        lon = 3.24556
        self.point_feature_dlg.lat_degrees_lineedit.setText(str(lat))
        self.point_feature_dlg.lon_degrees_lineedit.setText(str(lon))
        self.point_feature_dlg.accept()

        layers = list(self.proj.mapLayers().values())
        self.assertEqual(1, len(layers))
        self.assertEqual(QgsWkbTypes.PointGeometry, layers[0].geometryType())

        for feature in layers[0].getFeatures():
            wp = feature.geometry().asPoint()
            self.assertAlmostEqual(lat, wp.y(), 8)
            self.assertAlmostEqual(lon, wp.x(), 8)

    def add_map_layer(self, layer):
        self.proj.addMapLayer(layer, True)


if __name__ == "__main__":
    unittest.main()
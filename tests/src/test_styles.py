"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QApplication
from qgis.core import QgsProject, QgsApplication
from qgis.gui import QgsMapCanvas

from iquaview.src import resources_rc, resources_qgis
from iquaview.src import styles
from iquaview.src.mapsetup.decoration import northarrow, scalebar
from iquaview.src.mission.missioncontroller import MissionController
from iquaview.src.vehicle.vehicleinfo import VehicleInfo
from iquaview.src.cola2api.mission_types import (Mission,
                                                 MissionStep,
                                                 MissionPosition,
                                                 MissionTolerance,
                                                 MissionWaypoint)

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.config import Config


class TestStyles(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)

        self.proj = QgsProject.instance()
        self.proj.setFileName("")

        self.canvas = QgsMapCanvas()
        self.view = None
        self.wp_dock = None
        self.templates_dock = None
        self.minfo_dock = None
        self.msg_log = QgsApplication.messageLog()
        self.mission_name = "temp_mission"

        self.m_ctrl = MissionController(self.config, self.vehicle_info, self.proj, self.canvas, self.view,
                                              self.wp_dock, self.templates_dock, self.minfo_dock, self.msg_log)
        write_temp_mission_xml("mission1")
        write_temp_mission_xml("mission2")
        self.m_ctrl.load_mission(os.getcwd()+"/mission1" + ".xml")
        self.m_ctrl.load_mission(os.getcwd()+"/mission2" + ".xml")

        self.scale_bar = scalebar.ScaleBar(self.canvas, self.config)
        self.north_arrow = northarrow.NorthArrow(self.canvas, self.config)

        self.styles_dlg = styles.StylesWidget(self.config, self.canvas, self.m_ctrl, self.north_arrow, self.scale_bar)

    def tearDown(self):
        os.remove("mission1.xml")
        os.remove("mission2.xml")

    def test(self):
        new_color = QColor("Red")

        def_color = self.north_arrow.default_color
        self.styles_dlg.north_arrow_color_button.setColor(new_color)
        self.assertEqual(new_color.name(), self.north_arrow.current_color)
        self.styles_dlg.reset_color_north_arrow_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.north_arrow_color_button.color().name())

        def_color = self.scale_bar.get_default_color()
        self.styles_dlg.change_scale_bar_color(new_color)
        self.assertEqual(new_color.name(), self.scale_bar.get_current_color().name())
        self.styles_dlg.reset_color_scale_bar_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.scalebar_color_button.color().name())

        def_color = self.styles_dlg.start_end_color_button.color()
        self.styles_dlg.start_end_color_button.setColor(new_color)
        for mt in self.m_ctrl.get_mission_list():
            self.assertEqual(new_color.name(), mt.get_current_start_end_marker_color().name())
        self.styles_dlg.reset_color_start_end_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.start_end_color_button.color().name())

        def_color = self.styles_dlg.missions_color_button.color()
        self.styles_dlg.missions_color_button.setColor(new_color)
        for mt in self.m_ctrl.get_mission_list():
            self.assertEqual(new_color.name(), mt.get_current_track_color().name())
        self.styles_dlg.reset_color_missions_btn.click()
        self.assertEqual(def_color.name(), self.styles_dlg.missions_color_button.color().name())

        self.styles_dlg.save_as_defaults()

        self.styles_dlg.close()


def write_temp_mission_xml(mission_name):
    mission = Mission()

    wp1 = MissionWaypoint(MissionPosition(-41.777, 3.030, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))
    wp2 = MissionWaypoint(MissionPosition(-41.787, 3.034, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))
    wp3 = MissionWaypoint(MissionPosition(-41.777, 3.030, 15.0, False), 0.5, MissionTolerance(2.0, 2.0, 1.0))

    mission_step1 = MissionStep()
    mission_step2 = MissionStep()
    mission_step3 = MissionStep()

    mission_step1.add_maneuver(wp1)
    mission_step2.add_maneuver(wp2)
    mission_step3.add_maneuver(wp3)

    mission.add_step(mission_step1)
    mission.add_step(mission_step2)
    mission.add_step(mission_step3)

    mission.write_mission(mission_name + '.xml')


if __name__ == "__main__":
    unittest.main()

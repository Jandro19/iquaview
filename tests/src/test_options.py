"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtWidgets import QApplication

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)

from iquaview.src.generaloptions import GeneralOptionsWidget
from iquaview.src.config import Config
from iquaview.src.vehicle.vehicleinfo import VehicleInfo


class TestGeneralOptions(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)
        #sparus2 default
        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)
        self.options_dialog = GeneralOptionsWidget(self.config, self.vehicle_info)


    def test_set_vessel_canvas_marker_mode(self):

        # check equal to default value in config
        self.assertEqual(self.config.csettings['canvas_marker_mode'], "auto")

        self.options_dialog.set_vessel_canvas_marker_mode()

        self.assertEqual(self.options_dialog.auto_canvasmarker_radioButton.isChecked(), True)
        self.assertEqual(self.options_dialog.manual_canvasmarker_radioButton.isChecked(), False)
        self.assertEqual(self.config.csettings['canvas_marker_scale'],self.options_dialog.changingscale_spinBox.value())

    def test_coordinate_combobox(self):

        # check equal to default value in config
        self.assertEqual(self.config.csettings['coordinate_format'], "degree")

        self.options_dialog.set_coordinate_combobox()

        self.assertEqual(self.options_dialog.coordinate_comboBox.currentIndex(), 0)

if __name__ == "__main__":
    unittest.main()

"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtWidgets import QApplication

from iquaview.src.config import Config
from iquaview.src.vehicle.vehicleinfo import VehicleInfo
from iquaview.src.connection.settings.usblconnectionwidget import USBLConnectionWidget


srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestUsblconnectionWidget(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)

    def test_validator(self):
        self.usbl_connection_widget = USBLConnectionWidget()
        self.usbl_connection_widget.ip = self.config.csettings['usbl_ip']
        self.usbl_connection_widget.port = self.config.csettings['usbl_port']
        self.usbl_connection_widget.ownid = self.config.csettings['usbl_own_id']
        self.usbl_connection_widget.targetid = self.config.csettings['usbl_target_id']

        self.assertTrue(self.usbl_connection_widget.is_usbl_valid())


if __name__ == "__main__":
    unittest.main()

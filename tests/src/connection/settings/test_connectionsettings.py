"""
Copyright (c) 2019 Iqua Robotics SL

This program is free software: you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation, either version 2 of
the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <http://www.gnu.org/licenses/>.
"""

import sys
import os
import unittest

from PyQt5.QtWidgets import QApplication
from iquaview.src.config import Config
from iquaview.src.connection.settings.connectionsettings import ConnectionSettingsWidget
from iquaview.src.vehicle.vehicleinfo import VehicleInfo

srcpath = os.path.dirname(os.path.realpath(sys.argv[0]))
iquaview_root_path = srcpath + '/../'
sys.path.append(iquaview_root_path)


class TestConnectionSettings(unittest.TestCase):

    def setUp(self):
        self.app = QApplication(sys.argv)

        self.config = Config()
        self.config.load()
        self.config.csettings = self.config.settings

        self.vehicle_info = VehicleInfo(self.config)

        self.connection_settings = ConnectionSettingsWidget(self.config, self.vehicle_info)

    def test(self):
        auv_ip = self.vehicle_info.get_vehicle_ip()
        joystick_dev = self.config.settings['joystick_device']
        self.connection_settings.auv_connection_widget.ip = auv_ip
        self.connection_settings.teleoperation_connection_widget.joystickdevice = joystick_dev
        self.connection_settings.save()

        self.assertEqual(auv_ip, self.vehicle_info.get_vehicle_ip())
        self.assertEqual(joystick_dev, self.config.settings['joystick_device'])


if __name__ == "__main__":
    unittest.main()
